/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;
import djf.components.AppDataComponent;
import static djf.settings.AppStartupConstants.APP_PROPERTIES_FILE_NAME;
import java.io.IOException;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import tam.TAManagerApp;
import tam.data.ProjectData;
import tam.data.ProjectStudentData;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.file.TAFiles;
import tam.file.TimeSlot;


/**
 *
 * @author User
 */
public class testSave {
    
    
    public static String startHour="9";
    public static String endHour="20";
    public static String subj="AMS";
    public static String semester="Fall";
    public static String number="310";
    public static String year="2017";
    public static String title="AMS 310";
    public static String teacherName="Quinn Favilla";
    public static String homeAddress="www.quinnfav.com";
    public static String exportDir="\\Courses\\ISE220\\SPRING2016\\public";
    public static String templateDir="C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManagerTester\\public_html";
    public static boolean home= true;
    public static boolean syllabus= true;
    public static boolean schedule=true;
    public static boolean hws=true;
    public static boolean projects=true;
    public static String bannerimage="bannerimage.PNG";
    public static String leftfooterimage="leftimage.PNG";
    public static String rightfooterimage="right.PNG";
    public static String stylesheet="syllabus_home.css";
    public static String startMondayMonth="11";
    public static String startMondayDay="28";
    public static String endFridayMonth="5";
    public static String endFridayDay="26";
    public static String img3Logo="file:C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\images\\TAManagerLogo.png";
    public static TeachingAssistant[] teachingAssistants= new TeachingAssistant[4];
    public static TimeSlot[] timeSlots= new TimeSlot[7];
    public static RecitationData[] recData= new RecitationData[2];
    public static ScheduleData[] schedData= new ScheduleData[4];
    public static ProjectData[] projData= new ProjectData[2];
    public static ProjectStudentData[] studentProjData= new ProjectStudentData[3];
    public static TAData data;
    public static TAFiles files;
    
        
    
    public testSave() throws IOException{
        TAManagerApp app= new TAManagerApp();
        //TAManagerApp app= new TAManagerApp();    
        app.loadProperties(APP_PROPERTIES_FILE_NAME);
        app.setData(new TAData(app));
        data=(TAData) app.getDataComponent();
        app.setFile(new TAFiles(app));
        files=(TAFiles) app.getFileComponent();
        
        
        data.setStartHour(9);
        data.setEndHour(20);
        data.setSubject("AMS");
        data.setSem("Fall");
        data.setNum("310");
        data.setYear("2017");
        data.setTitleTextField("AMS 310");
        data.setNameCourseTextField("Quinn Favilla");
        data.setHomeTextField("www.quinnfav.com");
        data.setExportDir("\\Courses\\ISE220\\SPRING2016\\public");
        data.setTemplateDir("C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManagerTester\\public_html");
       
        data.setHome(true);
        data.setSyllabus(true);
        data.setSchedule(true);
        data.setHWS(true);
        data.setProjects(true);
        
        data.setBannerImage("bannerimage.PNG");
        data.setLeftFooterImage("leftimage.PNG");
        data.setRightFooterImage("right.PNG");
        data.setStyleSheet("syllabus_home.css");
        
        data.addTA("John Doe", "johndoe@gmail.com", false);
        data.addTA("Jane Doe", "janedoe@gmail.com", false);
        data.addTA("Chuck Doe", "chuckdoe@gmail.com", false);
        data.addTA("Quinn Favilla", "qfavilla@gmail.com", false);
          
        
        
        TeachingAssistant ta1=new TeachingAssistant("John Doe", "johndoe@gmail.com", false);
        TeachingAssistant ta2=new TeachingAssistant("Jane Doe", "janedoe@gmail.com", false);
        TeachingAssistant ta3= new TeachingAssistant("Chuck Doe", "chuckdoe@gmail.com", false);
        TeachingAssistant ta4= new TeachingAssistant("Quinn Favilla", "qfavilla@gmail.com", false);
        
        teachingAssistants[0]=ta3;
        teachingAssistants[1]=ta2;
        teachingAssistants[2]=ta1;
        teachingAssistants[3]=ta4;
        
        ArrayList<TimeSlot> TAs= new ArrayList<TimeSlot>();
        TAs.add(new TimeSlot("TUESDAY", "10_00am", "Quinn Favilla"));
        TAs.add(new TimeSlot("MONDAY", "11_30am", "Quinn Favilla"));
        TAs.add(new TimeSlot("TUESDAY", "11_30am", "Quinn Favilla"));
        TAs.add(new TimeSlot("WEDNESDAY", "12_00pm", "Quinn Favilla"));
        TAs.add(new TimeSlot("TUESDAY", "1_30pm", "Test"));
        TAs.add(new TimeSlot("FRIDAY", "4_00pm", "John Doe"));
        TAs.add(new TimeSlot("MONDAY", "2_00pm", "Chuck Doe"));
        files.setTAGridHours(TAs);
        
        
       TimeSlot t1=new TimeSlot("TUESDAY", "10_00am", "Quinn Favilla");
       TimeSlot t2=new TimeSlot("MONDAY", "11_30am", "Quinn Favilla");
       TimeSlot t3=new TimeSlot("TUESDAY", "11_30am", "Quinn Favilla");
       TimeSlot t4=new TimeSlot("WEDNESDAY", "12_00pm", "Quinn Favilla");
       TimeSlot t5=new TimeSlot("TUESDAY", "1_30pm", "Test");
       TimeSlot t6= new TimeSlot("FRIDAY", "4_00pm", "John Doe");
       TimeSlot t7= new TimeSlot("MONDAY", "2_00pm", "Chuck Doe");
       
       
       
       
       timeSlots[0]=t1;
       timeSlots[1]=t2;
       timeSlots[2]=t3;
       timeSlots[3]=t4;
       timeSlots[4]=t5;
       timeSlots[5]=t6;
       timeSlots[6]=t7;
        
       
        data.addRecitation("R02", "Mckenna", "Wed 3:30pm-4:30pm", "old CS 2114", "Test", "John Doe");
        data.addRecitation("R05", "Banerjee", "Tues 5:30pm-6:40pm", "old CS 2114", "Chuck Doe", "Lily Adams");
        
        RecitationData d1= new RecitationData("R02", "Mckenna", "Wed 3:30pm-4:30pm", "old CS 2114", "Test", "John Doe");
        RecitationData d2=new RecitationData("R05", "Banerjee", "Tues 5:30pm-6:40pm", "old CS 2114", "Chuck Doe", "Lily Adams");
        
        recData[0]=d1;
        recData[1]=d2;
        
        data.addSchedData("Holiday", "17/2/9", "SNOW DAY", " ", " ", " ", " ");
        data.addSchedData("Lecture", "17/2/14", "Lecture 3", "Event Programming", " ", " ", " ");
        data.addSchedData("Holiday", "17/3/13", "Spring Break", " ", " ", " ", " ");
        data.addSchedData("HWs", "22/2/17", "HW 1", "due @ 11:59pm", "./hw/HW1.html", "12:01am", "none");
        
        data.setSchedStartMonthDate("1");
        data.setSchedStartDayDate("23");
        data.setSchedEndMonthDate("5");
        data.setSchedEndDayDate("19");
        
        
        ScheduleData s1= new ScheduleData("Holiday", "17/2/9", "SNOW DAY", " ", " ", " ", " ");
        ScheduleData s2= new ScheduleData("Lecture", "17/2/14", "Lecture 3", "Event Programming", " ", " ", " ");
        ScheduleData s3= new ScheduleData("Holiday", "17/3/13", "Spring Break", " ", " ", " ", " ");
        ScheduleData s4= new ScheduleData("HWs", "22/2/17", "HW 1", "due @ 11:59pm", "./hw/HW1.html", "12:01am", "none");
        
        schedData[0]=s1;
        schedData[1]=s2;
        schedData[2]=s3;
        schedData[3]=s4;
        
        data.addProjData("Atomic Comics", "55221", "ffffff", "http://atomiccomic.com");
        data.addProjData("C4 Comics", "235339", "ffffff", "https://c4-comics.appspot.com");
        
        ProjectData p1= new ProjectData("Atomic Comics", "55221", "ffffff", "http://atomiccomic.com");
        ProjectData p2= new ProjectData("C4 Comics", "235339", "ffffff", "https://c4-comics.appspot.com");
        
        projData[0]=p1;
        projData[1]=p2;
        
        data.addProjStudentData("Beau", "Brummel", "Atomic Comics", "Lead Designer");
        data.addProjStudentData("Jane", "Doe", "C4 Comics", "Lead Programmer");
        data.addProjStudentData("Noonian", "Soong", "Atomic Comics", "Data Designer");
        
        ProjectStudentData psd1= new ProjectStudentData("Beau", "Brummel", "Atomic Comics", "Lead Designer");
        ProjectStudentData psd2= new ProjectStudentData("Jane", "Doe", "C4 Comics", "Lead Programmer");
        ProjectStudentData psd3= new ProjectStudentData("Noonian", "Soong", "Atomic Comics", "Data Designer");
        
        studentProjData[0]=psd1;
        studentProjData[1]=psd2;
        studentProjData[2]=psd3;
        //files.saveData(data, "C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\work\\SiteSaveTest3.json");
      
    
        
    }
    
   public static void main(String [] args) throws IOException{
      //testSave test= new testSave(new TAManagerApp());
      testSave test= new testSave();
      test.getFiles().saveData(test.getData(), "C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\work\\SiteSaveTest2.json");
      
   }
    
  
    
    
    public String getStartHour(){
        return this.startHour;
    }
    
    public String getSubject(){
        return this.subj;
    }
    
    public String getSemester(){
        return this.semester;
    }
    
    public String getYear(){
        return this.year;
    }
    
    public String getNumber(){
        return this.number;
    }
    
    public String getEndHour(){
        return this.endHour;
    }
    
    public String getTitle(){
        return this.title;
    }
    
    public String getName(){
        return this.teacherName;
    }
    
    public String getHome(){
        return this.homeAddress;
    }
    
    public String getExportDir(){
        return this.exportDir;
    }
    
    public String getTemplateDir(){
        return this.templateDir;
    }
    
    public boolean getHomeBool(){
        return this.home;
    }
    
    public boolean getSyllabusBool(){
        return this.syllabus;
    }
    
    public boolean getProjectsBool(){
        return this.projects;
    }
    
    public boolean getSchedBool(){
        return this.schedule;
    }
    
    public boolean getHWSBool(){
        return this.hws;
    }
    
    public String getBannerImage(){
        return this.bannerimage;
    }
    
    public String getRightFooter(){
        return this.rightfooterimage;
    }
    
    public String getLeftFooter(){
        return this.leftfooterimage;
    }
    
    public String getStyleSheet(){
        return this.stylesheet;
    }
    
    public TAFiles getFiles(){
        return files;
    }
    
    public TAData getData(){
        return data;
    }
    
    public TeachingAssistant[] getTeachingAssistants(){
        return this.teachingAssistants;
    }
    
    public TimeSlot[] getTimeSlots(){
        return this.timeSlots;
    }
    
    public RecitationData[] getRecData(){
        return this.recData;
    }
    
    public ScheduleData[] getSchedData(){
        return this.schedData;
    }
    
    public ProjectData[] getProjectData(){
        return this.projData;
    }
    
    public ProjectStudentData[] getStudentProjData(){
        return this.studentProjData;
    }
    
   
    
}
