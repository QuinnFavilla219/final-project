/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test_bed;

import static djf.settings.AppStartupConstants.APP_PROPERTIES_FILE_NAME;
import java.io.IOException;
import java.util.ArrayList;
import tam.TAManagerApp;
import tam.data.ProjectData;
import tam.data.ProjectStudentData;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.file.TAFiles;
import tam.file.TimeSlot;

/**
 *
 * @author User
 */
public class testSave2 {
      
    
    String startHour="9";
    String endHour="20";
    String subj="ISE";
    String semester="Spring";
    String number="220";
    String year="2016";
    String title="ISE 220";
    String teacherName="Richard McKenna";
    String homeAddress="http://www.cs.stonybrook.edu/~richard";
    String exportDir="\\Courses\\ISE220\\SPRING2016\\public";
    String templateDir=".\\templates\\ISE220";
    boolean home= true;
    boolean syllabus= true;
    boolean schedule=true;
    boolean hws=true;
    boolean projects=false;
    String bannerimage="bannerimage.PNG";
    String leftfooterimage="leftimage.PNG";
    String rightfooterimage="right.PNG";
    String stylesheet="syllabus_home.css";
    TeachingAssistant[] teachingAssistants= new TeachingAssistant[3];
    TimeSlot[] timeSlots= new TimeSlot[3];
    RecitationData[] recData= new RecitationData[2];
    ScheduleData[] schedData= new ScheduleData[3];
    ProjectData[] projData= new ProjectData[2];
    ProjectStudentData[] studentProjData= new ProjectStudentData[3];
    TAData data;
    TAFiles files;
    
        
    
    public testSave2(TAManagerApp app) throws IOException{
        //TAManagerApp app= new TAManagerApp();
        //TAManagerApp app= new TAManagerApp();    
        app.loadProperties(APP_PROPERTIES_FILE_NAME);
        app.setData(new TAData(app));
        data=(TAData) app.getDataComponent();
        app.setFile(new TAFiles(app));
        files=(TAFiles) app.getFileComponent();
        
        
        data.setStartHour(9);
        data.setEndHour(20);
        data.setSubject("ISE");
        data.setSem("Spring");
        data.setNum("220");
        data.setYear("2016");
        data.setTitleTextField("ISE 220");
        data.setNameCourseTextField("Richard McKenna");
        data.setHomeTextField("http://www.cs.stonybrook.edu/~richard");
        data.setExportDir("\\Courses\\ISE220\\SPRING2016\\public");
        data.setTemplateDir(".\\templates\\ISE220");
       
        data.setHome(true);
        data.setSyllabus(true);
        data.setSchedule(true);
        data.setHWS(true);
        data.setProjects(false);
        
        data.setBannerImage("bannerimage.PNG");
        data.setLeftFooterImage("leftimage.PNG");
        data.setRightFooterImage("right.PNG");
        data.setStyleSheet("syllabus_home.css");
        
        data.addTA("John Doe", "johndoe@gmail.com", false);
        data.addTA("Jane Doe", "janedoe@gmail.com", false);
        data.addTA("Chuck Doe", "chuckdoe@gmail.com", true);
          
        
        
        TeachingAssistant ta1=new TeachingAssistant("John Doe", "johndoe@gmail.com", false);
        TeachingAssistant ta2=new TeachingAssistant("Jane Doe", "janedoe@gmail.com", false);
        TeachingAssistant ta3= new TeachingAssistant("Chuck Doe", "chuckdoe@gmail.com", true);
        
        teachingAssistants[0]=ta3;
        teachingAssistants[1]=ta2;
        teachingAssistants[2]=ta1;
        
        ArrayList<TimeSlot> TAs= new ArrayList<TimeSlot>();
        TAs.add(new TimeSlot("TUESDAY", "1_30pm", "Jane Doe"));
        TAs.add(new TimeSlot("FRIDAY", "4_00pm", "John Doe"));
        TAs.add(new TimeSlot("MONDAY", "2_00pm", "Chuck Doe"));
        files.setTAGridHours(TAs);
        
        
       TimeSlot t1= new TimeSlot("TUESDAY", "1_30pm", "Jane Doe");
       TimeSlot t2= new TimeSlot("FRIDAY", "4_00pm", "John Doe");
       TimeSlot t3= new TimeSlot("MONDAY", "2_00pm", "Chuck Doe");
       
       timeSlots[0]=t1;
       timeSlots[1]=t2;
       timeSlots[2]=t3;
        
       
        data.addRecitation("R02", "Mckenna", "Wed 3:30pm-4:30pm", "old CS 2114", "Jane Doe", "John Doe");
        data.addRecitation("R05", "Banerjee", "Tues 5:30pm-6:40pm", "old CS 2114", "Chuck Doe", "Lily Adams");
        
        RecitationData d1= new RecitationData("R02", "Mckenna", "Wed 3:30pm-4:30pm", "old CS 2114", "Jane Doe", "John Doe");
        RecitationData d2=new RecitationData("R05", "Banerjee", "Tues 5:30pm-6:40pm", "old CS 2114", "Chuck Doe", "Lily Adams");
        
        recData[0]=d1;
        recData[1]=d2;
        
        //type, date, title, topic, link, time, criteria
        
        data.addSchedData("Holiday", "2017/2/9", "SNOW DAY", "http://funnybizblog.com/funny-stuff/calvin-hobbes-snowman-cartoons", "http://funnybizblog.com/funny-stuff/calvin-hobbes-snowman-cartoons", "", "");
        data.addSchedData("Lecture", "2017/2/4", "Lecture 3", "Event Programming", "http://funnybizblog.com/funny-stuff/calvin-hobbes-snowman-cartoons", "", "");
        data.addSchedData("Holiday", "2017/3/13", "Spring Break", "http://funnybizblog.com/funny-stuff/calvin-hobbes-snowman-cartoons", "http://funnybizblog.com/funny-stuff/calvin-hobbes-snowman-cartoons", "", "" );
        data.addSchedData("HWs", "2017/2/22", "HW1", "due @ 11:59pm", "none", "12:01 am", "none" );
        data.addSchedData("Recitation", "2017/3/9", "Recitation 1", "HWML/CSS", "", "", "" );
        
        
        data.setSchedStartMonthDate("1");
        data.setSchedStartDayDate("23");
        data.setSchedEndMonthDate("5");
        data.setSchedEndDayDate("19");
        
        ScheduleData s1= new ScheduleData("Holiday", "2/9/17", "SNOW DAY", " ", " ", " ", " ");
        ScheduleData s2= new ScheduleData("Lecture", "2/14/17", "Lecture 3", "Event Programming", " ", " ", " ");
        ScheduleData s3= new ScheduleData("Holiday", "3/13/17", "Spring Break", " ", " ", " ", " ");
        
        schedData[0]=s1;
        schedData[1]=s2;
        schedData[2]=s3;
        
        data.addProjData("Atomic Comics", "55221", "fff fff", "http://atomiccomic.com");
        data.addProjData("C4 Comics", "235339", "fff fff", "https://c4-comics.appspot.com");
        
        ProjectData p1= new ProjectData("Atomic Comics", "55221", "fff fff", "http://atomiccomic.com");
        ProjectData p2= new ProjectData("C4 Comics", "235339", "fff fff", "https://c4-comics.appspot.com");
        
        projData[0]=p1;
        projData[1]=p2;
        
        data.addProjStudentData("Beau", "Brummel", "Atomic Comics", "Lead Designer");
        data.addProjStudentData("Jane", "Doe", "C4 Comics", "Lead Programmer");
        data.addProjStudentData("Noonian", "Soong", "Atomic Comics", "Data Designer");
        
        ProjectStudentData psd1= new ProjectStudentData("Beau", "Brummel", "Atomic Comics", "Lead Designer");
        ProjectStudentData psd2= new ProjectStudentData("Jane", "Doe", "C4 Comics", "Lead Programmer");
        ProjectStudentData psd3= new ProjectStudentData("Noonian", "Soong", "Atomic Comics", "Data Designer");
        
        studentProjData[0]=psd1;
        studentProjData[1]=psd2;
        studentProjData[2]=psd3;
        //files.saveData(data, "C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\work\\SiteSaveTest3.json");
      
    
        
    }
    
    public static void main(String [] args) throws IOException{
       testSave2 test= new testSave2(new TAManagerApp());
       test.getFiles().saveData2(test.getData(), "C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManagerTester\\public_html\\js\\CourseInfoData.json",
               "C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManagerTester\\public_html\\js\\OfficeHoursData.json",
               "C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManagerTester\\public_html\\js\\RecitationData.json",
               "C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManagerTester\\public_html\\js\\ScheduleData.json",
               "C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManagerTester\\public_html\\js\\ProjectData.json");
               
    }
    
  
    
    
    public String getStartHour(){
        return this.startHour;
    }
    
    public String getSubject(){
        return this.subj;
    }
    
    public String getSemester(){
        return this.semester;
    }
    
    public String getYear(){
        return this.year;
    }
    
    public String getNumber(){
        return this.number;
    }
    
    public String getEndHour(){
        return this.endHour;
    }
    
    public String getTitle(){
        return this.title;
    }
    
    public String getName(){
        return this.teacherName;
    }
    
    public String getHome(){
        return this.homeAddress;
    }
    
    public String getExportDir(){
        return this.exportDir;
    }
    
    public String getTemplateDir(){
        return this.templateDir;
    }
    
    public boolean getHomeBool(){
        return this.home;
    }
    
    public boolean getSyllabusBool(){
        return this.syllabus;
    }
    
    public boolean getProjectsBool(){
        return this.projects;
    }
    
    public boolean getSchedBool(){
        return this.schedule;
    }
    
    public boolean getHWSBool(){
        return this.hws;
    }
    
    public String getBannerImage(){
        return this.bannerimage;
    }
    
    public String getRightFooter(){
        return this.rightfooterimage;
    }
    
    public String getLeftFooter(){
        return this.leftfooterimage;
    }
    
    public String getStyleSheet(){
        return this.stylesheet;
    }
    
    public TAFiles getFiles(){
        return files;
    }
    
    public TAData getData(){
        return data;
    }
    
    public TeachingAssistant[] getTeachingAssistants(){
        return this.teachingAssistants;
    }
    
    public TimeSlot[] getTimeSlots(){
        return this.timeSlots;
    }
    
    public RecitationData[] getRecData(){
        return this.recData;
    }
    
    public ScheduleData[] getSchedData(){
        return this.schedData;
    }
    
    public ProjectData[] getProjectData(){
        return this.projData;
    }
    
    public ProjectStudentData[] getStudentProjData(){
        return this.studentProjData;
    }
    
    
}
