package tam.data;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import djf.components.AppDataComponent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.TAManagerProp;
import tam.workspace.TAWorkspace;

/**
 * This is the data component for TAManagerApp. It has all the data needed to be
 * set by the user via the User Interface and file I/O can set and get all the
 * data from this object
 *
 * @author Richard McKenna
 */
public class TAData implements AppDataComponent {

    // WE'LL NEED ACCESS TO THE APP TO NOTIFY THE GUI WHEN DATA CHANGES
    TAManagerApp app;

    // NOTE THAT THIS DATA STRUCTURE WILL DIRECTLY STORE THE
    // DATA IN THE ROWS OF THE TABLE VIEW
    ObservableList<TeachingAssistant> teachingAssistants;
    
    ObservableList<RecitationData> recDataTable;
    ObservableList<ScheduleData> schedData;
    ObservableList<ProjectData> projData;
    ObservableList<String> teamNames;
    ObservableList<ProjectStudentData> projStudentData;
    ObservableList<CoursePage> coursePages;

    // THIS WILL STORE ALL THE OFFICE HOURS GRID DATA, WHICH YOU
    // SHOULD NOTE ARE StringProperty OBJECTS THAT ARE CONNECTED
    // TO UI LABELS, WHICH MEANS IF WE CHANGE VALUES IN THESE
    // PROPERTIES IT CHANGES WHAT APPEARS IN THOSE LABELS
    HashMap<String, StringProperty> officeHours;

    // THESE ARE THE LANGUAGE-DEPENDENT VALUES FOR
    // THE OFFICE HOURS GRID HEADERS. NOTE THAT WE
    // LOAD THESE ONCE AND THEN HANG ON TO THEM TO
    // INITIALIZE OUR OFFICE HOURS GRID
    ArrayList<String> gridHeaders;

    // THESE ARE THE TIME BOUNDS FOR THE OFFICE HOURS GRID. NOTE
    // THAT THESE VALUES CAN BE DIFFERENT FOR DIFFERENT FILES, BUT
    // THAT OUR APPLICATION USES THE DEFAULT TIME VALUES AND PROVIDES
    // NO MEANS FOR CHANGING THESE VALUES
    int startHour;
    int endHour;
    
    String subject="";
    String num="";
    String year=""; 
    String sem="";
    String titleTextField="";
    String exportDir="";
    File exportDir2=null;
    String templateDirectory="";
    String homeTextField="";
    String nameCourseTextField="";
    String styleSheet="";
    String bannerImage="";
    String rightFooterImage="";
    String leftFooterImage="";
    String exportBanner="";
    String exportRight="";
    String exportLeft="";
    Image img1=null;
    Image img2=null;
    Image img3=null;
    
    String teachingAssistantName;
    String teachingAssistantEmail;
    
    String thisStartHour;
    
    ArrayList<RecitationData> recData;
    
    boolean teachingAssistantCheckBox;
    
    boolean home;
    boolean syllabus;
    boolean schedule;
    boolean hws;
    boolean projects;
    
    public static final String INIT_STARTMONTH_DATE = "1";
    public static final String INIT_STARTDAY_DATE = "23";
    public static final String INIT_ENDDAY_DATE = "19";
    public static final String INIT_ENDMONTH_DATE = "5";
    public static final String INIT_END_YEAR = "2017";
    public static final String INIT_START_YEAR = "2017";
    
    public static final String END_DATE = "2017-05-19";
    public static final String START_DATE = "2017-01-23";
    
    String schedStartMonth;
    String schedEndMonth;
    String schedStartDay;
    String schedEndDay;
    String startYear;
    String endYear;
    String startDate;
    String endDate;
    

    // DEFAULT VALUES FOR START AND END HOURS IN MILITARY HOURS
    public static final int MIN_START_HOUR = 9;
    public static final int MAX_END_HOUR = 20;
    
    ArrayList<String> comboboxVals= new ArrayList();
    

    /**
     * This constructor will setup the required data structures for use, but
     * will have to wait on the office hours grid, since it receives the
     * StringProperty objects from the Workspace.
     *
     * @param initApp The application this data manager belongs to.
     */
    public TAData(TAManagerApp initApp){
        // KEEP THIS FOR LATER
        app = initApp;

        // CONSTRUCT THE LISTrayList(); OF TAs FOR THE TABLE
        teachingAssistants = FXCollections.observableArrayList();
        recDataTable=FXCollections.observableArrayList();
        schedData=FXCollections.observableArrayList();
        projStudentData=FXCollections.observableArrayList();
        projData=FXCollections.observableArrayList();
        teamNames=FXCollections.observableArrayList();
        coursePages=FXCollections.observableArrayList();

        // THESE ARE THE DEFAULT OFFICE HOURS
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        
        //DEFAULT START DATES
        schedStartMonth=INIT_STARTMONTH_DATE;
        schedEndMonth=INIT_ENDMONTH_DATE;
        schedStartDay= INIT_STARTDAY_DATE;
        schedEndDay= INIT_ENDDAY_DATE;
        startYear=INIT_START_YEAR;
        endYear=INIT_END_YEAR;
        startDate=START_DATE;
        endDate=END_DATE;

        //THIS WILL STORE OUR OFFICE HOURS
        officeHours = new HashMap();

        // THESE ARE THE LANGUAGE-DEPENDENT OFFICE HOURS GRID HEADERS
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> timeHeaders = props.getPropertyOptionsList(TAManagerProp.OFFICE_HOURS_TABLE_HEADERS);
        ArrayList<String> dowHeaders = props.getPropertyOptionsList(TAManagerProp.DAYS_OF_WEEK);
        gridHeaders = new ArrayList();
        gridHeaders.addAll(timeHeaders);
        gridHeaders.addAll(dowHeaders);
        
        comboboxVals.add("ISE");
        comboboxVals.add("Spring");
        comboboxVals.add("220");
        comboboxVals.add("2016");
    }

    /**
     * Called each time new work is created or loaded, it resets all data and
     * data structures such that they can be used for new values.
     */
    @Override
    public void resetData() {
        startHour = MIN_START_HOUR;
        endHour = MAX_END_HOUR;
        teachingAssistants.clear();
        officeHours.clear();
    }
    

    // ACCESSOR METHODS
    public int getStartHour() {
        return startHour;
    }

    public int getEndHour() {
        return endHour;
    }

    public void setStartHour(int startTime) {
        startHour = startTime;
    }

    public void setEndHour(int endTime) {
        endHour = endTime;
    }

    public ArrayList<String> getGridHeaders() {
        return gridHeaders;
    }

    public ObservableList getTeachingAssistants() {
        return teachingAssistants;
    }
    
    public ObservableList getRecDataTable(){
        return recDataTable;
    }
    
    public Image getImageView1(){
        return img1;
    }
    
    public Image getImageView2(){
        return img2;
    }
    
    public Image getImageView3(){
        return img3;
    }
    
    public String getStartYear(){
        return startYear;
    }
    
    public String getEndYear(){
        return endYear;
    }
    
    public void setStartYear(String s){
        startYear=s;
    }
    
    public void setEndYear(String s){
        endYear=s;
    }
    
    public void setImageView3(Image x){
        img3=x;
    }
    
    public void setBannerExport(String x){
        exportBanner=x;
    }
    
     public void setRightExport(String x){
        exportRight=x;
    }
     
      public void setLeftExport(String x){
        exportLeft=x;
    }
      
       public String getBannerExport(){
        return exportBanner;
    }
         public String getLeftExport(){
        return exportLeft;
    }
         
       public String getRightExport(){
        return exportRight;
    }
    
     public void setImageView2(Image x){
        img2=x;
    }
     
      public void setImageView1(Image x){
        img1=x;
    }
    
    public ObservableList getSchedData(){
        return schedData;
    }
    
    public ObservableList getCourseData(){
        return coursePages;
    }
    
    public String getStartDate(){
        return startDate;
    }
    
    public void setStartDate(String s){
        startDate=s;
    }
    
    public String getEndDate(){
        return endDate;
    }
    
    public void setEndDate(String s){
        endDate=s;
    }
    
    public ObservableList getProjData(){
        return projData;
    }
    
    public ObservableList getProjStudentData(){
        return projStudentData;
    }

    public String getCellKey(int col, int row) {
        return col + "_" + row;
    }

    public StringProperty getCellTextProperty(int col, int row) {
        String cellKey = getCellKey(col, row);
        return officeHours.get(cellKey);
    }

    public HashMap<String, StringProperty> getOfficeHours() {
        return officeHours;
    }

    public int getNumRows() {
        return ((endHour - startHour) * 2) + 1;
    }

    public String getTimeString(int militaryHour, boolean onHour) {
        String minutesText = "00";
        if (!onHour) {
            minutesText = "30";
        }

        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutesText;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }

    public String getCellKey(String day, String time) {
        int col = gridHeaders.indexOf(day);
        int row = 1;
        int hour = Integer.parseInt(time.substring(0, time.indexOf("_")));
        int milHour = hour;
        if (hour < startHour) {
            milHour += 12;
        }
        row += (milHour - startHour) * 2;
        if (time.contains("_30")) {
            row += 1;
        }
        return getCellKey(col, row);
    }

    public TeachingAssistant getTA(String testName) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName)) {
                return ta;
            }
        }
        return null;
    }

    /**
     * This method is for giving this data manager the string property for a
     * given cell.
     */
    public void setCellProperty(int col, int row, StringProperty prop) {
        String cellKey = getCellKey(col, row);
        officeHours.put(cellKey, prop);
    }

    /**
     * This method is for setting the string property for a given cell.
     */
    public void setGridProperty(ArrayList<ArrayList<StringProperty>> grid,
            int column, int row, StringProperty prop) {
        grid.get(row).set(column, prop);
    }

    /*
    * THis method updates the time for office hours 
     */
    public void updateTime(int newStartHour, int newEndHour) {
        HashMap<String, StringProperty> newOfficeHours;
        newOfficeHours = new HashMap();
        TAWorkspace workspaceComponent = (TAWorkspace) app.getWorkspaceComponent();
        int shiftIndex = (newEndHour - newStartHour) * 2 + 1;
        int oldStartRow = (newStartHour - startHour) * 2 + 1;
        int newStartRow = 1;
        if (newStartHour >= startHour && newEndHour <= endHour) // IF New Time is 10 and old time is 8.  New End is 16 , old end is 20         && newEndHour<=endHour
        {

            for (int col = 2; col < 7; col++) {
                for (int i = 0; i < ((newEndHour - newStartHour) * 2); i++) {

                    int row = oldStartRow;
                    String cellKey = col + "_" + row;
                    StringProperty prop = officeHours.get(cellKey);
                    String newCellKey = col + "_" + newStartRow;
                    newOfficeHours.put(newCellKey, prop);
                    oldStartRow++;
                    newStartRow++;

                }
                oldStartRow = (newStartHour - startHour) * 2 + 1;
                newStartRow = 1;
            }
        } else if (newStartHour >= startHour && newEndHour > endHour) {

            for (int col = 2; col < 7; col++) {
                for (int i = 0; i < ((endHour - newStartHour) * 2); i++) {

                    int row = oldStartRow;
                    String cellKey = col + "_" + row;
                    StringProperty prop = officeHours.get(cellKey);
                    String newCellKey = col + "_" + newStartRow;
                    newOfficeHours.put(newCellKey, prop);
                    oldStartRow++;
                    newStartRow++;

                }
                oldStartRow = (newStartHour - startHour) * 2 + 1;
                newStartRow = 1;
            }
        } else if (startHour > newStartHour && endHour >= newEndHour) {
            oldStartRow = 1;
            newStartRow = (startHour - newStartHour) * 2 + 1;
            for (int col = 2; col < 7; col++) {
                for (int i = 0; i < ((newEndHour - startHour) * 2); i++) {

                    int row = oldStartRow;
                    String cellKey = col + "_" + row;
                    StringProperty prop = officeHours.get(cellKey);
                    String newCellKey = col + "_" + newStartRow;
                    newOfficeHours.put(newCellKey, prop);
                    oldStartRow++;
                    newStartRow++;

                }
                oldStartRow = 1;
                newStartRow = (startHour - newStartHour) * 2 + 1;
            }
        } else if (startHour > newStartHour && newEndHour > endHour) {
            oldStartRow = 1;
            newStartRow = (startHour - newStartHour) * 2 + 1;
            for (int col = 2; col < 7; col++) {
                for (int i = 0; i < ((endHour - startHour) * 2); i++) {

                    int row = oldStartRow;
                    String cellKey = col + "_" + row;
                    StringProperty prop = officeHours.get(cellKey);
                    String newCellKey = col + "_" + newStartRow;
                    newOfficeHours.put(newCellKey, prop);
                    oldStartRow++;
                    newStartRow++;

                }
                oldStartRow = 1;
                newStartRow = (startHour - newStartHour) * 2 + 1;
            }
        }

        workspaceComponent.resetWorkspace();
        initOfficeHoursUpdate(newStartHour, newEndHour);

        for (HashMap.Entry<String, StringProperty> entry : newOfficeHours.entrySet()) {
            String key = entry.getKey();
            StringProperty prop = newOfficeHours.get(key);
            toggleTAOfficeHoursUpdate(key, prop);
        }
    }

    private void initOfficeHours(int initStartHour, int initEndHour) {
        // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
        startHour = initStartHour;
        endHour = initEndHour;

        // EMPTY THE CURRENT OFFICE HOURS VALUES
        officeHours.clear();
        // WE'LL BUILD THE USER INTERFACE COMPONENT FOR THE
        // OFFICE HOURS GRID AND FEED THEM TO OUR DATA
        // STRUCTURE AS WE GO
        TAWorkspace workspaceComponent = (TAWorkspace) app.getWorkspaceComponent();
        workspaceComponent.reloadOfficeHoursGrid(this);
    }

    private void initOfficeHoursUpdate(int initStartHour, int initEndHour) {
        // NOTE THAT THESE VALUES MUST BE PRE-VERIFIED
        startHour = initStartHour;
        endHour = initEndHour;

        // EMPTY THE CURRENT OFFICE HOURS VALUES
        officeHours.clear();

        // WE'LL BUILD THE USER INTERFACE COMPONENT FOR THE
        // OFFICE HOURS GRID AND FEED THEM TO OUR DATA
        // STRUCTURE AS WE GO
        TAWorkspace workspaceComponent = (TAWorkspace) app.getWorkspaceComponent();
        workspaceComponent.reloadOfficeHoursGrid(this);
    }

    public void initHours(String startHourText, String endHourText) {
        int initStartHour = Integer.parseInt(startHourText);
        int initEndHour = Integer.parseInt(endHourText);
        if ((initStartHour >= MIN_START_HOUR)
                && (initEndHour <= MAX_END_HOUR)
                && (initStartHour <= initEndHour)) {
            // THESE ARE VALID HOURS SO KEEP THEM
            initOfficeHours(initStartHour, initEndHour);
        }
    }
  

    public boolean containsTA(String testName, String testEmail) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName)) {
                return true;
            }
            if (ta.getEmail().equals(testEmail)) {
                return true;
            }
        }
        return false;
        
    }
    
    public TeachingAssistant getTA(String testName, String testEmail) {
        TeachingAssistant taToReturn=null;
        for (TeachingAssistant ta : teachingAssistants) {
            if (ta.getName().equals(testName) && ta.getEmail().equals(testEmail)) {
                taToReturn=ta;
        }
        
    }
        return taToReturn;
    }
    
    public ScheduleData getSched(String type, String date, String title, String topic, String link, String time, String criteria) {
        ScheduleData taToReturn=null;
        for (ScheduleData sched : schedData) {
            String z=sched.getType();
            String d=sched.getTitle();
            String s=sched.getDate();
            String q=sched.getTopic();
            String a=sched.getLink();
            String w=sched.getCriteria();
            String t=sched.getTime();
            System.out.println(z + " " + type);
            System.out.println(d+ " " + title);
            System.out.println(s + " " + date);
            System.out.println(q + " " +topic);
            System.out.println(a + " "+link);
            System.out.println(w+ " " + criteria);
            System.out.println(t + " "+ time);
            if (sched.getType().equals(type) && sched.getDate().equals(date) && sched.getTitle().equals(title) &&
                    sched.getTopic().equals(topic) && sched.getLink().equals(link) && sched.getTime().equals(time) &&
                    sched.getCriteria().equals(criteria)) {
                taToReturn=sched;
        }
        
    }
        return taToReturn;
    }
    
    public void addSchedData(String type, String date, String title, String topic, String link, String time, String criteria){
        System.out.println(type+" "+ date+" "+title +" "+topic+" " +link+" " +time+" "+criteria);
        ScheduleData sched= new ScheduleData(type, date, title, topic, link, time, criteria);
        if(!containsSched(type, date, title, topic, link, time, criteria)){
        schedData.add(sched);
    }
    }
    
    
    
    public boolean containsSched(String type, String date, String title, String topic, String link, String time, String criteria) {
        for (ScheduleData sched : schedData) {
            if (sched.getType().equals(type) && sched.getDate().equals(date) &&sched.getTitle().equals(title) &&
                    sched.getTopic().equals(topic) && sched.getLink().equals(link) &&sched.getTime().equals(time) &&sched.getCriteria().equals(criteria)) {
                return true;
            }
        }
        return false;
        
    }
    
    public void addProjData(String name, String color, String textColor, String link){
        TAWorkspace workspaceComponent = (TAWorkspace) app.getWorkspaceComponent();
        ProjectData proj= new ProjectData(name, color, textColor, link);
        if(!containsProj(name, color, textColor, link)){
        projData.add(proj);
        teamNames.add(name);
    }
    }
    
    public boolean containsProj(String name, String color, String textColor, String link) {
        for (ProjectData proj : projData) {
            
            if(proj.getLink().equals(link) &&proj.getName().equals(name) && proj.getColorHex().equals(color) &&proj.getTxtColor().equals(textColor)){
                return true;
            }
             
        }
        return false;
        
    }
    
     public void removeTeam(){
        TAWorkspace workspaceComponent = (TAWorkspace) app.getWorkspaceComponent();
        workspaceComponent.getStudentProjTeamText().getItems().clear();
        for(ProjectData x: projData){
            workspaceComponent.getStudentProjTeamText().getItems().add(x);
        }
    }
    
    public void removeProjData(String name, String color, String textColor, String link){
        for(ProjectData x: projData){
            if(x.getName().equals(name)&& x.getColorHex().equals(color) && x.getTxtColor().equals(textColor) && x.getLink().equals(link)){
                projData.remove(x);
                return;
            }
        }
    }
    
    
    public ProjectData getProjData(String name, String color, String textColor, String link){
        for(ProjectData x: projData){
            if(x.getName().equals(name)&& x.getColorHex().equals(color) && x.getTxtColor().equals(textColor) && x.getLink().equals(link)){
                return x;
            }
        }
        return null;
    }
    
    public void addProjStudentData(String firstname, String lastname, String team, String role){
        ProjectStudentData proj= new ProjectStudentData(firstname, lastname, team, role);
        if(!containsStudentProj(firstname, lastname, team, role)){
        projStudentData.add(proj);
        }
    }
    
    public void addRecitation(String section, String instructor, String dayTime, String location,
            String TA1, String TA2){
        
        RecitationData rec= new RecitationData(section, instructor, dayTime, location, TA1, TA2);
        if(!containsRec(section, instructor, dayTime, location, TA1, TA2)){
        recDataTable.add(rec);
        }
    }
    
    public boolean containsRec(String section, String instructor, String dayTime, String location,
            String TA1, String TA2) {
        for (RecitationData rec : recDataTable) {
            
            if(rec.getSection().equals(section) && rec.getDayAndTime().equals(dayTime) &&rec.getInstructor().equals(instructor)
                    && rec.getLocation().equals(location) &&rec.getTA1().equals(TA1) &&rec.getTA2().equals(TA2)){
                return true;
            }
            
        }
        return false;
        
    }
    
    public boolean containsStudentProj(String firstname, String lastname, String team, String role) {
        for (ProjectStudentData proj : projStudentData) {
            
            if(proj.getFirstName().equals(firstname) && proj.getLastName().equals(lastname)){
                return true;
            }
            
            else if(proj.getFirstName().equals(firstname) &&proj.getLastName().equals(lastname) && proj.getTeam().equals(team) && proj.getRole().equals(role)){
                   return true;
        }
        }
        return false;
        
    }
    
    public RecitationData getREC(String section, String instructor, String dayTime, String location,
            String TA1, String TA2) {
        for (RecitationData rec : recDataTable) {
            if (rec.getDayAndTime().equals(dayTime) && rec.getInstructor().equals(instructor) && rec.getLocation().equals(location)&&
                    rec.getSection().equals(section) && rec.getTA1().equals(TA1) && rec.getTA2().equals(TA2)) {
                return rec;
            }
        }
        return null;
    }

    public void addTA(String initName, String initEmail, boolean checkbox) {
        // MAKE THE TA
        TeachingAssistant ta = new TeachingAssistant(initName, initEmail, checkbox);

        // ADD THE TA
        if (!containsTA(initName, initEmail)) {
            teachingAssistants.add(ta);
        }

        // SORT THE TAS
        Collections.sort(teachingAssistants);
    }

    public void removeTA(String name) {
        for (TeachingAssistant ta : teachingAssistants) {
            if (name.equals(ta.getName())) {
                teachingAssistants.remove(ta);
                return;
            }
        }
    }
    
    public void removeRec(String section, String instructor, String dayTime, String location,
            String TA1, String TA2) {
        for (RecitationData rec : recDataTable) {
            if (section.equals(rec.getSection()) && instructor.equals(rec.getInstructor()) && dayTime.equals(rec.getDayAndTime()) &&
                    location.equals(rec.getLocation()) && TA1.equals(rec.getTA1()) && TA2.equals(rec.getTA2())) {
                recDataTable.remove(rec);
                System.out.println("hi");
                return;
            }
        }
    }
    
    public ProjectStudentData getStudentProj(String firstname, String lastname, String team, String role) {
        for (ProjectStudentData proj : projStudentData) {
            if(proj.getFirstName().equals(firstname) && proj.getLastName().equals(lastname) && proj.getTeam().equals(team) && proj.getRole().equals(role)){
                return proj;
            }
            
        }
        return null;
        
    }
    
    public void removeStudentProj(String firstname, String lastname, String team, String role) {
        for (ProjectStudentData proj : projStudentData) {
            if(proj.getFirstName().equals(firstname) && proj.getLastName().equals(lastname) && proj.getTeam().equals(team) && proj.getRole().equals(role)){
                projStudentData.remove(proj);
                return;
            }
            
        }
        
        
    }
    
     public void removeSched(String type, String date, String title, String topic, String link, String time, String criteria) {
        for (ScheduleData sched : schedData) {
            if (sched.getType().equals(type) && sched.getDate().equals(date) && sched.getTitle().equals(title) &&
                    sched.getTopic().equals(topic) && sched.getLink().equals(link) && sched.getTime().equals(time) &&
                    sched.getCriteria().equals(criteria)) {
                schedData.remove(sched);
                return;
        }
        
    }
        
    }

    public void addOfficeHoursReservation(String day, String time, String taName) {
        String cellKey = getCellKey(day, time);
        toggleTAOfficeHours(cellKey, taName);
    }

    /**
     * This function toggles the taName in the cell represented by cellKey.
     * Toggle means if it's there it removes it, if it's not there it adds it.
     */
    public void toggleTAOfficeHours(String cellKey, String taName) {
        StringProperty cellProp = officeHours.get(cellKey);
        String cellText = cellProp.getValue();

        // IF IT ALREADY HAS THE TA, REMOVE IT
        if (cellText.contains(taName)) {
            removeTAFromCell(cellProp, taName);
        } // OTHERWISE ADD IT
        else if (cellText.length() == 0) {
            cellProp.setValue(taName);
        } else {
            cellProp.setValue(cellText + "\n" + taName);
        }
    }

    public void toggleTAOfficeHoursUpdate(String cellKey, StringProperty prop) {
        String cellText2 = prop.getValue();
        StringProperty cellProp = officeHours.get(cellKey);
        //String cellText = cellProp.getValue();
        cellProp.set(cellText2);

    }

    /**
     * This method removes taName from the office grid cell represented by
     * cellProp.
     */
    public void removeTAFromCell(StringProperty cellProp, String taName) {
        // GET THE CELL TEXT
        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(taName)) {
            cellProp.setValue("");
        } // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(taName) == 0) {
            int startIndex = cellText.indexOf("\n") + 1;
            cellText = cellText.substring(startIndex);
            cellProp.setValue(cellText);
        } // IS IT IN THE MIDDLE OF A LIST OF TAs
        else if (cellText.indexOf(taName) < cellText.indexOf("\n", cellText.indexOf(taName))) {
            int startIndex = cellText.indexOf("\n" + taName);
            int endIndex = startIndex + taName.length() + 1;
            cellText = cellText.substring(0, startIndex) + cellText.substring(endIndex);
            cellProp.setValue(cellText);
        } // IT MUST BE THE LAST TA
        else {
            int startIndex = cellText.indexOf("\n" + taName);
            cellText = cellText.substring(0, startIndex);
            cellProp.setValue(cellText);
        }
    }
    
    public void setSubject(String s){
        this.subject=s;
        
    }
    
    public String getSubj(){
        return this.subject;
    }
    
    public void setSem(String s){
       this.sem=s;
    }
    
    public String getSem(){
        return this.sem;
    }
    
    public void setLeftFooterImage(String s){
        this.leftFooterImage=s;
    }
    
    public void setRecData(ArrayList<RecitationData> data){
        this.recData=data;
    }
    
    public ArrayList<RecitationData> getRecData(){
        return this.recData;
    }
    
    public String getLeftFooterImage(){
        return leftFooterImage;
    }
    
    public void setRightFooterImage(String s){
        this.rightFooterImage=s;
    }
    
    public String getRightFooterImage(){
        return this.rightFooterImage;
    }
    
    public void setBannerImage(String s){
        this.bannerImage=s;
    }
    
    public String getBannerImage(){
        return this.bannerImage;
    }
    
    public void setNum(String s){
        this.num=s;
    }
    
    public void setThisStartHour(String s){
        this.thisStartHour=s;
    }
    
    public String getNum(){
        return this.num;
    }
    
    public void setYear(String s){
        this.year=s;
    }
    
    public String getYear(){
        return this.year;
    }
    
    public void setTeachingAssistantName(String name){
        this.teachingAssistantName=name;
    }
    
    public String getTeachingAssistantName(){
        return this.teachingAssistantName;
    }
    
    
    public void setTeachingAssistantEmail(String email){
        this.teachingAssistantEmail=email;
    }
    
    public String getTeachingAssistantEmail(){
        return this.teachingAssistantEmail;
    }
    
    public void setTeachingAssistantCheckBox(boolean c){
        this.teachingAssistantCheckBox=c;
    }
    
    public void setSchedStartMonthDate(String s){
        this.schedStartMonth=s;
    }
    
    public void setSchedStartDayDate(String s){
        this.schedStartDay=s;
    }
    
    public void setSchedEndDayDate(String e){
        this.schedEndDay=e;
    }
    
    public void setSchedEndMonthDate(String e){
        this.schedEndMonth=e;
    }
    
    public String getSchedEndMonthDate(){
        return schedEndMonth;
    }
    
    public String getSchedEndMonthDay(){
        return this.schedEndDay;
    }
    
    public String getSchedStartDayDate(){
        return this.schedStartDay;
    }
    
    public String getSchedStartMonthDate(){
        return this.schedStartMonth;
    }
    
    public boolean getTeachingAssistantCheckBox(){
        return this.teachingAssistantCheckBox;
    }
    
    public void setTitleTextField(String s){
        this.titleTextField=s;
    }
    
    public String getTitleTextField(){
        return this.titleTextField;
    }
    
    public void setExportDir(String s){
       this.exportDir=s;
    }
    
    public void setExportDir2(File s){
       this.exportDir2=s;
    }
    
    public File getExportDir2(){
       return exportDir2;
    }
    
    public String getExportDir(){
        return this.exportDir;
    }
    
    public void setTemplateDir(String s){
        this.templateDirectory=s;
    }
    
    public ObservableList getTeamNames(){
        return teamNames;
    }
    
    public String getTemplateDir(){
        return this.templateDirectory;
    }
    
    public void setHomeTextField(String s){
        this.homeTextField=s;
    }
    
    public String getHomeTextField(){
        return this.homeTextField;
    }
    
    public void setNameCourseTextField(String s){
       this.nameCourseTextField=s;
    }
    
    public String getNameCourseTextField(){
        return this.nameCourseTextField;
    }
    
    public void setHome(boolean h){
        this.home=h;
    }
    
    public boolean getHome(){
        return this.home;
    }
    
    public void setSyllabus(boolean s){
        this.syllabus=s;
    }
    
    public boolean getSyllabus(){
        return this.syllabus;
    }
    
    public void setSchedule(boolean s){
        this.schedule=s;
    }
    
    public void setStyleSheet(String s){
        this.styleSheet=s;
    }
    
    public String getStyleSheet(){
        return this.styleSheet;
    }
    
    public boolean getSchedule(){
        return this.schedule;
    }
    
    public void setHWS(boolean h){
        this.hws=h;
    }
    
    public boolean getHWS(){
        return this.hws;
    }
    
    public void setProjects(boolean p){
        this.projects=p;
    }
    
    public boolean getProjects(){
        return this.projects;
    }

    public void renameTaCell(StringProperty cellProp, String taName, String newName) {
        // GET THE CELL TEXT
        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(taName)) {
            cellProp.setValue(newName);
        } // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(taName) == 0) {
            int startIndex = cellText.indexOf("\n") + 1;
            cellText = cellText.substring(startIndex);
            cellProp.setValue(cellText + "\n" + newName);
        } // IS IT IN THE MIDDLE OF A LIST OF TAs
        else if (cellText.indexOf(taName) < cellText.indexOf("\n", cellText.indexOf(taName))) {
            int startIndex = cellText.indexOf("\n" + taName);
            int endIndex = startIndex + taName.length() + 1;
            cellText = cellText.substring(0, startIndex) + cellText.substring(endIndex);
            cellProp.setValue(cellText + "\n" + newName);
        } // IT MUST BE THE LAST TA
        else {
            int startIndex = cellText.indexOf("\n" + taName);
            cellText = cellText.substring(0, startIndex);
            cellProp.setValue(cellText + "\n" + newName);
        }
    }

    public void updateTaCell(StringProperty cellProp, String taName, String newName) {
        // GET THE CELL TEXT
        String cellText = cellProp.getValue();
        // IS IT THE ONLY TA IN THE CELL?
        if (cellText.equals(taName)) {
            cellProp.setValue("");
        } // IS IT THE FIRST TA IN A CELL WITH MULTIPLE TA'S?
        else if (cellText.indexOf(taName) == 0) {
            int startIndex = cellText.indexOf("\n") + 1;
            cellText = cellText.substring(startIndex);
            cellProp.setValue(newName + cellText);
        } // IS IT IN THE MIDDLE OF A LIST OF TAs
        else if (cellText.indexOf(taName) < cellText.indexOf("\n", cellText.indexOf(taName))) {
            int startIndex = cellText.indexOf("\n" + taName);
            int endIndex = startIndex + taName.length() + 1;
            cellText = cellText.substring(0, startIndex) + cellText.substring(endIndex);
            cellProp.setValue(cellText);
        } // IT MUST BE THE LAST TA
        else {
            int startIndex = cellText.indexOf("\n" + taName);
            cellText = cellText.substring(0, startIndex);
            cellProp.setValue(cellText);
        }
    }
}
