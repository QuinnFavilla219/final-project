/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */
public class RecitationData {
    private final StringProperty section;
    private final StringProperty instructor;
    private final StringProperty dayAndTime;
    private final StringProperty location;
    private final StringProperty TA1;
    private final StringProperty TA2;
    
    public RecitationData(String thisSection, String thisInstructor, String thisDayAndTime, String thisLocation,
            String thisTA1, String thisTA2){
        section= new SimpleStringProperty(thisSection);
        instructor= new SimpleStringProperty(thisInstructor);
        dayAndTime=new SimpleStringProperty(thisDayAndTime);
        location= new SimpleStringProperty(thisLocation);
        TA1= new SimpleStringProperty(thisTA1);
        TA2= new SimpleStringProperty(thisTA2);
        
    }
    
    public String getSection() {
        return section.get();
    }
    
     public void setSection(String thisSection) {
        section.set(thisSection);
    }

    public String getInstructor() {
        return instructor.get();
    }
    
    public void setInstructor(String thisInstructor) {
        instructor.set(thisInstructor);
    }

    public String getDayAndTime() {
       return dayAndTime.get();
    }
    
    public void setDayAndTime(String thisDayAndTime) {
        dayAndTime.set(thisDayAndTime);
    }
    
    public String getLocation(){
        return location.get();
    }
    
    public void setLocation(String thisLocation) {
        location.set(thisLocation);
    }
    
    public String getTA1(){
        return TA1.get();
    }
    
    public void setTA1(String thisTA1) {
        TA1.set(thisTA1);
    }
    
    public String getTA2(){
        return TA2.get();
    }
    
    public void setTA2(String thisTA2) {
        TA2.set(thisTA2);
    }
}
