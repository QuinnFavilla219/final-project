/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */

/**
 * This class represents a Teaching Assistant for the table of TAs.
 * 
 * @author Richard McKenna
 */
public class CoursePage {
    // THE TABLE WILL STORE TA NAMES AND EMAILS
    private final StringProperty navBarTitle;
    private final StringProperty fileName;
    private final StringProperty script;
    private final BooleanProperty checkBox;
    /**
     * Constructor initializes both the TA name and email.
     */
    public CoursePage(boolean check, String navTitle, String file, String scr) {
        checkBox= new SimpleBooleanProperty(check);
        navBarTitle = new SimpleStringProperty(navTitle);
        fileName = new SimpleStringProperty(file);
        script= new SimpleStringProperty(scr);
    }

    // ACCESSORS AND MUTATORS FOR THE PROPERTIES
    
    public BooleanProperty getCheckBox(){
        return checkBox;
    }
    
    public void setCheckBox(boolean check){
        checkBox.set(check);
    }

    public String getNavBarTitle() {
        return navBarTitle.get();
    }
    
    
    public void setNavBarTitle(String navTitle){
        navBarTitle.set(navTitle);
    }

    public String getFileName() {
        return fileName.get();
    }
    
    public void setFileName(String file){
        fileName.set(file);
    }

    public String getScript() {
       return script.get();
    }
    
    public void setScript(String scr){
        script.set(scr);
    }

    @Override
    public String toString() {
        return navBarTitle.getValue();
    }

    
}
