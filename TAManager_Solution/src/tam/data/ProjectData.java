/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */
public class ProjectData {
    private final StringProperty name;
    private final StringProperty colorHex;
    private final StringProperty txtColor;
    private final StringProperty link;
    
    public ProjectData(String n, String c, String t, String l){
        name = new SimpleStringProperty(n);
        colorHex = new SimpleStringProperty(c);
        txtColor= new SimpleStringProperty(t);
        link= new SimpleStringProperty(l);
    }
    
    public String getName() {
        return name.get();
    }
    
    public void setName(String n){
        name.set(n);
    }

    public String getColorHex() {
        return colorHex.get();
    }
    
    public void setColorHex(String c){
        colorHex.set(c);
    }

    public String getTxtColor() {
       return txtColor.get();
    }
    
    public void setTxtColor(String t){
        txtColor.set(t);
    }
    
    public String getLink(){
        return link.get();
    }
    
    public void setLink(String l){
        link.set(l);
    }
 
    
}
