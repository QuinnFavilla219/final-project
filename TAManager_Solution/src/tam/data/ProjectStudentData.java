/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */
public class ProjectStudentData {
    private final StringProperty firstName;
    private final StringProperty lastName;
    private final StringProperty team;
    private final StringProperty role;
            
    public ProjectStudentData(String f, String l, String t, String r){
        firstName = new SimpleStringProperty(f);
        lastName = new SimpleStringProperty(l);
        team= new SimpleStringProperty(t);
        role= new SimpleStringProperty(r);
    }
    
    public String getFirstName() {
        return firstName.get();
    }
    
    public void setFirstName(String f){
        firstName.set(f);
    }

    public String getLastName() {
        return lastName.get();
    }
    
    public void setLastName(String l){
        lastName.set(l);
    }

    public String getTeam() {
       return team.get();
    }
    
    public void setTeam(String t){
        team.set(t);
    }
    public String getRole(){
        return role.get();
    }
    
    public void setRole(String r){
        role.set(r);
    }
    
}


