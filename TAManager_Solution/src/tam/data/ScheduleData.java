/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.data;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */
public class ScheduleData {
    private final StringProperty type;
    private final StringProperty date;
    private final StringProperty title;
    private final StringProperty topic;
    private final StringProperty link;
    private final StringProperty time;
    private final StringProperty criteria;
    
     public ScheduleData(String typ, String D, String T, String top, String l, String tim, String c) {
        type = new SimpleStringProperty(typ);
        date = new SimpleStringProperty(D);
        title= new SimpleStringProperty(T);
        topic= new SimpleStringProperty(top);
        link=new SimpleStringProperty(l);
        time= new SimpleStringProperty(tim);
        criteria= new SimpleStringProperty(c);
    }
     
    public String getType() {
        return type.get();
    }
    
    
    public void setTyle(String t){
        type.set(t);
    }

    public String getDate() {
        return date.get();
    }
    
    public void setDate(String d){
        date.set(d);
    }

    public String getTitle() {
       return title.get();
    }
    
    public void setTitle(String t){
        title.set(t);
    }
    
    public String getTopic(){
        return topic.get();
    }
    
    public void setTopic(String top){
        topic.set(top);
    }
    
    public void setLink(String l){
        link.set(l);
    }
    
    public String getLink(){
        return link.get();
    }
    
    public void setTime(String t){
        time.set(t);
    }
    
    public String getTime(){
        return time.get();
    }
    
    public void setCriteria(String c){
        criteria.set(c);
    }
    
    public String getCriteria(){
        return criteria.get();
    }
    
 
    
}
