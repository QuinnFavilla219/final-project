package tam.workspace;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import tam.data.ProjectData;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class RemoveTeam_Trans implements jTPS_Transaction {

    private String txtColor;
    private String hexColor;
    private String link;
    private String name;
   
    private TAData dataManager;
    private TAWorkspace workspace;

    public RemoveTeam_Trans(String teamName, String color, String textColor, String link1, TAData taData, TAWorkspace work) {
        txtColor=textColor;
        hexColor=color;
        link=link1;
        name=teamName;
        dataManager=taData;
        workspace=work;
    }

    @Override
    public void doTransaction() {  //Control Y 
       System.out.println("undo Transaction");
       ObservableList<String> y=FXCollections.observableArrayList();
        ObservableList<ProjectData> projData = dataManager.getProjData();
        for(ProjectData x: projData){
            if(x.getName().equals(name)&& x.getColorHex().equals(hexColor) && x.getTxtColor().equals(txtColor) && x.getLink().equals(link)){
                projData.remove(x);
                dataManager.getTeamNames().remove(x.getName());
                return;
            }
        }
        
   
        workspace.getProjLinkTextField().clear();
        workspace.getProjNameTextField().clear();
        workspace.getColorPicker1().setValue(Color.WHITE);
        workspace.getColorPicker2().setValue(Color.WHITE);
        
    }
       
    
    

    @Override
    public void undoTransaction() {  //Control Z 
         dataManager.addProjData(name, hexColor, txtColor, link);
         

        workspace.getProjLinkTextField().clear();
        workspace.getProjNameTextField().clear();
        workspace.getColorPicker1().setValue(Color.WHITE);
        workspace.getColorPicker2().setValue(Color.WHITE);
        // data.removeTA(taName);

}
}