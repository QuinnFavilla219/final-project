/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.Collections;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import jtps.jTPS_Transaction;
import tam.data.RecitationData;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class DeleteRec_Trans implements jTPS_Transaction {

    private String sec;
    private String inst;
    private String tim;
    private String loc;
    private String ta1;
    private String ta2;
    private TAData dataManager;
    private TAWorkspace workspace;

    public DeleteRec_Trans(String section, String instructor, String time, String location, String teach1, String teach2, TAData taData, TAWorkspace work) {
        sec=section;
        inst=instructor;
        tim=time;
        loc=location;
        ta1=teach1;
        ta2=teach2;
        dataManager=taData;
        workspace=work;
    }

    @Override
    public void doTransaction() {  //Control Y 
       System.out.println("do Transaction");
        ObservableList<RecitationData> recList = dataManager.getRecDataTable();
        for (RecitationData rec : recList) {
            if (sec.equals(rec.getSection()) && inst.equals(rec.getInstructor()) && tim.equals(rec.getDayAndTime())
                    && loc.equals(rec.getLocation()) && ta1.equals(rec.getTA1()) && ta2.equals(rec.getTA2())) {
                recList.remove(rec);
                return;
            }

        }
        workspace.getSection().clear();
        workspace.getInstructor().clear();
        workspace.getLocation().clear();
        workspace.getTime().clear();
        workspace.superTACombo1.setPromptText("TA1");
        workspace.superTACombo2.setPromptText("TA2");
    }

    @Override
    public void undoTransaction() {  //Control Z 
        dataManager.addRecitation(sec, inst, tim, loc, ta1, ta2);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("undoTransaction");
        
        workspace.getSection().clear();
        workspace.getInstructor().clear();
        workspace.getLocation().clear();
        workspace.getTime().clear();
        workspace.superTACombo1.setPromptText("TA1");
        workspace.superTACombo2.setPromptText("TA2");
        
        // data.removeTA(taName);

    }

}