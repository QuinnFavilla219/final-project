/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.Collections;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import tam.data.ProjectData;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class AddProj_Trans implements jTPS_Transaction {

    private String team;
    private String link;
    private String hexcolor;
    private String txtcolor;
    private TAData dataManager;
    private TAWorkspace work;

    public AddProj_Trans(String name, String color, String txtColor, String link1, TAData taData, TAWorkspace workspace) {
        
        team=name;
        hexcolor=color;
        txtcolor=txtColor;
        link=link1;
        dataManager=taData;
        work=workspace;
    }

    @Override
    public void doTransaction() {  //Control Y 
        dataManager.addProjData(team, hexcolor, txtcolor, link);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransaction");
        work.getColorPicker1().setValue(Color.WHITE);
        work.getColorPicker2().setValue(Color.WHITE);
        work.getProjLinkTextField().clear();
        work.getProjNameTextField().clear();
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo Transaction");
        ObservableList<ProjectData> projData = dataManager.getProjData();
       for(ProjectData x: projData){
            if(x.getName().equals(team)&& x.getColorHex().equals(hexcolor) && x.getTxtColor().equals(txtcolor) && x.getLink().equals(link)){
                projData.remove(x);
                dataManager.getTeamNames().remove(x.getName());
                return;
            }
        
        }
        
    
        work.getColorPicker1().setValue(Color.WHITE);
        work.getColorPicker2().setValue(Color.WHITE);
        work.getProjLinkTextField().clear();
        work.getProjNameTextField().clear();
    }
        // data.removeTA(taName);

    }