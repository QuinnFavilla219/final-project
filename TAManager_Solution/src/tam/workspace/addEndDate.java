/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import jtps.jTPS_Transaction;
import tam.data.TAData;

/**
 *
 * @author User
 */
public class addEndDate implements jTPS_Transaction {
    TAWorkspace work;
    LocalDate newStart;
    TAData dataManager;
    String oldEndDate;
    
    public addEndDate(LocalDate endDate, String oldDate, TAData data, TAWorkspace workspace){
        work=workspace;
        newStart=endDate;
        dataManager=data;
        oldEndDate=oldDate;
        
    }

    @Override
    public void doTransaction() {
        String newEndDate=newStart.toString();
        dataManager.setStartDate(newEndDate);
        work.getStarter().getEditor().setText(newEndDate);
        
    }
    

    @Override
    public void undoTransaction() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(oldEndDate, format);
        String oldDate=localDate.toString();
        
        dataManager.setStartDate(oldDate);
        work.getStarter().getEditor().setText(oldDate);
        
    }
    

}

