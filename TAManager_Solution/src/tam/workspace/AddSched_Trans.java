/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.Collections;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import jtps.jTPS_Transaction;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class AddSched_Trans implements jTPS_Transaction {

    private String type;
    private String date;
    private String title;
    private String topic;
    private String link;
    private String time;
    private String criteria;
    private TAData dataManager;
    private TAWorkspace work;

    public AddSched_Trans(String holiday, String dat, String tit, String top, String lin, String tim, String crit, TAData taData, TAWorkspace workspace) {
        type=holiday;
        time=tim;
        date=dat;
        title=tit;
        topic=top;
        link=lin;
        criteria=crit;
        dataManager=taData;
        work=workspace;
    }

    @Override
    public void doTransaction() {  //Control Y 
        dataManager.addSchedData(type, date, title, topic, link, time, criteria);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransaction");
        work.getHolidayPicker().getSelectionModel().select("Type");
        work.getDateBox().setPromptText("Date");
        work.getTitleSchedTextField().clear();
        work.getTopicSchedTextField().clear();
        work.getLinkSchedTextField().clear();
        work.getTimeSchedTextField().clear();
        work.getCriteriaSchedTextField().clear();
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo Transaction");
        ObservableList<ScheduleData> schedList = dataManager.getSchedData();
        for (ScheduleData sched : schedList) {
            if (sched.getType().equals(type) && sched.getDate().equals(date) && sched.getTitle().equals(title) &&
                    sched.getTopic().equals(topic) && sched.getLink().equals(link) && sched.getTime().equals(time) &&
                    sched.getCriteria().equals(criteria)) {
                schedList.remove(sched);
                return;
        }
        
    }
        work.getHolidayPicker().getSelectionModel().select("Type");
        work.getDateBox().setPromptText("Date");
        work.getTitleSchedTextField().clear();
        work.getTopicSchedTextField().clear();
        work.getLinkSchedTextField().clear();
        work.getTimeSchedTextField().clear();
        work.getCriteriaSchedTextField().clear();
    }
        // data.removeTA(taName);

    }



