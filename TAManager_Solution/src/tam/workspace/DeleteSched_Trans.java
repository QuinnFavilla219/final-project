package tam.workspace;

import java.util.Collections;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import jtps.jTPS_Transaction;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class DeleteSched_Trans implements jTPS_Transaction {

   private String type;
    private String date;
    private String title;
    private String topic;
    private String link;
    private String time;
    private String criteria;
    private TAData dataManager;
    private TAWorkspace workspace;

    public DeleteSched_Trans(String holiday, String dat, String tit, String top, String lin, String tim, String crit, TAData taData, TAWorkspace work) {
        type=holiday;
        time=tim;
        date=dat;
        title=tit;
        topic=top;
        link=lin;
        criteria=crit;
        dataManager=taData;
        workspace=work;
    }

    @Override
    public void doTransaction() {  //Control Y 
       System.out.println("undo Transaction");
        ObservableList<ScheduleData> schedList = dataManager.getSchedData();
        for (ScheduleData sched : schedList) {
            if (sched.getType().equals(type) && sched.getDate().equals(date) && sched.getTitle().equals(title) &&
                    sched.getTopic().equals(topic) && sched.getLink().equals(link) && sched.getTime().equals(time) &&
                    sched.getCriteria().equals(criteria)) {
                schedList.remove(sched);
                return;
        }
        
    }
        workspace.getHolidayPicker().getSelectionModel().select("Type");
        workspace.getDateBox().setPromptText("Date");
        workspace.getTimeSchedTextField().clear();
        workspace.getTitleSchedTextField().clear();
        workspace.getTopicSchedTextField().clear();
        workspace.getLinkSchedTextField().clear();
        workspace.getCriteriaSchedTextField().clear();
    }
    

    @Override
    public void undoTransaction() {  //Control Z 
         dataManager.addSchedData(type, date, title, topic, link, time, criteria);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransaction");
        
        workspace.getHolidayPicker().getSelectionModel().select("Type");
        workspace.getDateBox().setPromptText("Date");
        workspace.getTimeSchedTextField().clear();
        workspace.getTitleSchedTextField().clear();
        workspace.getTopicSchedTextField().clear();
        workspace.getLinkSchedTextField().clear();
        workspace.getCriteriaSchedTextField().clear();
}
        // data.removeTA(taName);

}