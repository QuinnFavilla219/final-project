package tam.workspace;

import djf.components.AppDataComponent;
import djf.components.AppWorkspaceComponent;
import djf.controller.AppFileController;
import djf.ui.AppGUI;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoDialogSingleton;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import tam.TAManagerApp;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Separator;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.util.Callback;
import javafx.util.StringConverter;
import jtps.jTPS_Transaction;
import jtps.test.AddToNum_Transaction;
import properties_manager.PropertiesManager;
import tam.TAManagerProp;
import static tam.TAManagerProp.CHANGE_DATE_WARN_MESS;
import static tam.TAManagerProp.CHANGE_DATE_WARN_TITLE;
import static tam.TAManagerProp.INVALID_DATE;
import static tam.TAManagerProp.INVALID_DATEMESS;
import static tam.TAManagerProp.INVALID_END;
import static tam.TAManagerProp.INVALID_END_MESS;
import static tam.TAManagerProp.INVALID_START;
import static tam.TAManagerProp.INVALID_START_MESS;
import static tam.TAManagerProp.UPDATE_DATE_MESSAGE;
import static tam.TAManagerProp.UPDATE_DATE_TITLE;
import static tam.TAManagerProp.SEM1;
import tam.data.CoursePage;
import tam.data.ProjectData;
import tam.data.ProjectStudentData;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.style.TAStyle;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import test_bed.testSave;

/**
 * This class serves as the workspace component for the TA Manager application.
 * It provides all the user interface controls in the workspace area.
 *
 * @author Richard McKenna
 */
public class TAWorkspace extends AppWorkspaceComponent {

    // THIS PROVIDES US WITH ACCESS TO THE APP COMPONENTS
    TAManagerApp app;

    // THIS PROVIDES RESPONSES TO INTERACTIONS WITH THIS WORKSPACE
    TAController controller;
    AppFileController fileController;

    // NOTE THAT EVERY CONTROL IS PUT IN A BOX TO HELP WITH ALIGNMENT
    // FOR THE HEADER ON THE LEFT
    HBox tasHeaderBox;
    Label tasHeaderLabel;
    
    Image img;
    Button changeBanner;
    
    Button addUpdateStudent;
    Button clearStudentButton;
    Button templateDir;
    
    ImageView imgView;
    
    DatePicker starter;
    DatePicker ender;
    DatePicker dateBox;
    Label teamLabel;
    Label addeditProjData;
    
    
    ComboBox styleSheet;
    ImageView imgView3;
    
    int counter=0; 
    RecitationData data1;
    RecitationData data2;
    RecitationData data3;
    
    TextField instructorField;
    TextField sectionField;
    TextField daytimeField;
    TextField locationField;
    
    public ObservableList<CoursePage> al;
    
    ComboBox superTACombo1;
    ComboBox superTACombo2;
    
    Button addUpSchedData;
    Button clearSchedData;
    
    Button addUpdate;
    Button clear;
    Button removeProjData;
    
    TextField nameText;
    TextField linkProjText;
    
    Button changeRight;
    Image img3;
    
    Button changeLeft;
    ImageView imgView2;
    
    Button changeDates;
    
    Label addEditStudentsData;
    Label students;
    Text subj;
    Text number;
    Text semester;
    Text year;
    Text iName;
    Text iHome;
    Text title;
    Text tempMessage;
    Text tempAdd;
    Text sitePages;
    Text bannerImage;
    Text leftFooter;
    Text rightFooter;
    Text styleMess;
    Text styleLabel;
    
    Button addUpdateProj;
    Button clearProj;
    
    Button removeSchedItem;
    
    Text calendarLabel;
    
    CoursePage coursePage1;
    CoursePage coursePage2;
    CoursePage coursePage3;
    CoursePage coursePage4;
    CoursePage coursePage5;
    
    TableView<ProjectData> projTable;
    TableView<ProjectStudentData> projStudentTable;
    
    ComboBox cse;
    ComboBox sem;
    ComboBox yearBox;
    ComboBox num;
    ComboBox type;
    
    TextField timeText;
    TextField titleText;
    TextField topicText;
    TextField linkText;
    TextField criteriaText;
    
    
    Button removeRecitation;
    
    Text exportCourseDetails;
    Text dirAdd;
    
    StackPane sp;
    
    
    Label pageStyle;
    ArrayList<Text> buttons;
    ArrayList<Button> courseDetailsButtons;
    ArrayList<ComboBox> boxes;
    // FOR THE TA TABLE
    TableView<TeachingAssistant> taTable;
    TableColumn<TeachingAssistant, String> nameColumn;
    TableColumn<TeachingAssistant, String> emailColumn;
    TableColumn<TeachingAssistant, Boolean> undergradCol;
    
    ArrayList<DatePicker> timePickerBoxes;
    
    Button dirBT;
    
    TextField firstNameText;
    TextField lastNameText;
    ComboBox teamText;
    TextField roleText;
    Button removeStudent;

    ScrollPane scroll;
    ScrollPane recitationScroll;
    ScrollPane projectScroll;
    ScrollPane schedScroll;
    TableView<CoursePage> templateTable;
    TableView<ScheduleData> schedTable;
    TableView<RecitationData> recTable;
    ArrayList<Label> recLabels=new ArrayList<Label>();
    Text template;
    Label recitation;
    Label addEdit;
    Label schedule;
    Label schedItemLabel;
    Label addUpdateLabel;
    
    ArrayList<String> TAS= new ArrayList<String>();
    
    TextField homeTextField;
    TextField titleTextField;
    TextField nameCourseTextField;
    
    TableColumn dateColumn;
    Label projLabel;

    // THE TA INPUT
    HBox addBox;
    TextField nameTextField;
    TextField emailTextField;
    Button addButton;
    Button updateTaButton;
    Button clearButton;
    Button clearButton1;
    Button taRemove;

    // THE HEADER ON THE RIGHT
    HBox officeHoursHeaderBox;
    Label officeHoursHeaderLabel;
    //Start and End Time for Office Hours 
    
    ColorPicker picker1;
    ColorPicker picker2;
    
    ComboBox newStartTime;
    ComboBox newEndTime;
    Button changeTimeButton;

    // THE OFFICE HOURS GRID
    GridPane officeHoursGridPane;
    HashMap<String, Pane> officeHoursGridTimeHeaderPanes;
    HashMap<String, Label> officeHoursGridTimeHeaderLabels;
    HashMap<String, Pane> officeHoursGridDayHeaderPanes;
    HashMap<String, Label> officeHoursGridDayHeaderLabels;
    HashMap<String, Pane> officeHoursGridTimeCellPanes;
    HashMap<String, Label> officeHoursGridTimeCellLabels;
    HashMap<String, Pane> officeHoursGridTACellPanes;
    HashMap<String, Label> officeHoursGridTACellLabels;
    
    TabPane tabs= new TabPane();
    
    Label courseInfo;
    /**
     * The contstructor initializes the user interface, except for the full
     * office hours grid, since it doesn't yet know what the hours will be until
     * a file is loaded or a new one is created.
     */
    public TAWorkspace(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // WE'LL NEED THIS TO GET LANGUAGE PROPERTIES FOR OUR UI
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAData data = (TAData) app.getDataComponent();
        ObservableList<TeachingAssistant> tableData = data.getTeachingAssistants();

        // INIT THE HEADER ON THE LEFT
        tasHeaderBox = new HBox();
        taRemove= new Button(props.getProperty(TAManagerProp.REMOVE_TXT.toString()));
        String tasHeaderText = props.getProperty(TAManagerProp.TAS_HEADER_TEXT.toString());
        tasHeaderLabel = new Label(tasHeaderText);
        tasHeaderBox.getChildren().addAll(tasHeaderLabel, taRemove);

        // MAKE THE TABLE AND SETUP THE DATA MODEL
        taTable = new TableView();
        taTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        taTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        //TAData data = (TAData) app.getDataComponent();
        //ObservableList<TeachingAssistant> TATableData = data.getTeachingAssistants();
        taTable.setItems(tableData);
        String nameColumnText = props.getProperty(TAManagerProp.NAME_COLUMN_TEXT.toString());
        String emailColumnText = props.getProperty(TAManagerProp.EMAIL_COLUMN_TEXT.toString());
        String undergradColumnText=props.getProperty(TAManagerProp.UNDERGRAD_LABEL.toString());
        nameColumn = new TableColumn(nameColumnText);
        emailColumn = new TableColumn(emailColumnText);
        undergradCol= new TableColumn(undergradColumnText);
        /*
        undergradCol.setCellValueFactory(
            new Callback<CellDataFeatures<TeachingAssistant, Boolean>, ObservableValue<Boolean>>() {
                @Override
                public ObservableValue<Boolean> call(CellDataFeatures<TeachingAssistant, Boolean>  param) {
                    //System.out.println(param.getValue());
                    //return null;
                    return param.getValue().getCheckBox();
                    
                }
                
            } 
                //new PropertyValueFactory<TeachingAssistant, Boolean>("undergrad")
        );
        */
         undergradCol.setCellValueFactory(new Callback<CellDataFeatures<TeachingAssistant, Boolean>, ObservableValue<Boolean>>() {
 
            public ObservableValue<Boolean> call(CellDataFeatures<TeachingAssistant, Boolean> param) {
                TeachingAssistant person = param.getValue();
 
                SimpleBooleanProperty booleanProp = new SimpleBooleanProperty(person.getUndergrad().getValue());
 
                booleanProp.addListener(new ChangeListener<Boolean>() {
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
                            Boolean newValue) {
                        person.setUndergrad(newValue);
                        System.out.println(newValue);
                        controller.markWorkAsEdited();
                        //person.setOldCheckValue(oldValue);
                        
                    }
                });
                return booleanProp;
            }
        });


        //undergradCol.setCellValueFactory(e -> new SimpleBooleanProperty(e.getValue().getCheckBox()));
        //undergradCol.setCellValueFactory(new PropertyValueFactory<TeachingAssistant, Boolean>("checkbox"));
        undergradCol.setCellFactory(CheckBoxTableCell.forTableColumn(undergradCol));
        //undergradCol.setCellFactory(col -> new CheckBoxTableCell());
        
        //undergradCol.setCellFactory(col -> new CheckBoxTableCell());
        nameColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("name")
        );
        emailColumn.setCellValueFactory(
                new PropertyValueFactory<TeachingAssistant, String>("email")
        );
        
        //undergradCol.setEditable(true);
        taTable.setEditable(true);
        taTable.getColumns().add(undergradCol);
        taTable.getColumns().add(nameColumn);
        taTable.getColumns().add(emailColumn);
        

        // ADD BOX FOR ADDING A TA
        String namePromptText = props.getProperty(TAManagerProp.NAME_PROMPT_TEXT.toString());
        String emailPromptText = props.getProperty(TAManagerProp.EMAIL_PROMPT_TEXT.toString());
        String startHourPromptText = props.getProperty(TAManagerProp.START_HOUR_PROMPT_TEXT.toString());
        String endHourPromptText = props.getProperty(TAManagerProp.END_HOUR_PROMPT_TEXT.toString());
        String addButtonText = props.getProperty(TAManagerProp.ADD_BUTTON_TEXT.toString());
        String updateTaButtonText = props.getProperty(TAManagerProp.UPDATE_TA_BUTTON_TEXT.toString());
        String clearButtonText = props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString());
        String changeTimeButtonText = props.getProperty(TAManagerProp.CHANGE_TIME_BUTTON_TEXT.toString());

        changeTimeButton = new Button(changeTimeButtonText);
        newStartTime = new ComboBox();
        newEndTime = new ComboBox();
        for (int i = 0; i < 24; i++) {
            newStartTime.getItems().addAll(buildCellText(i, "00"));
        }
        for (int i = 0; i < 24; i++) {
            newEndTime.getItems().addAll(buildCellText(i, "00"));
        }

        nameTextField = new TextField();
        emailTextField = new TextField();
        nameTextField.setPromptText(namePromptText);
        emailTextField.setPromptText(emailPromptText);
        addButton = new Button(addButtonText);
        updateTaButton = new Button(updateTaButtonText);
        clearButton = new Button(clearButtonText);
        clearButton1 = new Button(clearButtonText);
        addBox = new HBox();
        newStartTime.setPromptText(startHourPromptText);
        newEndTime.setPromptText(endHourPromptText);

        nameTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        emailTextField.prefWidthProperty().bind(addBox.widthProperty().multiply(.4));
        addButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        updateTaButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        clearButton.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        updateTaButton.prefHeightProperty().bind(addBox.heightProperty().multiply(1));
        clearButton.prefHeightProperty().bind(addBox.heightProperty().multiply(1));
        clearButton1.prefWidthProperty().bind(addBox.widthProperty().multiply(.2));
        clearButton1.prefHeightProperty().bind(addBox.heightProperty().multiply(1));
        

        addBox.getChildren().add(nameTextField);
        addBox.getChildren().add(emailTextField);
        addBox.getChildren().add(addButton);
        addBox.getChildren().add(clearButton1);

        // INIT THE HEADER ON THE RIGHT
        officeHoursHeaderBox = new HBox();
        String officeHoursGridText = props.getProperty(TAManagerProp.OFFICE_HOURS_SUBHEADER.toString());
        officeHoursHeaderLabel = new Label(officeHoursGridText);
        officeHoursHeaderBox.getChildren().add(officeHoursHeaderLabel);
        officeHoursHeaderBox.getChildren().add(newStartTime);
        officeHoursHeaderBox.getChildren().add(newEndTime);
        officeHoursHeaderBox.getChildren().add(changeTimeButton);

        // THESE WILL STORE PANES AND LABELS FOR OUR OFFICE HOURS GRID
        officeHoursGridPane = new GridPane();
        officeHoursGridTimeHeaderPanes = new HashMap();
        officeHoursGridTimeHeaderLabels = new HashMap();
        officeHoursGridDayHeaderPanes = new HashMap();
        officeHoursGridDayHeaderLabels = new HashMap();
        officeHoursGridTimeCellPanes = new HashMap();
        officeHoursGridTimeCellLabels = new HashMap();
        officeHoursGridTACellPanes = new HashMap();
        officeHoursGridTACellLabels = new HashMap();
        
        String dataTxt=props.getProperty(TAManagerProp.TADATA_LABEL.toString());
        String courseTxt=props.getProperty(TAManagerProp.COURSEDETAILS_LABEL.toString());
        String recTxt=props.getProperty(TAManagerProp.RECITATIONDATA_LABEL.toString());
        String schedTxt=props.getProperty(TAManagerProp.SCHEDULEDATA_LABEL.toString());
        String projTxt=props.getProperty(TAManagerProp.PROJECTDATA_LABEL.toString());
        Tab TAData= new Tab(dataTxt);
        Tab courseDetails= new Tab(courseTxt);
        Tab recitationData= new Tab(recTxt);
        Tab scheduleData= new Tab(schedTxt);
        Tab projData= new Tab(projTxt);

        // ORGANIZE THE LEFT AND RIGHT PANES
        VBox leftPane = new VBox();
        leftPane.getChildren().add(tasHeaderBox);
        leftPane.getChildren().add(taTable);
        leftPane.getChildren().add(addBox);
        VBox rightPane = new VBox();
        rightPane.getChildren().add(officeHoursHeaderBox);
        rightPane.getChildren().add(officeHoursGridPane);

        // BOTH PANES WILL NOW GO IN A SPLIT PANE
        SplitPane sPane = new SplitPane(leftPane, new ScrollPane(rightPane));
        workspace = new BorderPane();

        // AND PUT EVERYTHING IN THE WORKSPACE
        //((BorderPane) workspace).setCenter(sPane);

        // MAKE SURE THE TABLE EXTENDS DOWN FAR ENOUGH
        taTable.prefHeightProperty().bind(workspace.heightProperty().multiply(1.9));
        
        TAData.setContent(sPane);
        tabs.getTabs().addAll(courseDetails, TAData, recitationData, scheduleData, projData);
        
        for (Tab x: tabs.getTabs()){
            x.setClosable(false);
        }
        
        ((BorderPane) workspace).setCenter(tabs);
        String [] str;
        str= new String []{
            "Chaeyoung Lee", "Jerrel Sogoni", "Yu Zhao", "Jacob Evans", "James Hoffman", "Robert Trujillo", 
            "Wonje Kang", "Wonyong Jeong", "Michael Mathew", "Ali Afzal", "Muhammad R Islam", "Milto Ndrenika"};
        
        for(String x: str){
            TAS.add(x);
        }
       
        //courseDetails Tab
        
        boxes=new ArrayList<ComboBox>();
        HBox box1= new HBox();
        HBox box2= new HBox();
        VBox finalBox= new VBox();
        VBox templateBox= new VBox();
        courseDetailsButtons= new ArrayList<Button>();
        GridPane courseDetailsPane= new GridPane(); 
        DirectoryChooser dir=new DirectoryChooser();
        String infoTxt=props.getProperty(TAManagerProp.COURSE_INFO_LABEL.toString());
        courseInfo= new Label(infoTxt);
        box1.getChildren().add(courseInfo);
        //courseDetailsPane.add(box1, 0, 0);
        String subjTxt=props.getProperty(TAManagerProp.SUBJECT_LABEL.toString());
        subj= new Text(subjTxt);
        courseDetailsPane.add(subj, 0, 1);
        String numTxt=props.getProperty(TAManagerProp.NUMBER_LABEL.toString());
        String semTxt=props.getProperty(TAManagerProp.SEMESTER_LABEL.toString());
        String yearTxt=props.getProperty(TAManagerProp.YEAR_LABEL.toString());
        number= new Text(numTxt);
        semester= new Text(semTxt);
        year= new Text(yearTxt);
        cse= new ComboBox();
        num= new ComboBox();
        sem= new ComboBox();
        yearBox= new ComboBox();
        sem.setPromptText(props.getProperty(TAManagerProp.FALL_LABEL.toString()));
        cse.setPromptText(props.getProperty(TAManagerProp.CSE_LABEL.toString()));
        cse.getItems().addAll("CSE", "ISE", "AMS", "ASC", "ACC", "BIO", "ANP", "BUS", "CHE", "CIV", "ECO", "ENG", "EST", "ESE", "GEO", "ITS", "ENG", "JPN", "JRN");
        sem.getItems().addAll("Winter", "Spring", "Summer", "Fall");
        courseDetailsPane.add(cse, 1, 1);
        courseDetailsPane.add(number, 2, 1);
        num.setPromptText(props.getProperty(TAManagerProp.CLASS_LABEL.toString()));
        yearBox.setPromptText(props.getProperty(TAManagerProp.YEAR_PROMPT.toString()));
        num.getItems().addAll("101", "110", "114", "130", "150", "160", "161", "214", "215", "219", "220", "261", "300", "301", "310", "311", "312", "328", "377", "376");
        yearBox.getItems().addAll("2012", "2013", "2014", "2015", "2016", "2017", "2018");
        courseDetailsPane.add(num, 3, 1);
        courseDetailsPane.add(semester, 0, 2);
        courseDetailsPane.add(sem, 1, 2);
        courseDetailsPane.add(year, 2, 2);
        courseDetailsPane.add(yearBox, 3, 2);
        boxes.add(cse);
        boxes.add(num);
        boxes.add(sem);
        boxes.add(yearBox);
        String titleTxt=props.getProperty(TAManagerProp.TITLE_LABEL.toString());
        title= new Text(titleTxt);
        String iNameTxt=props.getProperty(TAManagerProp.INSTRUCTOR_NAME_LABEL.toString());
        iName= new Text(iNameTxt);
        String iHomeTxt=props.getProperty(TAManagerProp.INSTRUCTOR_HOME_LABEL.toString());
        iHome= new Text(iHomeTxt);
        courseDetailsPane.add(title, 0, 3);
        titleTextField= new TextField();
        courseDetailsPane.add(titleTextField, 1, 3);
        courseDetailsPane.add(iName, 0, 4);
        nameCourseTextField= new TextField();
        courseDetailsPane.add(nameCourseTextField, 1, 4);
        homeTextField=new TextField();
        courseDetailsPane.add(iHome, 0, 5);
        courseDetailsPane.add(homeTextField, 1, 5);
        String exportTxt=props.getProperty(TAManagerProp.EXPORT_DIR_LABEL.toString());
        String btTxt=props.getProperty(TAManagerProp.CHANGE_BUTTON_TXT.toString());
        exportCourseDetails= new Text(exportTxt);
        dirBT = new Button(btTxt);
        dirAdd= new Text(props.getProperty(TAManagerProp.DIRECTORY_ADD.toString()));
        courseDetailsPane.add(exportCourseDetails, 0, 6);
        courseDetailsPane.add(dirAdd, 1, 6);
        courseDetailsPane.add(dirBT, 2, 6);
        courseDetailsButtons.add(dirBT);
       
        
        
        String siteTxt=props.getProperty(TAManagerProp.SITETEMPLATE_LABEL.toString());
        template= new Text(siteTxt);
        templateBox.getChildren().add(template);
        String message=props.getProperty(TAManagerProp.TEMPLATE_MESSAGE.toString());
        tempMessage= new Text(message);
        templateBox.getChildren().add(tempMessage);
        String templateAdd= props.getProperty(TAManagerProp.TEMPLATE_ADD.toString());
        tempAdd= new Text(templateAdd);
        templateBox.getChildren().add(tempAdd);
        String tempBT=props.getProperty(TAManagerProp.TEMPLATE_BUTTON.toString());
        templateDir= new Button(tempBT);
        courseDetailsButtons.add(templateDir);
        templateBox.getChildren().add(templateDir);
        String sitePagesTxt=props.getProperty(TAManagerProp.SITE_PAGES_TXT.toString());
        sitePages= new Text(sitePagesTxt);
        templateBox.getChildren().add(sitePages);
        
        
        //create table
        templateTable = new TableView();
        templateTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        al = FXCollections.observableArrayList();
        coursePage1= new CoursePage(false, props.getProperty(TAManagerProp.HOME.toString()), props.getProperty(TAManagerProp.HOMEHTML.toString()), props.getProperty(TAManagerProp.HOMEBUILDER.toString()));
        coursePage2=new CoursePage(false, props.getProperty(TAManagerProp.SYLLABUS.toString()), props.getProperty(TAManagerProp.SYLLABUSHTML.toString()), props.getProperty(TAManagerProp.SYLLABUSBUILDER.toString()));
        coursePage3=new CoursePage(false, props.getProperty(TAManagerProp.SCHEDULE.toString()), props.getProperty(TAManagerProp.SCHEDULEHTML.toString()), props.getProperty(TAManagerProp.SCHEDULEBUILDER.toString()));
        coursePage4=new CoursePage(false, props.getProperty(TAManagerProp.HWS.toString()), props.getProperty(TAManagerProp.HWHTML.toString()), props.getProperty(TAManagerProp.HWSBUILDER.toString()));
        coursePage5=new CoursePage(false, props.getProperty(TAManagerProp.PROJECTS.toString()), props.getProperty(TAManagerProp.PROJECTHTML.toString()), props.getProperty(TAManagerProp.PROJECTBUILDER.toString()));
        al.add(coursePage1);
        al.add(coursePage2);
        al.add(coursePage3);
        al.add(coursePage4);
        al.add(coursePage5);
        templateTable.setItems(al);
        String useColumnText = props.getProperty(TAManagerProp.USE_COL_TXT.toString());
        String navbarColumnText = props.getProperty(TAManagerProp.NAVBAR_TXT.toString());
        String fileColumnText = props.getProperty(TAManagerProp.FILE_TXT.toString());
        String scriptColumnText = props.getProperty(TAManagerProp.SCRIPT_TXT.toString());
        TableColumn useColumn = new TableColumn(useColumnText);
        TableColumn navbarColumn = new TableColumn(navbarColumnText);
        TableColumn fileColumn = new TableColumn(fileColumnText);
        TableColumn scriptColumn = new TableColumn(scriptColumnText);
        templateTable.getColumns().add(useColumn);
        templateTable.getColumns().add(navbarColumn);
        templateTable.getColumns().add(fileColumn);
        templateTable.getColumns().add(scriptColumn);
        navbarColumn.setCellValueFactory(new PropertyValueFactory<CoursePage, String>("navBarTitle"));
        fileColumn.setCellValueFactory(new PropertyValueFactory<CoursePage, String>("fileName"));
        scriptColumn.setCellValueFactory(new PropertyValueFactory<CoursePage, String>("script"));
        //useColumn.setCellValueFactory(new PropertyValueFactory<CoursePage, Boolean>("jj"));
        //useColumn.setCellFactory(CheckBoxTableCell.forTableColumn(useColumn));
        useColumn.setCellValueFactory(new Callback<CellDataFeatures<CoursePage, Boolean>, ObservableValue<Boolean>>() {
 
            public ObservableValue<Boolean> call(CellDataFeatures<CoursePage, Boolean> param) {
                CoursePage person = param.getValue();
 
                SimpleBooleanProperty booleanProp = new SimpleBooleanProperty(person.getCheckBox().getValue());
 
                booleanProp.addListener(new ChangeListener<Boolean>() {
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
                            Boolean newValue) {
                        person.setCheckBox(newValue);
                        System.out.println(newValue);
                        controller.markWorkAsEdited();
                        //person.setOldCheckValue(oldValue);
                        
                    }
                });
                return booleanProp;
            }
        });
        useColumn.setCellFactory(CheckBoxTableCell.forTableColumn(useColumn));
        useColumn.setEditable(true);
        templateTable.setEditable(true);
        templateTable.setMaxWidth(467.0);
        templateTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        templateBox.getChildren().add(templateTable);
        
        GridPane secondTemplatePane= new GridPane();
        String pageLabelText = props.getProperty(TAManagerProp.PAGESTYLE_LABEL.toString());
        pageStyle= new Label(pageLabelText);
        box2.getChildren().add(pageStyle);
        String bannerImText = props.getProperty(TAManagerProp.BANNERSCHOOL_LABEL.toString());
        bannerImage= new Text(bannerImText);
        secondTemplatePane.add(bannerImage, 0, 1);
        sp = new StackPane();
        img = new Image("file:C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\images\\leftimage.png");
        data.setImageView1(img);
        imgView = new ImageView(img);
        sp.getChildren().add(imgView);
        secondTemplatePane.add(sp, 1, 1);
        changeBanner= new Button(props.getProperty(TAManagerProp.CHANGE_BUTTON_TXT.toString()));
        secondTemplatePane.add(changeBanner, 2, 1);
        leftFooter= new Text(props.getProperty(TAManagerProp.LEFTFOOTER_LABEL.toString()));
        secondTemplatePane.add(leftFooter, 0, 2);
        StackPane sp2 = new StackPane();
        Image img2 = new Image("file:C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\images\\rightimage.png");
        data.setImageView2(img2);
        imgView2 = new ImageView(img2);
        sp2.getChildren().add(imgView2);
        secondTemplatePane.add(sp2, 1, 2);
        changeLeft= new Button(props.getProperty(TAManagerProp.CHANGE_BUTTON_TXT.toString()));
        secondTemplatePane.add(changeLeft, 2, 2);
        rightFooter= new Text(props.getProperty(TAManagerProp.RIGHTFOOTER_LABEL.toString()));
        secondTemplatePane.add(rightFooter, 0, 3);
        StackPane sp3 = new StackPane();
        img3 = new Image("file:C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\images\\bannerimage.png");
        data.setImageView3(img3);
        imgView3 = new ImageView(img3);
        sp3.getChildren().add(imgView3);
        secondTemplatePane.add(sp3, 1, 3);
        changeRight= new Button(props.getProperty(TAManagerProp.CHANGE_BUTTON_TXT.toString()));
        secondTemplatePane.add(changeRight,2, 3);
        styleSheet= new ComboBox();
        styleSheet.setPromptText("Style");
        styleSheet.getItems().addAll("course_homepage_layout.css", "sea_wolf.css");
        boxes.add(styleSheet);
        styleLabel= new Text(props.getProperty(TAManagerProp.STYLESHEET_LABEL.toString()));
        HBox tempBox= new HBox();
        tempBox.getChildren().addAll(styleLabel, styleSheet);
        secondTemplatePane.add(tempBox, 0, 4);
        styleMess= new Text(props.getProperty(TAManagerProp.STYLESHEET_MESSAGE.toString()));
        secondTemplatePane.add(styleMess, 0, 5);
        courseDetailsButtons.add(changeBanner);
        courseDetailsButtons.add(changeLeft);
        courseDetailsButtons.add(changeRight);
        finalBox.getChildren().addAll(box1, courseDetailsPane, templateBox, box2, secondTemplatePane);
        courseDetailsPane.setVgap(5.0);
        courseDetailsPane.setHgap(5.0);
        templateBox.setSpacing(5.0);
        secondTemplatePane.setVgap(5.0);
        secondTemplatePane.setHgap(5.0);
        scroll= new ScrollPane();
        scroll.setContent(finalBox);
        //courseDetails.setContent(finalBox);
        courseDetails.setContent(scroll);
        
        //recitationpane
        VBox recBox= new VBox();
        GridPane recitationTab= new GridPane();
        String recLabel=props.getProperty(TAManagerProp.RECITATIONTITLE_LABEL.toString());
        recitation= new Label(recLabel);
        HBox recTitle= new HBox();
        recTitle.getChildren().add(recitation);
        removeRecitation= new Button(props.getProperty(TAManagerProp.REMOVE_TXT.toString()));
        //recitationTab.add(recitation, 0, 0);
        //recitationTab.add(removeRecitation, 1, 0);
        
        //recTable
        recTable = new TableView();
        recTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList<RecitationData> recData = data.getRecDataTable();
        String sectionColumnText = props.getProperty(TAManagerProp.SECTION_LABEL.toString());
        String instructorColumnText = props.getProperty(TAManagerProp.INSTRUCTOR_LABEL.toString());
        String DayTimeColumnText = props.getProperty(TAManagerProp.DAYTIME_LABEL.toString());
        String locationColumnText = props.getProperty(TAManagerProp.LOCATION_LABEL.toString());
        String TAColumnText = props.getProperty(TAManagerProp.TA_LABEL.toString());
        TableColumn<RecitationData, String> sectionColumn = new TableColumn(sectionColumnText);
        TableColumn instructorColumn = new TableColumn(instructorColumnText);
        TableColumn daytimeColumn=new TableColumn(DayTimeColumnText);
        TableColumn locationColumn = new TableColumn(locationColumnText);
        TableColumn TAColumn1 = new TableColumn(TAColumnText);
        TableColumn TAColumn2 = new TableColumn(TAColumnText);
        recTable.getColumns().addAll(sectionColumn, instructorColumn, daytimeColumn, locationColumn, TAColumn1, TAColumn2);
        sectionColumn.setCellValueFactory(new PropertyValueFactory<RecitationData, String>("section"));
        instructorColumn.setCellValueFactory(new PropertyValueFactory<RecitationData, String>("instructor"));
        daytimeColumn.setCellValueFactory(new PropertyValueFactory<RecitationData, String>("dayAndTime"));
        locationColumn.setCellValueFactory(new PropertyValueFactory<RecitationData, String>("location"));
        TAColumn1.setCellValueFactory(new PropertyValueFactory<RecitationData, String>("TA1"));
        TAColumn2.setCellValueFactory(new PropertyValueFactory<RecitationData, String>("TA2"));
        recTable.setItems(recData);
        recTable.setEditable(true);
        HBox recTableBox= new HBox();
        recTable.setMaxWidth(475);
        recTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        recTableBox.getChildren().addAll(recTable);
        //recitationTab.add(recTable, 0, 2);
     
        
        String addEditLabel=props.getProperty(TAManagerProp.ADDEDIT_LABEL.toString());
        addEdit= new Label(addEditLabel);
        Label section= new Label(props.getProperty(TAManagerProp.SECTIONPROMPT_LABEL.toString()));
        recitationTab.add(section, 0, 0);
        sectionField= new TextField();
        recitationTab.add(sectionField, 1, 0);
        Label instructor= new Label(props.getProperty(TAManagerProp.INSTRPROMPT_LABEL.toString()));
        recitationTab.add(instructor, 0, 2);
        instructorField= new TextField();
        recitationTab.add(instructorField, 1, 2);
        Label daytime= new Label(props.getProperty(TAManagerProp.DAYTIMEPROMPT_LABEL.toString()));
        recitationTab.add(daytime, 0, 3);
        daytimeField= new TextField();
        recitationTab.add(daytimeField, 1, 3);
        Label location= new Label(props.getProperty(TAManagerProp.LOCPROMPT_LABEL.toString()));
        recitationTab.add(location, 0, 4);
        locationField= new TextField();
        recitationTab.add(locationField, 1, 4);
        Label superTA1= new Label(props.getProperty(TAManagerProp.SUPERTAPROMPT_LABEL.toString()));
        recitationTab.add(superTA1, 0, 5);
        superTACombo1= new ComboBox();
        superTACombo1.setPromptText("TA1");
        recitationTab.add(superTACombo1, 1, 5);
        Label superTA2= new Label(props.getProperty(TAManagerProp.SUPERTAPROMPT_LABEL.toString()));
        recitationTab.add(superTA2, 0, 6);
        superTACombo2= new ComboBox();
        superTACombo2.setPromptText("TA2");
        /*
        for(String x: TAS){
           superTACombo1.getItems().addAll(x);
        }
         for(String x: TAS){
           superTACombo2.getItems().addAll(x);
        }
        */
        superTACombo1.setItems(tableData);
        superTACombo2.setItems(tableData);
        recitationTab.add(superTACombo2, 1, 6);
        String addupdate=props.getProperty(TAManagerProp.ADDUPDATE_LABEL.toString());
        addUpdate= new Button(addupdate);
        recitationTab.add(addUpdate, 0, 7);
        clear= new Button(props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString()));
        recitationTab.add(clear, 1, 7);
        recLabels.add(section);
        recLabels.add(instructor);
        recLabels.add(location);
        recLabels.add(daytime);
        recLabels.add(superTA1);
        recLabels.add(superTA2);
        boxes.add(superTACombo1);
        boxes.add(superTACombo2);
        courseDetailsButtons.add(clear);
        courseDetailsButtons.add(addUpdate);
        courseDetailsButtons.add(removeRecitation);
        recBox.getChildren().addAll(recTitle, removeRecitation, recTable, recitationTab);
        recitationTab.setVgap(5.0);
        recitationTab.setHgap(5.0);
        recBox.setSpacing(5.0);
        recitationScroll= new ScrollPane();
        recitationScroll.setContent(recBox);
        recitationData.setContent(recitationScroll);
        
        //schedule tab
        GridPane scheduleDataTab= new GridPane();
        VBox schedTab= new VBox();
        String schedLabelTxt=props.getProperty(TAManagerProp.SCHEDULE_LABEL.toString());
        schedule= new Label(schedLabelTxt);
        HBox scheduleBox= new HBox();
        VBox partial= new VBox();
        HBox partial2= new HBox();
        scheduleBox.getChildren().add(schedule);
        //scheduleDataTab.add(schedule, 0, 0);
        String callabelTxt=props.getProperty(TAManagerProp.CALENDARBOUND_LABEL.toString());
        calendarLabel= new Text(callabelTxt);
        calendarLabel.setFont(Font.font("Courier New", FontWeight.BOLD, 12));
        //scheduleDataTab.add(calendarLabel, 0, 1);
        String startLabelTxt=props.getProperty(TAManagerProp.STARTDATEREC_LABEL.toString());
        Label startLabel= new Label(startLabelTxt);
        //scheduleDataTab.add(startLabel, 0, 2);
        starter= new DatePicker();
        starter.setPromptText("4/22/2012");
        //scheduleDataTab.add(starter, 1, 2);
        String endLabelTxt=props.getProperty(TAManagerProp.ENDDATEREC_LABEL.toString());
        Label endLabel= new Label(endLabelTxt);
        //scheduleDataTab.add(endLabel, 2, 2);
        ender= new DatePicker();
        ender.setPromptText("4/22/2012");
        //scheduleDataTab.add(ender, 3, 2);
        changeDates= new Button(props.getProperty(TAManagerProp.CHANGETIMES_LABEL.toString()));
        HBox schedItems= new HBox();
        partial2.getChildren().addAll(startLabel, starter, endLabel, ender, changeDates);
        partial2.setSpacing(5.0);
        partial.getChildren().addAll(calendarLabel, partial2);
        String schedItemLabelTxt=props.getProperty(TAManagerProp.SCHEDITEMS_LABEL.toString());
        schedItemLabel= new Label(schedItemLabelTxt);
        //scheduleDataTab.add(schedItemLabel, 0, 0);
        removeSchedItem= new Button(props.getProperty(TAManagerProp.REMOVE_TXT.toString()));
        //scheduleDataTab.add(removeSchedItem, 1, 0);
        schedItems.getChildren().addAll(schedItemLabel, removeSchedItem);
        recLabels.add(startLabel);
        recLabels.add(endLabel);
        courseDetailsButtons.add(removeSchedItem);
        courseDetailsButtons.add(changeDates);
        
        
        
        
        
        //create schedule table
        schedTable = new TableView();
        schedTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList<ScheduleData> schedData = data.getSchedData();
        schedTable.setItems(schedData);
        String typeColumnText = props.getProperty(TAManagerProp.TYPE_LABEL.toString());
        String dateColumnText = props.getProperty(TAManagerProp.DATEREC_LABEL.toString());
        String titleColumnText = props.getProperty(TAManagerProp.TITLEREC_LABEL.toString());
        String topicColumnText = props.getProperty(TAManagerProp.TOPICREC_LABEL.toString());
        TableColumn typeColumn = new TableColumn(typeColumnText);
        dateColumn = new TableColumn(dateColumnText);
        TableColumn titleColumn=new TableColumn(titleColumnText);
        TableColumn topicColumn = new TableColumn(topicColumnText);
        schedTable.getColumns().addAll(typeColumn, dateColumn, titleColumn, topicColumn);
        typeColumn.setCellValueFactory(new PropertyValueFactory<RecitationData, String>("type"));
        dateColumn.setCellValueFactory(new PropertyValueFactory<RecitationData, String>("date"));
        titleColumn.setCellValueFactory(new PropertyValueFactory<RecitationData, String>("title"));
        topicColumn.setCellValueFactory(new PropertyValueFactory<RecitationData, String>("topic"));
        schedTable.setMaxWidth(2000);
        schedTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        scheduleDataTab.add(schedTable, 0, 0);
        
        String addUpTxt=props.getProperty(TAManagerProp.ADDUPDATE_LABEL.toString());
        addUpdateLabel= new Label(addUpTxt);
        scheduleDataTab.add(addUpdateLabel, 0, 1);
        String typeLabelTxt=props.getProperty(TAManagerProp.TYPELABEL.toString());
        Label typeLabel= new Label(typeLabelTxt);
        scheduleDataTab.add(typeLabel, 0, 2);
        type= new ComboBox();
        type.setPromptText("Type");
        type.getItems().addAll("Holiday", "HW", "Lecture", "Recitation", "Reference");
        scheduleDataTab.add(type, 1, 2);
        String dateLabelTxt=props.getProperty(TAManagerProp.DATELABEL.toString());
        Label dateLabel= new Label(dateLabelTxt);
        scheduleDataTab.add(dateLabel, 0, 3);
        dateBox= new DatePicker();
        dateBox.setPromptText("Date");
        scheduleDataTab.add(dateBox, 1, 3);
        String timeTxt=props.getProperty(TAManagerProp.TIMELABEL.toString());
        Label timeLabel= new Label(timeTxt);
        scheduleDataTab.add(timeLabel, 0, 4);
        timeText= new TextField();
        scheduleDataTab.add(timeText, 1, 4);
        Label titleLabel= new Label(props.getProperty(TAManagerProp.TITLELABEL.toString()));
        scheduleDataTab.add(titleLabel, 0, 5);
        titleText= new TextField();
        scheduleDataTab.add(titleText, 1, 5);
        Label topicLabel= new Label(props.getProperty(TAManagerProp.TOPICLABEL.toString()));
        scheduleDataTab.add(topicLabel, 0, 6);
        topicText= new TextField();
        scheduleDataTab.add(topicText, 1, 6);
        String linkTxt=props.getProperty(TAManagerProp.LINKLABEL.toString());
        Label linkLabel= new Label(linkTxt);
        scheduleDataTab.add(linkLabel, 0, 7);
        linkText= new TextField();
        scheduleDataTab.add(linkText, 1, 7);
        String criteriaTxt=props.getProperty(TAManagerProp.CRITERIALABEL.toString());
        Label criteriaLabel= new Label(criteriaTxt);
        scheduleDataTab.add(criteriaLabel, 0, 8);
        criteriaText= new TextField();
        scheduleDataTab.add(criteriaText, 1, 8);
        addUpSchedData= new Button(props.getProperty(TAManagerProp.ADDUPDATE_LABEL.toString()));
        clearSchedData= new Button(props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString()));
        scheduleDataTab.add(addUpSchedData, 0, 9);
        scheduleDataTab.add(clearSchedData, 1, 9);
        schedTab.getChildren().addAll(scheduleBox, partial, schedItems, scheduleDataTab);
        schedTab.setSpacing(5.0);
        schedItems.setSpacing(5.0);
        scheduleDataTab.setVgap(5.0);
        scheduleDataTab.setHgap(5.0);
        schedScroll=new ScrollPane();
        schedScroll.setContent(schedTab);
        scheduleData.setContent(schedScroll);
        courseDetailsButtons.add(addUpSchedData);
        courseDetailsButtons.add(clearSchedData);
        recLabels.add(typeLabel);
        recLabels.add(dateLabel);
        recLabels.add(timeLabel);
        recLabels.add(titleLabel);
        recLabels.add(topicLabel);
        recLabels.add(linkLabel);
        recLabels.add(criteriaLabel);
        boxes.add(type);
        
        //project tab
        GridPane projectDataTab= new GridPane();
        HBox projUpper= new HBox();
        projectDataTab.setAlignment(Pos.CENTER);
        String projTxtLabel=props.getProperty(TAManagerProp.PROJECTS_LABEL.toString());
        projLabel= new Label(projTxtLabel);
        projectDataTab.add(projLabel, 0, 0);
        String teamsTxt=props.getProperty(TAManagerProp.TEAMS_LABEL.toString());
        teamLabel= new Label(teamsTxt);
        //projectDataTab.add(teamLabel, 0, 1);
        removeProjData= new Button(props.getProperty(TAManagerProp.REMOVE_TXT.toString()));
        projUpper.getChildren().addAll(teamLabel, removeProjData);
        projUpper.setSpacing(5.0);
        projectDataTab.add(projUpper, 0, 1);
        //projectDataTab.add(removeProjData, 1, 1);
        courseDetailsButtons.add(removeProjData);
        
        
        //create project table
        projTable = new TableView();
        projTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList<ProjectData> projectData = data.getProjData();
        projTable.setItems(projectData);
        String nameProjColumnText = props.getProperty(TAManagerProp.NAMELABEL.toString());
        String colorColumnText = props.getProperty(TAManagerProp.COLORHEX_LABEL.toString());
        String textColorColumnText = props.getProperty(TAManagerProp.TXTCOLOR_LABEL.toString());
        String projLinkColumnText = props.getProperty(TAManagerProp.LINKLABEL.toString());
        TableColumn nameProjColumn = new TableColumn(nameProjColumnText);
        TableColumn colorColumn = new TableColumn(colorColumnText);
        TableColumn textColorColumn=new TableColumn(textColorColumnText);
        TableColumn projLinkColumn = new TableColumn(projLinkColumnText);
        projTable.getColumns().addAll(nameProjColumn, colorColumn, textColorColumn, projLinkColumn);
        nameProjColumn.setCellValueFactory(new PropertyValueFactory<ProjectData, String>("name"));
        colorColumn.setCellValueFactory(new PropertyValueFactory<ProjectData, String>("colorHex"));
        textColorColumn.setCellValueFactory(new PropertyValueFactory<ProjectData, String>("txtColor"));
        projLinkColumn.setCellValueFactory(new PropertyValueFactory<ProjectData, String>("link"));
        projTable.setMaxWidth(700);
        projTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        projectDataTab.add(projTable, 0, 2);
        
        picker1= new ColorPicker();
        picker2= new ColorPicker();
        HBox colorBox= new HBox();
        HBox nameBox= new HBox();
        HBox linkBox= new HBox();
        String addEditProjTxt=props.getProperty(TAManagerProp.ADDEDIT_LABEL.toString());
        addeditProjData= new Label(addEditProjTxt);
        projectDataTab.add(addeditProjData, 0, 3);
        Label name= new Label(nameProjColumnText);
        //projectDataTab.add(name, 0, 4);
        nameText= new TextField();
        //projectDataTab.add(nameText, 1, 4);
        nameBox.getChildren().addAll(name, nameText);
        nameBox.setSpacing(10.0);
        projectDataTab.add(nameBox, 0, 4);
        String colorTxt=props.getProperty(TAManagerProp.COLOR_LABEL.toString());
        Label color= new Label(colorTxt);
        //projectDataTab.add(color, 0, 5);
        String textColorTxt=props.getProperty(TAManagerProp.TEXTCOLOR_LABEL.toString());
        Label textColor= new Label(textColorTxt);
        //projectDataTab.add(textColor, 2, 5);
        colorBox.getChildren().addAll(color, picker1, textColor, picker2);
        colorBox.setSpacing(10.0);
        projectDataTab.add(colorBox, 0, 5);
        String linkProjTxt=props.getProperty(TAManagerProp.LINKLABEL.toString());
        Label link= new Label(linkProjTxt);
        //projectDataTab.add(link, 0, 6);
        linkProjText= new TextField();
        //projectDataTab.add(linkProjText, 1, 6);
        linkBox.getChildren().addAll(link, linkProjText);
        linkBox.setSpacing(10.0);
        projectDataTab.add(linkBox, 0 ,6);
        HBox buttonBox= new HBox();
        addUpdateProj= new Button(addEditProjTxt);
        //projectDataTab.add(addUpdateProj, 0, 7);
        clearProj= new Button(props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString()));
        //projectDataTab.add(clearProj, 1, 7);
        buttonBox.getChildren().addAll(addUpdateProj, clearProj);
        buttonBox.setSpacing(10.0);
        projectDataTab.add(buttonBox, 0 ,7);
        HBox studentsLabelBox= new HBox();
        String studentsTxt=props.getProperty(TAManagerProp.STUDENTS_LABEL.toString());
        students= new Label(studentsTxt);
        //projectDataTab.add(students, 0, 10);
        removeStudent= new Button(props.getProperty(TAManagerProp.REMOVE_TXT.toString()));
        //projectDataTab.add(removeStudent, 1, 10);
        studentsLabelBox.getChildren().addAll(students, removeStudent);
        studentsLabelBox.setSpacing(10.0);
        projectDataTab.add(studentsLabelBox, 0, 10);
        recLabels.add(name);
        recLabels.add(color);
        recLabels.add(textColor);
        recLabels.add(link);
        courseDetailsButtons.add(addUpdateProj);
        courseDetailsButtons.add(clearProj);
        courseDetailsButtons.add(removeStudent);
        
        
        
        //create students table for projects tab
        projStudentTable = new TableView();
        projStudentTable.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        ObservableList<ProjectStudentData> projectStudentData = data.getProjStudentData();
        projStudentTable.setItems(projectStudentData);
        String firstnameColumnText = props.getProperty(TAManagerProp.FIRSTNAMELABEL.toString());
        String lastnameColumnText = props.getProperty(TAManagerProp.LASTNAMELABEL.toString());
        String teamColumnText = props.getProperty(TAManagerProp.TEAMLABEL.toString());
        String roleColumnText = props.getProperty(TAManagerProp.ROLELABEL.toString());
        TableColumn firstnameProjColumn = new TableColumn(firstnameColumnText);
        TableColumn lastnameColumn = new TableColumn(lastnameColumnText);
        TableColumn teamColumn=new TableColumn(teamColumnText);
        TableColumn roleColumn = new TableColumn(roleColumnText);
        projStudentTable.getColumns().addAll(firstnameProjColumn, lastnameColumn, teamColumn, roleColumn);
        firstnameProjColumn.setCellValueFactory(new PropertyValueFactory<ProjectStudentData, String>("firstName"));
        lastnameColumn.setCellValueFactory(new PropertyValueFactory<ProjectStudentData, String>("lastName"));
        teamColumn.setCellValueFactory(new PropertyValueFactory<ProjectStudentData, String>("team"));
        roleColumn.setCellValueFactory(new PropertyValueFactory<ProjectStudentData, String>("role"));
        projStudentTable.setMaxWidth(700);
        projStudentTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        projectDataTab.add(projStudentTable, 0, 11);
        
        
        GridPane semi= new GridPane();
        String addEditStudentsTxt=props.getProperty(TAManagerProp.ADDEDIT_LABEL.toString());
        addEditStudentsData= new Label(addEditStudentsTxt);
        projectDataTab.add(addEditStudentsData, 0, 12);
        Label firstname= new Label(firstnameColumnText);
        //projectDataTab.add(firstname, 0, 13);
        semi.add(firstname, 0, 0);
        firstNameText= new TextField();
        semi.add(firstNameText, 1, 0);
        Label lastname= new Label(lastnameColumnText);
        semi.add(lastname, 0, 2);
        lastNameText= new TextField();
        semi.add(lastNameText, 1, 2);
        Label team= new Label(teamColumnText);
        semi.add(team, 0, 3);
        teamText= new ComboBox();
        teamText.setItems(data.getTeamNames());
        teamText.setPromptText("Team");
        semi.add(teamText, 1, 3);
        Label role= new Label(roleColumnText);
        semi.add(role, 0, 4);
        roleText= new TextField();
        semi.add(roleText, 1, 4);
        addUpdateStudent= new Button(props.getProperty(TAManagerProp.ADDUPDATE_LABEL.toString()));
        clearStudentButton= new Button(props.getProperty(TAManagerProp.CLEAR_BUTTON_TEXT.toString()));
        semi.add(addUpdateStudent, 0, 5);
        semi.add(clearStudentButton, 1, 5);
        semi.setVgap(5.0);
        semi.setHgap(5.0);
        projectDataTab.add(semi, 0, 13);
        projectDataTab.setVgap(10.0);
        projectDataTab.setHgap(7.0);
        courseDetailsButtons.add(addUpdateStudent);
        courseDetailsButtons.add(clearStudentButton);
        recLabels.add(firstname);
        recLabels.add(lastname);
        recLabels.add(team);
        recLabels.add(role);
        projectScroll= new ScrollPane();
        projectScroll.setContent(projectDataTab);
        projData.setContent(projectScroll);
        
       
       

        // NOW LET'S SETUP THE EVENT HANDLING
        controller = new TAController(app);
        fileController=new AppFileController(app);
        AppGUI gui = app.getGUI();
        
        

        // CONTROLS FOR ADDING TAs
        nameTextField.setOnAction(e -> {
            controller.handleAddTA();
        });
        
       gui.undo.setOnAction(e -> {
            controller.handleUndoTransaction();
        });
       
       gui.redo.setOnAction(e -> {
            controller.handleReDoTransaction();
        });
       
       gui.exportButton.setOnAction(e -> {
            controller.handleExportRequest();
        });
        
        dirBT.setOnAction(e -> {
            File selectedFile=fileController.handleExportHandlerRequest();
            String fileName=selectedFile.toString();
            dirAdd.setText(fileName);
            data.setExportDir(fileName);
            data.setExportDir2(selectedFile);
            gui.exportButton.setDisable(false);
        });
        
        templateDir.setOnAction(e -> {
            File selectedFile=fileController.handleTemplateDirHandlerRequest();
            String fileName=selectedFile.toString();
            tempAdd.setText(fileName);
            data.setTemplateDir(fileName);
            System.out.println(fileName);

            File[] listOfFiles = selectedFile.listFiles();
            coursePage1.setCheckBox(false);
            data.setHome(false);
            coursePage2.setCheckBox(false);
            data.setHWS(false);
            coursePage3.setCheckBox(false);
            data.setSyllabus(false);
            coursePage4.setCheckBox(false);
            data.setProjects(false);
            coursePage5.setCheckBox(false);
            data.setSchedule(false);
            for (File file : listOfFiles) {
            if (file.isFile()) {
                String fileString=file.toString();
                if(fileString.contains("hws.html")){
                    coursePage4.setCheckBox(true);
                    data.setHWS(true);
                }
                if(fileString.contains("index.html")){
                    coursePage1.setCheckBox(true);
                    data.setHome(true);
                }
                if(fileString.contains("syllabus.html")){
                    coursePage2.setCheckBox(true);
                    data.setSyllabus(true);
                }
                if(fileString.contains("schedule.html")){
                    coursePage3.setCheckBox(true);
                    data.setSchedule(true);
                }
                if(fileString.contains("projects.html")){
                    coursePage5.setCheckBox(true);
                    data.setProjects(true);
                }
               
      
                        }
            
                }
             templateTable.refresh();
             controller.markWorkAsEdited();
             System.out.println(coursePage1.getCheckBox());
             System.out.println(coursePage2.getCheckBox());
             System.out.println(coursePage3.getCheckBox());
             System.out.println(coursePage4.getCheckBox());
             System.out.println(coursePage5.getCheckBox());
            gui.exportButton.setDisable(false);
        });
        
        changeBanner.setOnAction(e -> {
            File selectedFile=fileController.handleImageHandlerRequest();
            String fileName=selectedFile.toString();
            int index=fileName.lastIndexOf("\\");
            String tempFile=fileName.substring(index+1);
            System.out.println(tempFile);
            Image newImg = new Image("file:" + selectedFile);
            data.setImageView3(newImg);
            imgView.setImage(newImg);
            data.setBannerImage(selectedFile.toString());
            data.setBannerExport("./images/"+tempFile);
            controller.markWorkAsEdited();
            //sp.getChildren().add(imgView);
        });
        
        changeLeft.setOnAction(e -> {
            File selectedFile=fileController.handleImageHandlerRequest();
            String fileName=selectedFile.toString();
            int index=fileName.lastIndexOf("\\");
            String tempFile=fileName.substring(index+1);
            Image newImg = new Image("file:" + selectedFile);
            imgView2.setImage(newImg);
            data.setLeftExport("./images/"+tempFile);
            data.setImageView1(newImg);
            data.setLeftFooterImage(selectedFile.toString());
            controller.markWorkAsEdited();
            //sp.getChildren().add(imgView);
        });
        
        changeRight.setOnAction(e -> {
            File selectedFile=fileController.handleImageHandlerRequest();
            String fileName=selectedFile.toString();
            int index=fileName.lastIndexOf("\\");
            String tempFile=fileName.substring(index+1);
            Image newImg = new Image("file:" + selectedFile);
            data.setImageView2(newImg);
            imgView3.setImage(newImg);
            data.setRightExport("./images/"+tempFile);
            data.setRightFooterImage(selectedFile.toString());
            controller.markWorkAsEdited();
            //sp.getChildren().add(imgView);
        });
        
        emailTextField.setOnAction(e -> {
            controller.handleAddTA();
        });
        addButton.setOnAction(e -> {
            controller.handleAddTA();
        });
        
        taRemove.setOnAction(e -> {
            controller.handleRemoveTA();
        });
        
        removeProjData.setOnAction(e -> {
            controller.handleRemoveTeamData();
        });
        
        styleSheet.setOnAction(e -> {
            String style=styleSheet.getSelectionModel().getSelectedItem().toString();
            data.setStyleSheet(style);
            controller.markWorkAsEdited();
        });
        changeTimeButton.setOnAction(e -> {
            String startTime = (String) newStartTime.getValue();
            String endTime = (String) newEndTime.getValue();
            System.out.println(startTime);
            System.out.println(endTime);
            controller.handleChangeTime(startTime, endTime);

        });
        
       
       
       ender.setOnAction(new EventHandler() {
       String monthSetter;
       String daySetter;
        @Override
        public void handle(Event event) {
        System.out.println("here1");
        LocalDate date = ender.getValue();
        System.err.println("Selected date: " + date);
        AppYesNoDialogSingleton yesNoDialog = AppYesNoDialogSingleton.getSingleton();
        yesNoDialog.show(props.getProperty(UPDATE_DATE_TITLE), props.getProperty(UPDATE_DATE_MESSAGE));
        String selection = yesNoDialog.getSelection();
        if(selection.equals(AppYesNoDialogSingleton.YES)){
        Month month=date.getMonth();
        DayOfWeek day=date.getDayOfWeek();
        monthSetter=month.toString();
        daySetter=day.toString();
        String dateString=date.toString();
        String [] components=dateString.split("-");
        String yearString=components[0];
        String monthString=components[1];
        String dayString=components[2];
        String newDate=date.toString();
        if(components[2].charAt(0)==('0')){
                 dayString=dayString.substring(1);
             }
             if(components[1].charAt(0)==('0')){
                 monthString=monthString.substring(1);
             }
        
        if((!day.equals(DayOfWeek.FRIDAY))){
               System.out.println(day+"***");
               AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
               dialog.show(props.getProperty(INVALID_END), props.getProperty(INVALID_END_MESS));
            }
        
            else{
                System.out.println(data.getSchedEndMonthDate() + " " + data.getSchedEndMonthDay());
                String verdict=compareDates(newDate, data.getStartDate());
                
                if(verdict.equals("Date1 is before Date2")){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
               dialog.show(props.getProperty(INVALID_DATE), props.getProperty(INVALID_DATEMESS));
                }
                
                else if(verdict.equals("Date1 is after Date2")){
                    data.setSchedEndDayDate(dayString);
                    data.setSchedEndMonthDate(monthString);
                    data.setEndYear(yearString);
                    data.setEndDate(newDate);
                controller.handleEndDateChange(date);
                }
                else if(verdict.equals("Date is equal Date2")){
               AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
               dialog.show(props.getProperty(INVALID_DATE), props.getProperty(INVALID_DATEMESS));
                }
            
           
    }
        controller.markWorkAsEdited();
        }
            }
 
        });
       
       starter.setOnAction(new EventHandler() {
       String monthSetter;
       String daySetter;
        @Override
        public void handle(Event event) {
        LocalDate date = starter.getValue();
        System.err.println("Selected date: " + date);
        AppYesNoDialogSingleton yesNoDialog = AppYesNoDialogSingleton.getSingleton();
        yesNoDialog.show(props.getProperty(UPDATE_DATE_TITLE), props.getProperty(UPDATE_DATE_MESSAGE));
        String selection = yesNoDialog.getSelection();
        if(selection.equals(AppYesNoDialogSingleton.YES)){
        Month month=date.getMonth();
        DayOfWeek day=date.getDayOfWeek();
        monthSetter=month.toString();
        daySetter=day.toString();
        String dateString=date.toString();
        String [] components=dateString.split("-");
        String yearString=components[0];
        String monthString=components[1];
        String dayString=components[2];
        if(components[2].charAt(0)==('0')){
                 dayString=dayString.substring(1);
             }
             if(components[1].charAt(0)==('0')){
                 monthString=monthString.substring(1);
             }
        String newDate=date.toString();
        if(day.equals(DayOfWeek.MONDAY)==false){
               AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
               dialog.show(props.getProperty(INVALID_START), props.getProperty(INVALID_START_MESS));
            }
        
            else{
                System.out.println(data.getSchedEndMonthDate() + " " + data.getSchedEndMonthDay());
                String verdict=compareDates(newDate, data.getEndDate());
                
                if(verdict.equals("Date1 is before Date2")){
                    data.setStartDate(newDate);
                    data.setSchedStartMonthDate(monthString);
                    data.setSchedStartDayDate(dayString);
                    data.setStartYear(yearString);
                controller.handleStartDate(date);
                }
                
                else if(verdict.equals("Date1 is after Date2")){
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_END), props.getProperty(INVALID_END_MESS));
                }
                else if(verdict.equals("Date is equal Date2")){
               AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
               dialog.show(props.getProperty(INVALID_DATE), props.getProperty(INVALID_DATEMESS));
                }
            
           
    }
      controller.markWorkAsEdited();
        }
            }
 
        });

        
        changeDates.setOnAction(e -> {
            String startDate = starter.getValue().toString();
            String endDate = ender.getValue().toString();
            System.out.println(startDate);
            System.out.println(endDate);
            LocalDate localDate = starter.getValue();
            Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
            Date date = Date.from(instant);
            String [] dayOfWeek=date.toString().split(" ");
            LocalDate localDate2 = ender.getValue();
            Instant instant2 = Instant.from(localDate2.atStartOfDay(ZoneId.systemDefault()));
            Date date2 = Date.from(instant2);
            String [] dayOfWeek2=date2.toString().split(" ");
            controller.handleChangeDate(startDate, endDate, dayOfWeek[0], dayOfWeek2[0]);

        });
        updateTaButton.setOnAction(e -> {
            controller.handleUpdateTA();
            System.out.println("Update TA");
        });
        clearButton.setOnAction(e -> {
            addBox.getChildren().add(addButton);
            addBox.getChildren().add(clearButton1);
            addBox.getChildren().remove(updateTaButton);
            addBox.getChildren().remove(clearButton);
            nameTextField.clear();
            emailTextField.clear();
            nameTextField.setPromptText(namePromptText);
            emailTextField.setPromptText(emailPromptText);

        });
        clearButton1.setOnAction(e -> {
            nameTextField.clear();
            emailTextField.clear();
            nameTextField.setPromptText(namePromptText);
            emailTextField.setPromptText(emailPromptText);
        });
        
        clearProj.setOnAction(e -> {
            nameText.clear();
            linkProjText.clear();
            picker1.setValue(Color.WHITE);
            picker2.setValue(Color.WHITE);
            addUpdateProj.setText(props.getProperty(TAManagerProp.ADD_PROJ.toString()));
            
        });
        
        clearSchedData.setOnAction(e -> {
            timeText.setDisable(false);
            topicText.setDisable(false);
            criteriaText.setDisable(false);
            linkText.setDisable(false);
            controller.clearSchedData();
            addUpSchedData.setText(props.getProperty(TAManagerProp.ADD_SCHED.toString()));
        });

        workspace.setOnKeyPressed(e -> {
            if (e.isControlDown() && e.getCode() == (KeyCode.Y)) {
                System.out.println("Workspace Control Y");
                controller.handleReDoTransaction();
            } else if (e.isControlDown() && e.getCode() == (KeyCode.Z)) {
                System.out.println("Workspace Control Z Pressed");
                controller.handleUndoTransaction();
            }

        });
        
        cse.valueProperty().addListener(new ChangeListener<String>() {
        @Override public void changed(ObservableValue ov, String t, String t1) {
          System.out.println(ov);
            System.out.println(t);
            System.out.println(t1);
            data.setSubject(t1);
            controller.markWorkAsEdited();
        }    
    });
        
        sem.valueProperty().addListener(new ChangeListener<String>() {
        @Override public void changed(ObservableValue ov, String t, String t1) {
          System.out.println(ov);
            System.out.println(t);
            System.out.println(t1);
            data.setSem(t1);
            controller.markWorkAsEdited();
        }    
    });
        
        yearBox.valueProperty().addListener(new ChangeListener<String>() {
        @Override public void changed(ObservableValue ov, String t, String t1) {
          System.out.println(ov);
            System.out.println(t);
            System.out.println(t1);
            data.setYear(t1);
            controller.markWorkAsEdited();
        }    
    });
        
        num.valueProperty().addListener(new ChangeListener<String>() {
        @Override public void changed(ObservableValue ov, String t, String t1) {
          System.out.println(ov);
            System.out.println(t);
            System.out.println(t1);
            data.setNum(t1);
            controller.markWorkAsEdited();
        }    
    });

        taTable.setFocusTraversable(true);
        taTable.setOnKeyPressed(e -> {
            controller.handleKeyPress(e.getCode());
        });
        recTable.setOnKeyPressed(e -> {
            controller.handleKeyPressForRec(e.getCode());
        });
        
        schedTable.setOnKeyPressed(e -> {
            controller.handleKeyPressForSched(e.getCode());
        });
        
        projTable.setOnKeyPressed(e -> {
            controller.handleKeyPressForTeams(e.getCode());
        });
        
        projStudentTable.setOnKeyPressed(e -> {
            controller.handleKeyPressForStudents(e.getCode());
        });
        taTable.setOnMousePressed(e -> {
            addBox.getChildren().clear();
            controller.handleTaClicked(workspace, addBox);
            System.out.println("Clicked TA");
        });
        
        projTable.setOnMousePressed(e -> {
            controller.handleProjClicked();
            addUpdateProj.setText(props.getProperty(TAManagerProp.UPDATE_PROJ.toString()));
        });
        
         schedTable.setOnMousePressed(e -> {
            controller.handleSchedClicked(workspace);
            addUpSchedData.setText(props.getProperty(TAManagerProp.UPDATE_SCHED.toString()));
            System.out.println("Clicked Recitation");
        });
         
         recTable.setOnMousePressed(e -> {
            controller.handleRecitationClicked(workspace);
            addUpdate.setText(props.getProperty(TAManagerProp.UPDATE_REC_BUTTON_TEXT.toString()));
            System.out.println("Clicked Recitation");
        });
         
         projStudentTable.setOnMousePressed(e -> {
            controller.handleStudentProjClicked();
            addUpdateStudent.setText(props.getProperty(TAManagerProp.UPDATE_PROJ.toString()));
            System.out.println("Clicked Recitation");
        });
         
         clear.setOnAction(e -> {
            sectionField.clear();
            locationField.clear();
            instructorField.clear();
            daytimeField.clear();
            superTACombo1.getSelectionModel().select("TA1");
            superTACombo1.getSelectionModel().select("TA2");
            addUpdate.setText(props.getProperty(TAManagerProp.ADD_REC_BUTTON_TEXT.toString()));
        });
         
         clearStudentButton.setOnAction(e -> {
            lastNameText.clear();
            firstNameText.clear();
            teamText.getSelectionModel().select("Team");
            roleText.clear();
            addUpdateStudent.setText(props.getProperty(TAManagerProp.ADDUPDATE_LABEL.toString()));
        });
         
         addUpdate.setOnAction(e -> {
             if(addUpdate.getText().equals(props.getProperty(TAManagerProp.ADD_REC_BUTTON_TEXT.toString()))){
                 controller.handleAddRec();
             }
             else if(addUpdate.getText().equals(props.getProperty(TAManagerProp.UPDATE_REC_BUTTON_TEXT.toString()))){
             controller.handleUpdateREC();
         }
             else if(addUpdate.getText().equals(props.getProperty(TAManagerProp.ADDUPDATE_LABEL.toString()))){
             controller.handleAddRec();
         }
           
        });
         
         addUpdateStudent.setOnAction(e -> {
             if(addUpdateStudent.getText().equals(props.getProperty(TAManagerProp.ADD_PROJ.toString()))){
                 controller.handleAddProjectStudentData();
             }
             else if(addUpdateStudent.getText().equals(props.getProperty(TAManagerProp.UPDATE_PROJ.toString()))){
             controller.handleUpdateProjStudentData();
         }
             else if(addUpdateStudent.getText().equals(props.getProperty(TAManagerProp.ADDUPDATE_LABEL.toString()))){
             controller.handleAddProjectStudentData();
         }
             else if(addUpdateStudent.getText().equals(props.getProperty(TAManagerProp.ADDEDIT_LABEL.toString()))){
             controller.handleAddProjectStudentData();
         }
           
        });
         
         addUpdateProj.setOnAction(e -> {
             if(addUpdateProj.getText().equals(props.getProperty(TAManagerProp.ADD_PROJ.toString()))){
                 controller.handleAddProjectData();
             }
             else if(addUpdateProj.getText().equals(props.getProperty(TAManagerProp.UPDATE_PROJ.toString()))){
             controller.handleUpdateProj();
         }
             else if(addUpdateProj.getText().equals(props.getProperty(TAManagerProp.ADDEDIT_LABEL.toString()))){
                 System.out.println("gothere");
             controller.handleAddProjectData();
         }
           
        });
         
         
         
         
         addUpSchedData.setOnAction(e -> {
             if(addUpSchedData.getText().equals(props.getProperty(TAManagerProp.ADD_SCHED.toString()))){
                 String date="";
                 try{
                     date=dateBox.getValue().toString();
                 }
                 catch(Exception k){
                     date="";
                 }
                 controller.handleAddSched(date);
             }
             else if(addUpSchedData.getText().equals(props.getProperty(TAManagerProp.UPDATE_SCHED.toString()))){
             controller.handleUpdateSched();
         }
             else if(addUpSchedData.getText().equals(props.getProperty(TAManagerProp.ADDUPDATE_LABEL.toString()))){
                 String date="";
                 try{
                     date=dateBox.getValue().toString();
                 }
                 catch(Exception k){
                     date="";
                 }
             controller.handleAddSched(date);
         }
           
        });
         
         removeRecitation.setOnAction(e -> {
             controller.handleRemoveRec();
        });
         
         titleTextField.textProperty().addListener((observable, oldValue, newValue) -> {
         data.setTitleTextField(newValue);
         System.out.println(data.getTitleTextField());
});
         homeTextField.textProperty().addListener((observable, oldValue, newValue) -> {
         data.setHomeTextField(newValue);
         System.out.println(data.getTitleTextField());
});
         nameCourseTextField.textProperty().addListener((observable, oldValue, newValue) -> {
         data.setNameCourseTextField(newValue);
         System.out.println(data.getTitleTextField());
});
         
        type.valueProperty().addListener(new ChangeListener<String>() {
        @Override public void changed(ObservableValue ov, String t, String t1) {
          if(t1.equals("Holiday")){
            timeText.clear();
            topicText.clear();
            criteriaText.clear();
            timeText.setDisable(true);
            topicText.setDisable(true);
            criteriaText.setDisable(true);
            linkText.setDisable(false);
          }
          if(t1.equals("Lecture")){
              criteriaText.clear();
              timeText.clear();
            criteriaText.setDisable(true);
            timeText.setDisable(true);
            topicText.setDisable(false);
            linkText.setDisable(false);
          }
          if(t1.equals("Reference")){
            timeText.setDisable(true);
            criteriaText.setDisable(true);
            topicText.setDisable(false);
            linkText.setDisable(false);
          }
          if(t1.equals("Recitation")){
              timeText.clear();
              linkText.clear();
              criteriaText.clear();
            timeText.setDisable(true);
            linkText.setDisable(true);
            criteriaText.setDisable(true);
            topicText.setDisable(false);
          }
          if(t1.equals("HW")){
              timeText.clear();
              linkText.clear();
              criteriaText.clear();
              topicText.clear();
            timeText.setDisable(false);
            linkText.setDisable(false);
            criteriaText.setDisable(false);
            topicText.setDisable(false);
            
          }
        }    
    });
        
         removeSchedItem.setOnAction(e -> {
             controller.handleRemoveSched();
        });
         
         removeStudent.setOnAction(e -> {
             controller.handleRemoveStudentProjData();
        });

    }

    // WE'LL PROVIDE AN ACCESSOR METHOD FOR EACH VISIBLE COMPONENT
    // IN CASE A CONTROLLER OR STYLE CLASS NEEDS TO CHANGE IT
    
    public ComboBox getSubject(){
            return cse;
        }
    
    public ComboBox getNum(){
        return num;
    }
    
    public ComboBox getYearBox(){
        return yearBox;
    }
    
    public ComboBox getSem(){
        return sem;
    }
    
    public HBox getTAsHeaderBox() {
        return tasHeaderBox;
    }

    public Label getTAsHeaderLabel() {
        return tasHeaderLabel;
    }
    
    public Text getDirAdd(){
        return dirAdd;
    }
    
    public Text getExportLabelCourseDetails(){
        return exportCourseDetails;
    }

    public TableView getTATable() {
        return taTable;
    }
    
    public ArrayList<Button> getChangeButtonList(){
        return courseDetailsButtons;
    }
    
    public Button getTaRemoveButton(){
        return taRemove;
    }
    
    public Text getTempAdd(){
        return tempAdd;
    }

    public HBox getAddBox() {
        return addBox;
    }
    
    public Label getAddEdit(){
        return addEdit;
    }
    
    public Text getSubj(){
        return subj;
    }
    
    public Text getTempMessage(){
        return tempMessage;
    }

    public TextField getNameTextField() {
        return nameTextField;
    }

    public TextField getEmailTextField() {
        return emailTextField;
    }
    
    public ArrayList getRecLabels(){
        return recLabels;
    }
    
    public ComboBox getSuperTA1(){
        return superTACombo1;
    }

    public ComboBox getSuperTA2(){
        return superTACombo2;
    }
    public Button getAddButton() {
        return addButton;
    }
    
    public TableView getSchedTable(){
        return schedTable;
    }
    
    public ScrollPane getScrollPane(){
        return scroll;
    }
    
    public ColorPicker getColorPicker1(){
        return picker1;
    }
    
    public Text getCalendarLabel(){
        return calendarLabel;
    }
    
    public ColorPicker getColorPicker2(){
        return picker2;
    }
    
    
    public Text getTitle(){
        return title;
    }
    
    public Text getNumber(){
        return number;
    }
    
    public Text getSemester(){
        return semester;
    }
    
    
    public Text getSitePages(){
        return sitePages;
    }
    
    public TextField getNameCourseTextField(){
        return nameCourseTextField;
    }
    
    public TextField getHomeTextField(){
        return homeTextField;
    }
    
    public TextField getTitleTextField(){
        return titleTextField;
    }
    
    public Text getBannerImage(){
        return bannerImage;
    }
    
    public Text getLeftFooter(){
        return leftFooter;
    }
    
    public Text getRightFooter(){
        return rightFooter;
    }
    
    public Text getStyleMess(){
        return styleMess;
    }
    
    public Text getStyleLabel(){
        return styleLabel;
    }
    public Text getYear(){
        return year;
    }
    
    public Text getiName(){
        return iName;
    }
    
    public Text getiHome(){
        return iHome;
    }

    public Button getUpdateTaButton() {
        return updateTaButton;
    }
    
    public Label getSchedItemLabel(){
        return schedItemLabel;
    }
    
    public TableView getTable(){
        return templateTable;
    }
    
    public Label getProjLabel(){
        return projLabel;
    }

    public Button getClearButton() {
        return clearButton;
    }

    public Button getClearButton1() {
        return clearButton1;
    }

    public Button getChangeTimeButton() {
        return changeTimeButton;
    }
    
    public Label getRecLabel(){
        return recitation;
    }

    public ComboBox getNewStartBox() {
        return newStartTime;
    }

    public ComboBox getNewEndBox() {
        return newEndTime;

    }
    
    public Label getAddUpdateLabel(){
        return addUpdateLabel;
    }
    
    public TableView getRecTable(){
        return recTable;
    }
    
    public TableView getProjTable(){
    return projTable;
    }
    
    public TableView getProjStudentTable(){
        return projStudentTable;
    }

    public HBox getOfficeHoursSubheaderBox() {
        return officeHoursHeaderBox;
    }
    
    public DatePicker getStarter(){
        return starter;
    }
    
    public DatePicker getEnder(){
        return ender;
    }
    
    public Label getStudentsLabel(){
        return students;
    }
    
    public Label getAddEditStudentsLabel(){
        return addEditStudentsData;
    }
    
    public TextField getTimeSchedTextField(){
        return timeText;
    }
    
    public TextField getTitleSchedTextField(){
        return titleText;
    }
    
    public TextField getTopicSchedTextField(){
        return topicText;
    }
    
    public TextField getLinkSchedTextField(){
        return linkText;
    }
    
    
    public TextField getCriteriaSchedTextField(){
        return criteriaText;
    }
    
    public DatePicker getDateBox(){
        return dateBox;
    }
    
    public ComboBox getHolidayPicker(){
        return type;
    }

    public TAController getController() {
        return controller;
    }
    
    public TextField getProjNameTextField(){
        return nameText;
    }
    
    public TextField getProjLinkTextField(){
        return linkProjText;
    }

    public TableView getTemplate(){
        return templateTable;
    }
    
    public Label getTeamLabel(){
        return teamLabel;
    }
    
    public TextField getSection(){
        return sectionField;
    }
    
    public TextField getLocation(){
        return locationField;
    }
    
    public TextField getTime(){
        return daytimeField;
    }
    
    public TextField getInstructor(){
        return instructorField;
    }
    public Label getAddEditProjData(){
        return addeditProjData;
    }
    
    public Text getTemplateLabel(){
        return template;
    }
    public Label getOfficeHoursSubheaderLabel() {
        return officeHoursHeaderLabel;
    }

    public GridPane getOfficeHoursGridPane() {
        return officeHoursGridPane;
    }
    
    public Label getCourseInfoLabel(){
        return courseInfo;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeHeaderPanes() {
        return officeHoursGridTimeHeaderPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeHeaderLabels() {
        return officeHoursGridTimeHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridDayHeaderPanes() {
        return officeHoursGridDayHeaderPanes;
    }
    
    public ArrayList<ComboBox> getList(){
        return boxes;
    }

    public HashMap<String, Label> getOfficeHoursGridDayHeaderLabels() {
        return officeHoursGridDayHeaderLabels;
    }

    public HashMap<String, Pane> getOfficeHoursGridTimeCellPanes() {
        return officeHoursGridTimeCellPanes;
    }
    
    public TableColumn getDateCol(){
        return dateColumn;
    }
    
    public TabPane getTab(){
        return tabs;
    }

    public HashMap<String, Label> getOfficeHoursGridTimeCellLabels() {
        return officeHoursGridTimeCellLabels;
    }
    
    public Label getPageStyle(){
        return pageStyle;
    }
    
    public Label getScheduleLabel(){
        return schedule;
    }
    
    public TextField getStudentProjFirstNameText(){
        return firstNameText;
    }
    
    public TextField getStudentProjRoleText(){
        return roleText;
    }
    
    public ComboBox getStudentProjTeamText(){
        return teamText;
    }
    
    public TextField getStudentProjLastNameText(){
        return lastNameText;
    }

    public HashMap<String, Pane> getOfficeHoursGridTACellPanes() {
        return officeHoursGridTACellPanes;
    }

    public HashMap<String, Label> getOfficeHoursGridTACellLabels() {
        return officeHoursGridTACellLabels;
    }

    public String getCellKey(Pane testPane) {
        for (String key : officeHoursGridTACellLabels.keySet()) {
            if (officeHoursGridTACellPanes.get(key) == testPane) {
                return key;
            }
        }
        return null;
    }

    public Label getTACellLabel(String cellKey) {
        return officeHoursGridTACellLabels.get(cellKey);
    }

    public Pane getTACellPane(String cellPane) {
        return officeHoursGridTACellPanes.get(cellPane);
    }

    public String buildCellKey(int col, int row) {
        return "" + col + "_" + row;
    }

    public String buildCellText(int militaryHour, String minutes) {
        // FIRST THE START AND END CELLS
        int hour = militaryHour;
        if (hour > 12) {
            hour -= 12;
        }
        String cellText = "" + hour + ":" + minutes;
        if (militaryHour < 12) {
            cellText += "am";
        } else {
            cellText += "pm";
        }
        return cellText;
    }
    
    public String compareDates(String d1,String d2){
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date1 = sdf.parse(d1);
            Date date2 = sdf.parse(d2);

            System.out.println("Date1"+sdf.format(date1));
            System.out.println("Date2"+sdf.format(date2));System.out.println();

            // Create 2 dates ends
            //1

            // Date object is having 3 methods namely after,before and equals for comparing
            // after() will return true if and only if date1 is after date 2
            if(date1.after(date2)){
                System.out.println("Date1 is after Date2");
                return "Date1 is after Date2";
            }
            // before() will return true if and only if date1 is before date2
            if(date1.before(date2)){
                System.out.println("Date1 is before Date2");
                return "Date1 is before Date2";
            }

            //equals() returns true if both the dates are equal
            if(date1.equals(date2)){
                System.out.println("Date1 is equal Date2");
                return "Date1 is equal Date2";
            }

            System.out.println();
        }
        catch(ParseException ex){
            ex.printStackTrace();
        }
        return null;
    }

    @Override
    public void resetWorkspace() {
        counter=counter+1;
        // CLEAR OUT THE GRID PANE
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        officeHoursGridPane.getChildren().clear();
        recTable.getItems().clear();
        projTable.getItems().clear();
        projStudentTable.getItems().clear();
        schedTable.getItems().clear();
        num.getSelectionModel().clearSelection();
        cse.getSelectionModel().clearSelection();
        yearBox.getSelectionModel().clearSelection();
        sem.getSelectionModel().clearSelection();
        num.setPromptText("Number");
        cse.setPromptText("Subject");
        yearBox.setPromptText("Year");
        sem.setPromptText("Semester");
        dirAdd.setText(props.getProperty(TAManagerProp.DIRECTORY_ADD.toString()));
        tempAdd.setText(props.getProperty(TAManagerProp.TEMPLATE_ADD.toString()));
        titleTextField.clear();
        nameCourseTextField.clear();
        homeTextField.clear();
        Image img2 = new Image("file:C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\images\\rightimage.png");
        imgView2.setImage(img2);
        Image img = new Image("file:C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\images\\leftimage.png");
        imgView.setImage(img);
        Image img3 = new Image("file:C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\images\\bannerimage.png");
        imgView3.setImage(img3);
        styleSheet.setPromptText("Style");
        for(CoursePage x: al){
            x.setCheckBox(false);
        }
        templateTable.refresh();
        // AND THEN ALL THE GRID PANES AND LABELS
        officeHoursGridTimeHeaderPanes.clear();
        officeHoursGridTimeHeaderLabels.clear();
        officeHoursGridDayHeaderPanes.clear();
        officeHoursGridDayHeaderLabels.clear();
        officeHoursGridTimeCellPanes.clear();
        officeHoursGridTimeCellLabels.clear();
        officeHoursGridTACellPanes.clear();
        officeHoursGridTACellLabels.clear();
    }
    
    public final LocalDate LOCAL_DATE (String dateString){
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    LocalDate localDate = LocalDate.parse(dateString, formatter);
    return localDate;
        }
    
    public void reloadCourseSite(TAData data){
        titleTextField.setText(data.getTitleTextField());
        cse.getSelectionModel().select(data.getSubj());
        num.getSelectionModel().select(data.getNum());
        sem.getSelectionModel().select(data.getSem());
        yearBox.getSelectionModel().select(data.getYear());
        nameCourseTextField.setText(data.getNameCourseTextField());
        homeTextField.setText(data.getHomeTextField());
        titleTextField.setText(data.getTitleTextField());
        dirAdd.setText(data.getExportDir());
        tempAdd.setText(data.getTemplateDir());
        coursePage1.setCheckBox(data.getHome());
        coursePage2.setCheckBox(data.getSyllabus());
        coursePage3.setCheckBox(data.getSchedule());
        coursePage4.setCheckBox(data.getHWS());
        coursePage5.setCheckBox(data.getProjects());
        Image tempImage= new Image("file:"+data.getBannerImage());
         int index=data.getBannerImage().lastIndexOf("\\");
         String tempFile=data.getBannerImage().substring(index+1);
         data.setBannerExport("./images/"+tempFile);
        Image tempImage2= new Image("file:"+data.getRightFooterImage());
         int index2=data.getRightFooterImage().lastIndexOf("\\");
         String tempFile2=data.getRightFooterImage().substring(index2+1);
         data.setRightExport("./images/"+tempFile2);
        Image tempImage3= new Image("file:" +data.getLeftFooterImage());
         int index3=data.getLeftFooterImage().lastIndexOf("\\");
         String tempFile3=data.getLeftFooterImage().substring(index3+1);
         data.setLeftExport("./images/"+tempFile3);
        imgView3.setImage(tempImage2);
        imgView2.setImage(tempImage3);
        imgView.setImage(tempImage);
        styleSheet.getSelectionModel().select(data.getStyleSheet());
        String startDay=data.getSchedStartDayDate();
        String startMonth=data.getSchedStartMonthDate();
        String endDay=data.getSchedEndMonthDay();
        System.out.println(endDay);
        String endMonth=data.getSchedEndMonthDate();
        String startYear=data.getStartYear();
        String endYear=data.getEndYear();
        if(endMonth!=" "){
        int endDayInt= Integer.parseInt(endDay);
        int endMonthInt=Integer.parseInt(endMonth);
        int startDayInt=Integer.parseInt(startDay);
        int startMonthInt=Integer.parseInt(startMonth);
        int endYearInt=Integer.parseInt(endYear);
        int startYearInt=Integer.parseInt(startYear);
        String thisYear= startMonth+"-"+startDay+"-"+"2017";
        final DatePicker date= new DatePicker(LocalDate.now());
        starter.setValue(LocalDate.of(startYearInt, startMonthInt, startDayInt));
        ender.setValue(LocalDate.of(endYearInt, endMonthInt, endDayInt));
        if(startDay.charAt(0)=='0'){
            System.out.println("gothere");
                 startDay=startDay.substring(1);
                 data.setSchedStartDayDate(startDay);
             }
             if(startMonth.charAt(0)=='0'){
                 startMonth=startMonth.substring(1);
                 data.setSchedStartMonthDate(startMonth);
             }
             if(endDay.charAt(0)=='0'){
            System.out.println("gothere");
                 endDay=endDay.substring(1);
                 data.setSchedEndDayDate(endDay);
             }
             if(endMonth.charAt(0)=='0'){
                 endMonth=endMonth.substring(1);
                 data.setSchedEndMonthDate(endMonth);
             }
        }
    }



    @Override
    public void reloadWorkspace(AppDataComponent dataComponent) {
        TAData taData = (TAData) dataComponent;
        /*
        cse.getSelectionModel().select(testSave.subj);
        num.getSelectionModel().select(testSave.number);
        sem.getSelectionModel().select(testSave.semester);
        yearBox.getSelectionModel().select(testSave.year);
        nameCourseTextField.setText(testSave.teacherName);
        homeTextField.setText(testSave.homeAddress);
        titleTextField.setText(testSave.title);
        dirAdd.setText(testSave.exportDir);
        tempAdd.setText(testSave.templateDir);
        coursePage1.setCheckBox(testSave.home);
        coursePage2.setCheckBox(testSave.syllabus);
        coursePage3.setCheckBox(testSave.schedule);
        coursePage4.setCheckBox(testSave.hws);
        coursePage5.setCheckBox(testSave.projects);
        Image imgNew= new Image("file:C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\images\\TAManagerLogo.png");
        //Image imgNew2= new Image("file:C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\images\\TAManagerLogo.png");
        imgView3.setImage(imgNew);
        styleSheet.getSelectionModel().select(testSave.stylesheet);
        String startDay=testSave.startMondayDay;
        String startMonth=testSave.startMondayMonth;
        String endDay=testSave.endFridayDay;
        System.out.println(endDay);
        String endMonth=testSave.endFridayMonth;
        if(endMonth!=" "){
        int endDayInt= Integer.parseInt(endDay);
        int endMonthInt=Integer.parseInt(endMonth);
        int startDayInt=Integer.parseInt(startDay);
        int startMonthInt=Integer.parseInt(startMonth);
        String thisYear= startMonth+"-"+startDay+"-"+"2017";
        //starter.setConverter(LOCAL_DATE(thisYear));
        //ender.setValue(LocalDate.of(2017,endDayInt , endMonthInt));
        final DatePicker date= new DatePicker(LocalDate.now());
        starter.setOnAction(new EventHandler(){
            public void handle(Event t){
                LocalDate thisDate=date.getValue();
            }
        });
        starter.setValue(LocalDate.of(2017, startMonthInt, startDayInt));
        ender.setValue(LocalDate.of(2017, endMonthInt, endDayInt));
        }
*/
        reloadOfficeHoursGrid(taData);
    }
   

    public void reloadOfficeHoursGrid(TAData dataComponent) {
        ArrayList<String> gridHeaders = dataComponent.getGridHeaders();

        // ADD THE TIME HEADERS
        for (int i = 0; i < 2; i++) {
            addCellToGrid(dataComponent, officeHoursGridTimeHeaderPanes, officeHoursGridTimeHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }

        // THEN THE DAY OF WEEK HEADERS
        for (int i = 2; i < 7; i++) {
            addCellToGrid(dataComponent, officeHoursGridDayHeaderPanes, officeHoursGridDayHeaderLabels, i, 0);
            dataComponent.getCellTextProperty(i, 0).set(gridHeaders.get(i));
        }

        // THEN THE TIME AND TA CELLS
        int row = 1;
        for (int i = dataComponent.getStartHour(); i < dataComponent.getEndHour(); i++) {
            // START TIME COLUMN
            int col = 0;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(i, "00"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row + 1);
            dataComponent.getCellTextProperty(col, row + 1).set(buildCellText(i, "30"));

            // END TIME COLUMN
            col++;
            int endHour = i;
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row);
            dataComponent.getCellTextProperty(col, row).set(buildCellText(endHour, "30"));
            addCellToGrid(dataComponent, officeHoursGridTimeCellPanes, officeHoursGridTimeCellLabels, col, row + 1);
            dataComponent.getCellTextProperty(col, row + 1).set(buildCellText(endHour + 1, "00"));
            col++;

            // AND NOW ALL THE TA TOGGLE CELLS
            while (col < 7) {
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row);
                addCellToGrid(dataComponent, officeHoursGridTACellPanes, officeHoursGridTACellLabels, col, row + 1);
                col++;
            }
            row += 2;
        }

        // CONTROLS FOR TOGGLING TA OFFICE HOURS
        for (Pane p : officeHoursGridTACellPanes.values()) {
            p.setFocusTraversable(true);
            p.setOnKeyPressed(e -> {
                controller.handleKeyPress(e.getCode());
            });
            p.setOnMouseClicked(e -> {
                controller.handleCellToggle((Pane) e.getSource());
            });
            p.setOnMouseExited(e -> {
                controller.handleGridCellMouseExited((Pane) e.getSource());
            });
            p.setOnMouseEntered(e -> {
                controller.handleGridCellMouseEntered((Pane) e.getSource());
            });
        }

        // AND MAKE SURE ALL THE COMPONENTS HAVE THE PROPER STYLE
        TAStyle taStyle = (TAStyle) app.getStyleComponent();
        taStyle.initOfficeHoursGridStyle();
    }

    public void addCellToGrid(TAData dataComponent, HashMap<String, Pane> panes, HashMap<String, Label> labels, int col, int row) {
        // MAKE THE LABEL IN A PANE
        Label cellLabel = new Label("");
        HBox cellPane = new HBox();
        cellPane.setAlignment(Pos.CENTER);
        cellPane.getChildren().add(cellLabel);

        // BUILD A KEY TO EASILY UNIQUELY IDENTIFY THE CELL
        String cellKey = dataComponent.getCellKey(col, row);
        cellPane.setId(cellKey);
        cellLabel.setId(cellKey);

        // NOW PUT THE CELL IN THE WORKSPACE GRID
        officeHoursGridPane.add(cellPane, col, row);

        // AND ALSO KEEP IN IN CASE WE NEED TO STYLIZE IT
        panes.put(cellKey, cellPane);
        labels.put(cellKey, cellLabel);

        // AND FINALLY, GIVE THE TEXT PROPERTY TO THE DATA MANAGER
        // SO IT CAN MANAGE ALL CHANGES
        dataComponent.setCellProperty(col, row, cellLabel.textProperty());
    }
}
