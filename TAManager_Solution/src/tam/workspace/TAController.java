package tam.workspace;

import djf.ui.AppGUI;
import djf.controller.AppFileController;
import static djf.controller.AppFileController.copy;
import static djf.settings.AppPropertyType.EXPORT_COMPLETED_MESSAGE;
import static djf.settings.AppPropertyType.EXPORT_COMPLETED_TITLE;
import static djf.settings.AppPropertyType.EXPORT_TITLE;
import static djf.settings.AppPropertyType.LOAD_ERROR_MESSAGE;
import static djf.settings.AppPropertyType.LOAD_ERROR_TITLE;
import static djf.settings.AppStartupConstants.PATH_WORK;
import djf.ui.AppMessageDialogSingleton;
import djf.ui.AppYesNoCancelDialogSingleton;
import static tam.TAManagerProp.*;
import djf.ui.AppYesNoDialogSingleton;
import java.io.File;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.util.Callback;
import jtps.jTPS;
import jtps.jTPS_Transaction;
import properties_manager.PropertiesManager;
import tam.TAManagerApp;
import tam.TAManagerProp;
import tam.data.ProjectData;
import tam.data.ProjectStudentData;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.style.TAStyle;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_CELL;
import static tam.style.TAStyle.CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN;
import static tam.style.TAStyle.CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE;
import tam.workspace.TAWorkspace;

/**
 * This class provides responses to all workspace interactions, meaning
 * interactions with the application controls not including the file toolbar.
 *
 * @author Richard McKenna
 * @version 1.0
 */
public class TAController {

    // THE APP PROVIDES ACCESS TO OTHER COMPONENTS AS NEEDED
    TAManagerApp app;
    static jTPS jTPS = new jTPS();

    /**
     * Constructor, note that the app must already be constructed.
     */
    public TAController(TAManagerApp initApp) {
        // KEEP THIS FOR LATER
        app = initApp;
    }

    /**
     * This helper method should be called every time an edit happens.
     */
    public void markWorkAsEdited() {
        // MARK WORK AS EDITED
        AppGUI gui = app.getGUI();
        gui.getFileController().markAsEdited(gui);
    }

    /**
     * This method responds to when the user requests to add a new TA via the
     * UI. Note that it must first do some validation to make sure a unique name
     * and email address has been provided.
     */
    public void handleAddTA() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();
        String name = nameTextField.getText();
        String email = emailTextField.getText();
        EmailValidator checkEmail = new EmailValidator();

        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
        } // DOES A TA ALREADY HAVE THE SAME NAME OR EMAIL?
        else if (data.containsTA(name, email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_TITLE), props.getProperty(TA_NAME_AND_EMAIL_NOT_UNIQUE_MESSAGE));
        } // **********Check the TA Email Address for correct format 
        else if (!checkEmail.validate(email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));

        } // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            data.addTA(name, email, false);
            TeachingAssistant ta=data.getTA(name, email);
            jTPS_Transaction transaction1 = new AddTA_Transaction(name, email, ta.getUndergrad().getValue(), data);

            jTPS.addTransaction(transaction1);
            //jTPS.doTransaction();
            // CLEAR THE TEXT FIELDS
            nameTextField.setText("");
            emailTextField.setText("");

            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }
    
    public void handleAddProjectData() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TextField nameTextField = workspace.getProjNameTextField();
        TextField linkTextField = workspace.getProjLinkTextField();
        ColorPicker picker1= workspace.getColorPicker1();
        ColorPicker picker2=workspace.getColorPicker2();
        //String color = Integer.toHexString(picker1.getValue().hashCode());
        //String txtColor= Integer.toHexString(picker2.getValue().hashCode());
        String color = Integer.toHexString(picker1.getValue().hashCode()).substring(0, 6).toUpperCase();
        String txtColor=Integer.toHexString(picker2.getValue().hashCode()).substring(0, 6).toUpperCase();
        String name = nameTextField.getText();
        String link = linkTextField.getText();
        

        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (name.isEmpty() || link.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_INFO_TITLE2), props.getProperty(MISSING_INFO_MESS2));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        
        else {
            // ADD THE NEW TA TO THE DATA
            data.addProjData(name, color, txtColor, link);
            jTPS_Transaction transaction1 = new AddProj_Trans(name, color, txtColor, link, data, workspace);
            jTPS.addTransaction(transaction1);
            //jTPS.doTransaction();
            // CLEAR THE TEXT FIELDS
            
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }
    
      public void handleAddProjectStudentData() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TextField firstNameTextField = workspace.getStudentProjFirstNameText();
        TextField lastNameTextField=workspace.getStudentProjLastNameText();
        TextField roleTextField=workspace.getStudentProjRoleText();
        ComboBox teamTextField=workspace.getStudentProjTeamText();
        //String color = Integer.toHexString(picker1.getValue().hashCode());
        //String txtColor= Integer.toHexString(picker2.getValue().hashCode());
        String firstName=firstNameTextField.getText();
        String lastName=lastNameTextField.getText();
        String role=roleTextField.getText();
        String team="";
        try{
        team=teamTextField.getValue().toString();
        
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        

        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (firstName.isEmpty() || lastName.isEmpty() || team.isEmpty() || team.equals("Team")) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_INFO_TITLE3), props.getProperty(MISSING_INFO_MESSAGE3));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        if (!(role.equalsIgnoreCase("Lead Designer") || role.equalsIgnoreCase("Lead Programmer") || role.equalsIgnoreCase("Data Designer") || role.equalsIgnoreCase("Project Manager"))) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(UNIQUE_NAME_TITLE2), props.getProperty(UNIQUE_NAME_MESS2));
        }
        else {
            // ADD THE NEW TA TO THE DATA
            
            data.addProjStudentData(firstName, lastName, team, role);
            jTPS_Transaction transaction1 = new AddProjStudent_Trans(firstName, lastName, team, role, data, workspace);
            jTPS.addTransaction(transaction1);
            //jTPS.doTransaction();
            // CLEAR THE TEXT FIELDS
            
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
        }
        catch(Exception e){
        AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
        dialog.show(props.getProperty(MISSING_INFO_TITLE3), props.getProperty(MISSING_INFO_MESSAGE3));    
        }

        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        
    }
    
    public void handleRemoveTeamData() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();
        //String type, String date, String title, String topic, String link, String time, String criteria
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TableView teamTable=workspace.getProjTable();
        Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
        ProjectData proj = (ProjectData) selectedItem;
        String colorHex= proj.getColorHex();
        String txtColor=proj.getTxtColor();
        String teamName=proj.getName();
        String link=proj.getLink();

        //data.removeSched(type, date, title, topic, link, time, criteria);
        jTPS_Transaction transaction2 = new RemoveTeam_Trans(teamName, colorHex, txtColor, link, data, workspace);
        jTPS.addTransaction(transaction2);
        //data.removeTeam();
        
        //jTPS_Transaction transaction1 = new DeleteRec_Trans(section, instructor, time, location, ta1, ta2, data);
        //jTPS.addTransaction(transaction1);
        
        
        
    }
    
    public void handleRemoveStudentProjData() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();
        //String type, String date, String title, String topic, String link, String time, String criteria
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TableView projTable=workspace.getProjStudentTable();
        Object selectedItem = projTable.getSelectionModel().getSelectedItem();
        ProjectStudentData proj = (ProjectStudentData) selectedItem;
        String firstName=proj.getFirstName();
        String lastName=proj.getLastName();
        String role=proj.getRole();
        String team= proj.getTeam();

        //data.removeSched(type, date, title, topic, link, time, criteria);
        jTPS_Transaction transaction2 = new RemoveProjStudentData_Trans(firstName, lastName, team, role, data, workspace);
        jTPS.addTransaction(transaction2);
        
        //jTPS_Transaction transaction1 = new DeleteRec_Trans(section, instructor, time, location, ta1, ta2, data);
        //jTPS.addTransaction(transaction1);
        
        
        
    }
    
     public void handleRemoveSched() {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();
        //String type, String date, String title, String topic, String link, String time, String criteria
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TableView schedTable = workspace.getSchedTable();
        Object selectedItem = schedTable.getSelectionModel().getSelectedItem();
        ScheduleData sched = (ScheduleData) selectedItem;
        String type=sched.getType();
        String date=sched.getDate();
        String title=sched.getTitle();
        String topic=sched.getTopic();
        String link=sched.getLink();
        String time=sched.getTime();
        String criteria=sched.getCriteria();
        System.out.println(type);

        //data.removeSched(type, date, title, topic, link, time, criteria);
        jTPS_Transaction transaction1 = new DeleteSched_Trans(type, date, title, topic, link, time, criteria, data, workspace);
        jTPS.addTransaction(transaction1);
        
        //jTPS_Transaction transaction1 = new DeleteRec_Trans(section, instructor, time, location, ta1, ta2, data);
        //jTPS.addTransaction(transaction1);
        
        
        
    }
    
    public void handleAddSched(String date) {
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TextField timeTextField=workspace.getTimeSchedTextField();
        TextField titleTextField=workspace.getTitleSchedTextField();
        TextField topicTextField=workspace.getTopicSchedTextField();
        TextField linkTextField=workspace.getLinkSchedTextField();
        TextField criteriaTextField=workspace.getCriteriaSchedTextField();
        ComboBox holidayPicker=workspace.getHolidayPicker();
        DatePicker datePicker=workspace.getDateBox();
        String time = timeTextField.getText();
        String title=titleTextField.getText();
        String topic=topicTextField.getText();
        String link=linkTextField.getText();
        String criteria=criteriaTextField.getText();
        String holiday="";
        String updatedDate="";
        
        
        try{
        holiday= holidayPicker.getValue().toString();
        System.out.println(date);
        if (holiday.equals("Type")){
            holiday="";
        }
        if(date.equals("Date")){
            date="";
        }
        if(topic.equals("")){
            topic="";
        }
        if(time.equals("")){
            time="";
        }
        if(title.equals("")){
            title="";
        }
        if(date.isEmpty()==false){
            String [] components=date.split("-");
            String year=components[0];
            String month=components[1];
            String day=components[2];
            if(month.charAt(0)=='0'){
                month=month.substring(1);
            }
            if(day.charAt(0)=='0'){
                day=day.substring(1);
            }
            date=month+"/"+day+"/"+year;
        }
        data.addSchedData(holiday, date, title, topic, link, time, criteria);
        jTPS_Transaction transaction1 = new AddSched_Trans(holiday, date, title, topic, link, time, criteria, data, workspace);
        jTPS.addTransaction(transaction1);
        
        }
        catch(Exception e){
            if(holiday.equals(null)){
                holiday="";
            }
            if(date.equals(null)){
                date="";
            }
            if(topic.equals("")){
            topic="";
            }
            if(time.equals("")){
            time="";
            }
            if(title.equals("")){
            title="";
            }
            if(date.isEmpty()==false){
            String [] components=date.split("-");
            String year=components[0];
            String month=components[1];
            String day=components[2];
            if(month.charAt(0)=='0'){
                month=month.substring(1);
            }
            if(day.charAt(0)=='0'){
                day=day.substring(1);
            }
            date=month+"/"+day+"/"+year;
        }
            System.out.println(date+ "**");
           data.addSchedData(holiday, date, title, topic, link, time, criteria);
           jTPS_Transaction transaction1 = new AddSched_Trans(holiday, date, title, topic, link, time, criteria, data, workspace);
           jTPS.addTransaction(transaction1);
            }
      
            markWorkAsEdited();
        
    }
    
    public void handleAddRec() {
        System.out.println("hi");
        // WE'LL NEED THE WORKSPACE TO RETRIEVE THE USER INPUT VALUES
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TextField instructorField = workspace.getInstructor();
        TextField locationField= workspace.getLocation();
        TextField daytimeField= workspace.getTime();
        TextField sectionField= workspace.getSection();
        ComboBox ta1= workspace.getSuperTA1();
        ComboBox ta2= workspace.getSuperTA2();
        String instructor = instructorField.getText();
        String location= locationField.getText();
        String time= daytimeField.getText();
        String section=sectionField.getText();
        String teach1="";
        String teach2="";
        

        // WE'LL NEED TO ASK THE DATA SOME QUESTIONS TOO
        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        // DID THE USER NEGLECT TO PROVIDE A TA NAME?
        if (instructor.isEmpty() || location.isEmpty() || time.isEmpty() || section.isEmpty()) {
            System.out.println("hi");
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_INFO_TITLE), props.getProperty(MISSING_INFO_MESS));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
         // EVERYTHING IS FINE, ADD A NEW TA
        try{
        teach1= ta1.getValue().toString();
        teach2= ta2.getValue().toString();
        String officialTeach1="";
        String officialTeach2="";
        ObservableList<TeachingAssistant> k=data.getTeachingAssistants();
        for (TeachingAssistant ta : k) {
            if (ta.getName().equals(teach1)) {
                officialTeach1=ta.getName();
            }
            if(ta.getName().equals(teach2)){
                officialTeach2=ta.getName();
            }
            
        }
        if(officialTeach1.isEmpty() || officialTeach2.isEmpty()){
             AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TEACH_ERR), props.getProperty(TEACH_MESS));
        }
        else{
            data.addRecitation(section, instructor, time, location, teach1, teach2);
            jTPS_Transaction transaction1 = new AddRec_Trans(section, instructor, time, location, teach1, teach2, data);

            jTPS.addTransaction(transaction1);
            //jTPS.doTransaction();
            // CLEAR THE TEXT FIELDS
            instructorField.setText("");
            locationField.setText("");
            daytimeField.setText("");
            sectionField.setText("");
            ta1.getSelectionModel().select("TA1");
            ta2.getSelectionModel().select("TA2");
           
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
            
        }
        }
        catch(Exception e){
            if(teach1.equals(null)){
                teach1="";
            }
            if(teach2.equals(null)){
                teach2="";
            }
         String officialTeach1="";
        String officialTeach2="";   
        ObservableList<TeachingAssistant> k=data.getTeachingAssistants();
        for (TeachingAssistant ta : k) {
            if (ta.getName().equals(teach1)) {
                officialTeach1=ta.getName();
            }
            if(ta.getName().equals(teach2)){
                officialTeach2=ta.getName();
            }
            
        }
        if(officialTeach1.isEmpty() || officialTeach2.isEmpty()){
             AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TEACH_ERR), props.getProperty(TEACH_MESS));
        }
        else{
            data.addRecitation(section, instructor, time, location, teach1, teach2);
            jTPS_Transaction transaction1 = new AddRec_Trans(section, instructor, time, location, teach1, teach2, data);

            jTPS.addTransaction(transaction1);
            //jTPS.doTransaction();
            // CLEAR THE TEXT FIELDS
            instructorField.setText("");
            locationField.setText("");
            daytimeField.setText("");
            sectionField.setText("");
            ta1.getSelectionModel().select("TA1");
            ta2.getSelectionModel().select("TA2");
           
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
            
        }
    }
        
        
        }
    


    /**
     * This function provides a response for when the user presses a keyboard
     * key. Note that we're only responding to Delete, to remove a TA.
     *
     * @param code The keyboard code pressed.
     */
    public void handleKeyPress(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?

        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView taTable = workspace.getTATable();

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                TeachingAssistant ta = (TeachingAssistant) selectedItem;
                String taName = ta.getName();
                TAData data = (TAData) app.getDataComponent();
                HashMap<String, StringProperty> officeHours = data.getOfficeHours();
                jTPS_Transaction transaction1 = new DeleteTA_Transaction(ta, data, officeHours);
                jTPS.addTransaction(transaction1);

                /*data.removeTA(taName);

                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
               
                for (Label label : labels.values()) {
                    if (label.getText().equals(taName)
                            || (label.getText().contains(taName + "\n"))
                            || (label.getText().contains("\n" + taName))) {
                        data.removeTAFromCell(label.textProperty(), taName);
                    }
                } */
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
            }
        }

    }
    
     public void handleKeyPressForRec(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?

        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView recTable = workspace.getRecTable();

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = recTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                RecitationData rec = (RecitationData) selectedItem;
                String section=rec.getSection();
                String location=rec.getLocation();
                String instructor=rec.getInstructor();
                String time=rec.getDayAndTime();
                String ta1=rec.getTA1();
                String ta2=rec.getTA2();
                TAData data = (TAData) app.getDataComponent();
                jTPS_Transaction transaction1 = new DeleteRec_Trans(section, instructor, time, location, ta1, ta2, data, workspace);
                jTPS.addTransaction(transaction1);
                /*data.removeTA(taName);

                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
               
                for (Label label : labels.values()) {
                    if (label.getText().equals(taName)
                            || (label.getText().contains(taName + "\n"))
                            || (label.getText().contains("\n" + taName))) {
                        data.removeTAFromCell(label.textProperty(), taName);
                    }
                } */
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
            }
        }

    }
     
     public void handleKeyPressForTeams(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?

        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView teamTable = workspace.getProjTable();
            TAData data = (TAData) app.getDataComponent();

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = teamTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
            ProjectData proj = (ProjectData) selectedItem;
            String colorHex= proj.getColorHex();
            String txtColor=proj.getTxtColor();
            String teamName=proj.getName();
            String link=proj.getLink();

        //data.removeSched(type, date, title, topic, link, time, criteria);
            jTPS_Transaction transaction2 = new RemoveTeam_Trans(teamName, colorHex, txtColor, link, data, workspace);
            jTPS.addTransaction(transaction2);
               
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
            }
        }

    }
     
     public void handleExportRequest() {
	// WE'LL NEED THIS TO GET CUSTOM STUFF
	PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAData data = (TAData) app.getDataComponent();
        try {
	    	// PROMPT THE USER FOR A FILE NAME
                
                File fileName=new File("../TAManagerTester/public_html");
                copy(fileName, data.getExportDir2());
                app.getFileComponent().exportData(app.getDataComponent(), data.getExportDir2().toString());
                //File newDestination=new File(selectedFile.getAbsoluteFile()+"/js/OfficeHoursGridData.json"); 
                //copyFile(currentWorkFile,newDestination);
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
              dialog.show(props.getProperty(EXPORT_COMPLETED_TITLE), props.getProperty(EXPORT_COMPLETED_MESSAGE));
                
                
                
        } catch (IOException ioe) {
	    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
	    dialog.show(props.getProperty(LOAD_ERROR_TITLE), props.getProperty(LOAD_ERROR_MESSAGE));
        }
    }
     
     public void handleKeyPressForStudents(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?

        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView studentsTable = workspace.getProjStudentTable();
            TAData data = (TAData) app.getDataComponent();

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = studentsTable.getSelectionModel().getSelectedItem();
            ProjectStudentData proj = (ProjectStudentData) selectedItem;
            String firstName=proj.getFirstName();
            String lastName=proj.getLastName();
            String role=proj.getRole();
            String team= proj.getTeam();

        //data.removeSched(type, date, title, topic, link, time, criteria);
            jTPS_Transaction transaction2 = new RemoveProjStudentData_Trans(firstName, lastName, team, role, data, workspace);
            jTPS.addTransaction(transaction2);
               
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
            }
        }

    
     
     public void handleKeyPressForSched(KeyCode code) {
        // DID THE USER PRESS THE DELETE KEY?

        if (code == KeyCode.DELETE) {
            // GET THE TABLE
            TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView schedTable = workspace.getSchedTable();

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = schedTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                ScheduleData sched = (ScheduleData) selectedItem;
        String type=sched.getType();
        String date=sched.getDate();
        String title=sched.getTitle();
        String topic=sched.getTopic();
        String link=sched.getLink();
        String time=sched.getTime();
        String criteria=sched.getCriteria();
        System.out.println(type);
        TAData data = (TAData) app.getDataComponent();
        //data.removeSched(type, date, title, topic, link, time, criteria);
        jTPS_Transaction transaction1 = new DeleteSched_Trans(type, date, title, topic, link, time, criteria, data, workspace);
        jTPS.addTransaction(transaction1);
                /*data.removeTA(taName);

                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
               
                for (Label label : labels.values()) {
                    if (label.getText().equals(taName)
                            || (label.getText().contains(taName + "\n"))
                            || (label.getText().contains("\n" + taName))) {
                        data.removeTAFromCell(label.textProperty(), taName);
                    }
                } */
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
            }
        }

    }
    
    
    
    public void handleRemoveTA(){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
            TableView taTable = workspace.getTATable();

            // IS A TA SELECTED IN THE TABLE?
            Object selectedItem = taTable.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                TeachingAssistant ta = (TeachingAssistant) selectedItem;
                String taName = ta.getName();
                TAData data = (TAData) app.getDataComponent();
                HashMap<String, StringProperty> officeHours = data.getOfficeHours();
                jTPS_Transaction transaction1 = new DeleteTA_Transaction(ta, data, officeHours);
                jTPS.addTransaction(transaction1);

                /*data.removeTA(taName);

                // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
               
                for (Label label : labels.values()) {
                    if (label.getText().equals(taName)
                            || (label.getText().contains(taName + "\n"))
                            || (label.getText().contains("\n" + taName))) {
                        data.removeTAFromCell(label.textProperty(), taName);
                    }
                } */
                // WE'VE CHANGED STUFF
                markWorkAsEdited();
            }

        
    }
    
    public void handleEndDateChange(LocalDate startDate){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TAData data = (TAData) app.getDataComponent();
        String oldDate=data.getEndDate();
        jTPS_Transaction transaction1 = new addNewStartDate(startDate, oldDate, data, workspace);
        jTPS.addTransaction(transaction1);
        markWorkAsEdited();
            
        
    }
    
    public void handleStartDate(LocalDate startDate){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TAData data = (TAData) app.getDataComponent();
        String oldDate=data.getStartDate();
        jTPS_Transaction transaction1 = new addEndDate(startDate, oldDate, data, workspace);
        jTPS.addTransaction(transaction1);
        markWorkAsEdited();
            
        
    }
    
    public void handleRemoveRec(){
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView recTable = workspace.getRecTable();
        Object selectedItem = recTable.getSelectionModel().getSelectedItem();
        RecitationData rec = (RecitationData) selectedItem;
        String section=rec.getSection();
        String location=rec.getLocation();
        String instructor=rec.getInstructor();
        String time=rec.getDayAndTime();
        String ta1=rec.getTA1();
        String ta2=rec.getTA2();

   

        TAData data = (TAData) app.getDataComponent();
        //data.removeRec(section, instructor, time, location, ta1, ta2);
        jTPS_Transaction transaction1 = new DeleteRec_Trans(section, instructor, time, location, ta1, ta2, data, workspace);
        jTPS.addTransaction(transaction1);
        /*
        workspace.getSection().clear();
        workspace.getInstructor().clear();
        workspace.getLocation().clear();
        workspace.getTime().clear();
        workspace.superTACombo1.setPromptText("TA1");
        workspace.superTACombo2.setPromptText("TA2");
        */
        
    }

    public void handleUndoTransaction() {
        System.out.println("Transaction Control Z");
        jTPS.undoTransaction();
        markWorkAsEdited();
    }

    public void handleReDoTransaction() {
        System.out.println("Transaction crlt y");
        jTPS.doTransaction();
        markWorkAsEdited();
    }

    /**
     * This function provides a response for when the user clicks on the office
     * hours grid to add or remove a TA to a time slot.
     *
     * @param pane The pane that was toggled.
     */
    public void handleCellToggle(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            // GET THE TA
            jTPS_Transaction transaction = new ToggleTa_Transaction(selectedItem, app, pane);
            jTPS.addTransaction(transaction);

            /*TeachingAssistant ta = (TeachingAssistant) selectedItem;
            String taName = ta.getName();
            TAData data = (TAData) app.getDataComponent();
            String cellKey = pane.getId();

            // AND TOGGLE THE OFFICE HOURS IN THE CLICKED CELL
            data.toggleTAOfficeHours(cellKey, taName);*/
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
        }
    }
    
    public void handleSchedClicked(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView schedTable = workspace.getSchedTable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = schedTable.getSelectionModel().getSelectedItem();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        if (selectedItem != null) {
            //selectedItem = taTable.getSelectionModel().getSelectedItem();
            ScheduleData sched= (ScheduleData) selectedItem;
            String type=sched.getType();
            String topic=sched.getTopic();
            String criteria=sched.getCriteria();
            String date=sched.getDate();
            String link=sched.getLink();
            String title=sched.getTitle();
            String time=sched.getTime();
            DatePicker dateBox=workspace.getDateBox();
            System.out.println(date);
            System.out.println(type);
            System.out.println(topic);
            TAData data = (TAData) app.getDataComponent();
            String [] dates;
            if(date.contains("/")){
            dates=date.split("/");
            }
            else{
            dates=date.split("-");  
            }
            // SET TextField To TA NAME 
            workspace.getTopicSchedTextField().setText(topic);
            workspace.getCriteriaSchedTextField().setText(criteria);
            workspace.getLinkSchedTextField().setText(link);
            workspace.getTitleSchedTextField().setText(title);
            workspace.getTimeSchedTextField().setText(time);
            workspace.getHolidayPicker().getSelectionModel().select(type);
            final DatePicker datePicker= new DatePicker(LocalDate.now());
            dateBox.setOnAction(new EventHandler(){
            public void handle(Event t){
                LocalDate thisDate=dateBox.getValue();
            }
        });
        try{
            if(dates[0].length()==1){
                 dates[0]='0'+dates[0];
             }
             if(dates[1].length()==1){
                 dates[1]='0'+dates[1];
             }
        dateBox.setValue(LocalDate.of(Integer.parseInt(dates[2]), Integer.parseInt(dates[0]), Integer.parseInt(dates[1]))); 
        }
        catch(Exception e){
            if(dates[0].length()==1){
                 dates[0]='0'+dates[0];
             }
             if(dates[1].length()==1){
                 dates[1]='0'+dates[1];
             }
        dateBox.setValue(LocalDate.of(Integer.parseInt(dates[2]), Integer.parseInt(dates[0]), Integer.parseInt(dates[1])));    
        }
           
           // workspace.dateBox.setValue(LocalDate.of(2017, Integer.parseInt(dates[1]), Integer.parseInt(dates[0])));

            // });
            //markWorkAsEdited();   
        }
    }

    public void handleTaClicked(Pane pane, Pane addBox) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        if (selectedItem != null) {
            //selectedItem = taTable.getSelectionModel().getSelectedItem();
            TeachingAssistant ta = (TeachingAssistant) selectedItem;
            // workspace.nameTextField.clear();
            //workspace.emailTextField.clear(); 
            System.out.println("TA CLICKED");
            System.out.println(ta.getName());
            // addBox.getChildren().remove(workspace.addButton); 
            // addBox.getChildren().remove(workspace.clearButton1); 
            addBox.getChildren().add(workspace.nameTextField);
            addBox.getChildren().add(workspace.emailTextField);
            addBox.getChildren().add(workspace.updateTaButton);
            addBox.getChildren().add(workspace.clearButton);
            // GET THE TA
            String taName = ta.getName();
            String taEmail = ta.getEmail();
            TAData data = (TAData) app.getDataComponent();

            // SET TextField To TA NAME 
            workspace.nameTextField.setText(taName);
            workspace.emailTextField.setText(taEmail);
            
            //workspace.addUpdate.setText(props.getProperty(TAManagerProp.UPDATE_REC_BUTTON_TEXT.toString()));
            // workspace.updateTaButton.setOnAction(e -> {
            //handleUpdateTA(taName,taEmail,ta);

            // });
            //markWorkAsEdited();   
        }
    }
    
     public void handleProjClicked() {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView projTable = workspace.getProjTable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = projTable.getSelectionModel().getSelectedItem();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        if (selectedItem != null) {
            //selectedItem = taTable.getSelectionModel().getSelectedItem();
            ProjectData proj = (ProjectData) selectedItem;
            
            String hexColor = '#'+proj.getColorHex();
            String txtColor='#'+proj.getTxtColor();
            String name=proj.getName();
            String link=proj.getLink();
            TAData data = (TAData) app.getDataComponent();

            // SET TextField To TA NAME 
            workspace.getProjNameTextField().setText(name);
            workspace.getProjLinkTextField().setText(link);
            Color color1=Color.web(hexColor);
            Color color2=Color.web(txtColor);
            workspace.getColorPicker1().setValue(color1);
            workspace.getColorPicker2().setValue(color2);

            // });
            //markWorkAsEdited();   
        }
    }
     
     public void handleStudentProjClicked() {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView projStudentTable = workspace.getProjStudentTable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = projStudentTable.getSelectionModel().getSelectedItem();
        PropertiesManager props = PropertiesManager.getPropertiesManager();

        if (selectedItem != null) {
            //selectedItem = taTable.getSelectionModel().getSelectedItem();
            ProjectStudentData proj = (ProjectStudentData) selectedItem;
            
            String firstName = proj.getFirstName();
            String lastName=proj.getLastName();
            String role=proj.getRole();
            String team=proj.getTeam();
            TAData data = (TAData) app.getDataComponent();

            // SET TextField To TA NAME 
            workspace.getStudentProjFirstNameText().setText(firstName);
            workspace.getStudentProjLastNameText().setText(lastName);
            workspace.getStudentProjRoleText().setText(role);
            workspace.getStudentProjTeamText().getSelectionModel().select(team);

            // });
            //markWorkAsEdited();   
        }
    }
    
    public void handleRecitationClicked(Pane pane) {
        // GET THE TABLE
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView recTable = workspace.getRecTable();

        // IS A TA SELECTED IN THE TABLE?
        Object selectedItem = recTable.getSelectionModel().getSelectedItem();

        if (selectedItem != null) {
            //selectedItem = taTable.getSelectionModel().getSelectedItem();
            RecitationData rec= (RecitationData) selectedItem;
            // workspace.nameTextField.clear();
            //workspace.emailTextField.clear(); 
            System.out.println("REC CLICKED");
            System.out.println(rec.getDayAndTime());
           
            // GET THE REC DETAILS
            String instructor = rec.getInstructor();
            String time = rec.getDayAndTime();
            String location=rec.getLocation();
            String section=rec.getSection();
            String TA1= rec.getTA1();
            String TA2= rec.getTA2();
            TAData data = (TAData) app.getDataComponent();
            
            // SET TextField To TA NAME 
            workspace.sectionField.setText(section);
            workspace.instructorField.setText(instructor);
            workspace.daytimeField.setText(time);
            workspace.locationField.setText(location);
            workspace.superTACombo1.getSelectionModel().select(TA1);
            workspace.superTACombo2.getSelectionModel().select(TA2);
            // workspace.updateTaButton.setOnAction(e -> {
            //handleUpdateTA(taName,taEmail,ta);

            // });
            markWorkAsEdited();   
        }
    }
    
    public void handleUpdateREC() {
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView recTable = workspace.getRecTable();
        Object selectedItem = recTable.getSelectionModel().getSelectedItem();
        RecitationData rec = (RecitationData) selectedItem;
        String section=rec.getSection();
        String location=rec.getLocation();
        String instructor=rec.getInstructor();
        String time=rec.getDayAndTime();
        String ta1=rec.getTA1();
        String ta2=rec.getTA2();

        TextField instructorField = workspace.getInstructor();
        TextField locationField=workspace.getLocation();
        TextField sectionField=workspace.getSection();
        TextField timeField=workspace.getTime();
        ComboBox teach1=workspace.getSuperTA1();
        ComboBox teach2=workspace.getSuperTA2();

        String instr = instructorField.getText();
        String loc = locationField.getText();
        String sec=sectionField.getText();
        String daytime=timeField.getText();
        String teacher1="";
        String teacher2="";
   

        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
         if (instructor.isEmpty() || location.isEmpty() || time.isEmpty() || section.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_INFO_TITLE), props.getProperty(MISSING_INFO_MESS));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
         // EVERYTHING IS FINE, ADD A NEW TA
         try{
           teacher1=teach1.getValue().toString();
           teacher2=teach2.getValue().toString();
           
            String officialTeach1="";
        String officialTeach2="";
        ObservableList<TeachingAssistant> k=data.getTeachingAssistants();
        for (TeachingAssistant ta : k) {
            if (ta.getName().equals(teacher1)) {
                officialTeach1=ta.getName();
            }
            if(ta.getName().equals(teacher2)){
                officialTeach2=ta.getName();
            }
            
        }
        if(officialTeach1.isEmpty() || officialTeach2.isEmpty()){
             AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TEACH_ERR), props.getProperty(TEACH_MESS));
        }
        else{
             jTPS_Transaction transaction2 = new UpdateRec_Trans(sec, section, instr, instructor, daytime, time, loc, location,
                        teacher1, ta1, teacher2, ta2, data, workspace);
                jTPS.addTransaction(transaction2);
                recTable.refresh();
                sectionField.setText(sec);
                instructorField.setText(instr);
                locationField.setText(loc);
                timeField.setText(daytime);
                teach1.getSelectionModel().select(teacher1);
                teach2.getSelectionModel().select(teacher2);
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
            
        }
         }
         catch(Exception e){
             if(teacher1.equals(null)){
                teacher1="";
            }
            if(teacher2.equals(null)){
                teacher2="";
            }
         String officialTeach1="";
        String officialTeach2="";   
        ObservableList<TeachingAssistant> k=data.getTeachingAssistants();
        for (TeachingAssistant ta : k) {
            if (ta.getName().equals(teacher1)) {
                officialTeach1=ta.getName();
            }
            if(ta.getName().equals(teacher2)){
                officialTeach2=ta.getName();
            }
            
        }
        if(officialTeach1.isEmpty() || officialTeach2.isEmpty()){
             AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(TEACH_ERR), props.getProperty(TEACH_MESS));
        }
             
         else{
             jTPS_Transaction transaction2 = new UpdateRec_Trans(sec, section, instr, instructor, daytime, time, loc, location,
                        teacher1, ta1, teacher2, ta2, data, workspace);
                jTPS.addTransaction(transaction2);
                recTable.refresh();
                sectionField.setText(sec);
                instructorField.setText(instr);
                locationField.setText(loc);
                timeField.setText(daytime);
                teach1.getSelectionModel().select(teacher1);
                teach2.getSelectionModel().select(teacher2);
            // WE'VE CHANGED STUFF
            markWorkAsEdited();
            
        }

        }
        // workspace.reloadOfficeHoursGrid(data);

    }
    
    public void handleUpdateSched() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAData data = (TAData) app.getDataComponent();
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView schedTable = workspace.getSchedTable();
        Object selectedItem = schedTable.getSelectionModel().getSelectedItem();
        ScheduleData sched=(ScheduleData)selectedItem;
        String type=sched.getType();
        String topic=sched.getTopic();
        String criteria=sched.getCriteria();
        String date=sched.getDate();
        String link=sched.getLink();
        String title=sched.getTitle();
        String time=sched.getTime();
        System.out.println(type+ " " + topic + " " + criteria + " " + date+ " " + link+" "+title+ " "+time+ "****");
        
        TextField titleText=workspace.getTitleSchedTextField();
        TextField topicText=workspace.getTopicSchedTextField();
        TextField criteriaText=workspace.getCriteriaSchedTextField();
        TextField linkText=workspace.getLinkSchedTextField();
        TextField timeText=workspace.getTimeSchedTextField();
        DatePicker dateBox=workspace.getDateBox();
        ComboBox typeBox=workspace.getHolidayPicker();
        
        String updateTitle=titleText.getText();
        String updateTopic=topicText.getText();
        String updateCriteria=criteriaText.getText();
        String updateLink=linkText.getText();
        String updateTime=timeText.getText();
        String updateDate=dateBox.getValue().toString();
        String updateType=typeBox.getValue().toString();
        
        if(updateTitle.isEmpty() || updateTitle.equals(null)){
            updateTitle=" ";
        }
        if(updateTopic.isEmpty() || updateTopic.equals(null)){
            updateTopic=" ";
        }
        
        if(updateTitle.isEmpty() || updateTitle.equals(null)){
            updateTitle=" ";
        }
        if(updateCriteria.isEmpty() || updateCriteria.equals(null)){
            updateCriteria=" ";
        }
        if(updateLink.isEmpty() || updateLink.equals(null)){
            updateLink=" ";
        }
        if(updateTime.isEmpty() || updateTime.equals(null)){
            updateTime=" ";
        }
        if(updateType.isEmpty() || updateType.equals(null) ||updateType.equals("Type")){
            updateType=" ";
        }
        String [] comps=updateDate.split("-");
        String year=comps[0];
        String month=comps[1];
        String day=comps[2];
        if(comps[2].charAt(0)=='0'){
            System.out.println("gothere");
                 day=day.substring(1);
             }
             if(comps[1].charAt(0)=='0'){
                 month=month.substring(1);
             }
        String updatedDate=month+"/"+day+"/"+year;
        
        System.out.println(updateType +  " " + type + " " + updateCriteria + " " + criteria+ " " + updateDate+ " " + date);
         jTPS_Transaction transaction2 = new AddUpdateSched_Trans(updateType, type, updateCriteria, criteria, 
                       updatedDate, date, updateLink, link, updateTime, time, updateTitle, title, updateTopic, topic, data, workspace);
         jTPS.addTransaction(transaction2);
        
        schedTable.refresh();
        
        /*
        titleText.setText(updateTitle);
        topicText.setText(updateTopic);
        criteriaText.setText(updateCriteria);
        linkText.setText(updateLink);
        timeText.setText(updateTime);
        String [] dates=updateDate.split("-");
        final DatePicker datePicker= new DatePicker(LocalDate.now());
            dateBox.setOnAction(new EventHandler(){
            public void handle(Event t){
                LocalDate thisDate=dateBox.getValue();
            }
        });
        System.out.println(updateDate);
        dateBox.setValue(LocalDate.of(Integer.parseInt(dates[0]), Integer.parseInt(dates[1]), Integer.parseInt(dates[2])));
        typeBox.getSelectionModel().select(updateType);
        */


        //jTPS_Transaction transaction2 = new AddUpdateSched_Trans(updateType, type, updateCriteria, criteria, 
            //           updateDate, date, updateLink, link, updateTime, time, updateTitle, title, updateTopic, topic, data, workspace);
                //data.removeRec(section, instructor, time, location, ta1, ta2);
                //data.addRecitation(sec, instr, daytime, loc, teacher1, teacher2);
                
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        
                
               
               
               

                //jTPS.doTransaction();
                /*data.getTA(orgName).setName(name);
                handleUpdateTaGrid(orgName, name);
                ta.setName(name);                        // MOVED TO TRANSACTION CASE 
                taTable.refresh();
                 */
                markWorkAsEdited();
            
            //taTable.refresh();
            //nameTextField.setText(name);
            //emailTextField.setText(email);

            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            //nameTextField.requestFocus();
            // WE'VE CHANGED STUFF

        
        // workspace.reloadOfficeHoursGrid(data);

    }


    public void handleUpdateTA() {
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        Object selectedItem = taTable.getSelectionModel().getSelectedItem();
        TeachingAssistant ta = (TeachingAssistant) selectedItem;
        String orgName = ta.getName();
        String orgEmail = ta.getEmail();
        //boolean orgCheck= ta.getOldCheckValue().getValue();
        boolean check=ta.getUndergrad().getValue();

        TextField nameTextField = workspace.getNameTextField();
        TextField emailTextField = workspace.getEmailTextField();

        String name = nameTextField.getText();
        String email = emailTextField.getText();
        EmailValidator checkEmail = new EmailValidator();

        TAData data = (TAData) app.getDataComponent();

        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (name.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_NAME_TITLE), props.getProperty(MISSING_TA_NAME_MESSAGE));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        else if (email.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_TA_EMAIL_TITLE), props.getProperty(MISSING_TA_EMAIL_MESSAGE));
        } // **********Check the TA Email Address for correct format 
        else if (!checkEmail.validate(email)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));

        } // EVERYTHING IS FINE, ADD A NEW TA
        else {
            // ADD THE NEW TA TO THE DATA
            if (!orgName.equalsIgnoreCase(name)) { //case if only name is changed
                jTPS_Transaction transaction2 = new UpdateTA_Transaction(orgName, name, orgEmail, email, data, app, workspace);
                nameTextField.setText(name);
                emailTextField.setText(email);
                jTPS.addTransaction(transaction2);

                //jTPS.doTransaction();
                /*data.getTA(orgName).setName(name);
                handleUpdateTaGrid(orgName, name);
                ta.setName(name);                        // MOVED TO TRANSACTION CASE 
                taTable.refresh();
                 */
                markWorkAsEdited();
            }
            if (!orgEmail.equalsIgnoreCase(email)) {   //case if only email is changed 
                jTPS_Transaction transaction3 = new UpdateTA_EmailOnly_Transaction(orgName, orgEmail, email, data, workspace);
                jTPS.addTransaction(transaction3);

                // data.getTA(orgName).setEmail(email);     //moved to transaction class 
                //ta.setEmail(email);
                markWorkAsEdited();

            }
            if (orgEmail.equalsIgnoreCase(email) && orgName.equalsIgnoreCase(name)) {                //case if nothing is chagned 
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(NO_UPDATE_TITLE), props.getProperty(NO_UPDATE_MESSAGE));

            }
            taTable.refresh();
            nameTextField.setText(name);
            emailTextField.setText(email);

            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            nameTextField.requestFocus();
            // WE'VE CHANGED STUFF

        }
        // workspace.reloadOfficeHoursGrid(data);

    }

    public void handleUpdateProj() {
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView projTable = workspace.getProjTable();
        Object selectedItem = projTable.getSelectionModel().getSelectedItem();
        ProjectData proj = (ProjectData) selectedItem;
        String name=proj.getName();
        String link=proj.getLink();
        String hexColor=proj.getColorHex();
        String txtColor=proj.getTxtColor();
        //boolean orgCheck= ta.getOldCheckValue().getValue();
        
        TextField nameTextField = workspace.getProjNameTextField();
        TextField linkTextField = workspace.getProjLinkTextField();
        ColorPicker picker1=workspace.getColorPicker1();
        ColorPicker picker2=workspace.getColorPicker2();

        String newName = nameTextField.getText();
        String newLink = linkTextField.getText();
        String newHex = Integer.toHexString(picker1.getValue().hashCode()).substring(0, 6).toUpperCase();
        String newTextColor=Integer.toHexString(picker2.getValue().hashCode()).substring(0, 6).toUpperCase();
        
        TAData data = (TAData) app.getDataComponent();
        System.out.println(newName);
        System.out.println(newLink);
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        if (newName.equals(selectedItem) || newLink.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_INFO_TITLE2), props.getProperty(MISSING_INFO_MESS2));
        } // DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        
        else {
            // ADD THE NEW TA TO THE DATA
           
            
                jTPS_Transaction transaction3 = new UpdateProj_Trans(newName, name, newHex, hexColor, newTextColor, txtColor, newLink, link, data, workspace);
                jTPS.addTransaction(transaction3);

            projTable.refresh();
            

            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            //nameTextField.requestFocus();
            // WE'VE CHANGED STUFF

        }
        // workspace.reloadOfficeHoursGrid(data);

    }
    
    public void handleUpdateProjStudentData() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView projTable = workspace.getProjStudentTable();
        Object selectedItem = projTable.getSelectionModel().getSelectedItem();
        ProjectStudentData proj = (ProjectStudentData) selectedItem;
        String firstName=proj.getFirstName();
        String lastName=proj.getLastName();
        String role=proj.getRole();
        String team=proj.getTeam();
        //boolean orgCheck= ta.getOldCheckValue().getValue();
        
        TextField firstNameTextField = workspace.getStudentProjFirstNameText();
        TextField lastNameTextField = workspace.getStudentProjLastNameText();
        TextField roleTextField=workspace.getStudentProjRoleText();
        ComboBox teamTextField=workspace.getStudentProjTeamText();

        String newFirstName = firstNameTextField.getText();
        String newLastName = lastNameTextField.getText();
        String newRole = roleTextField.getText();
        String newTeam;
        try{
            newTeam=teamTextField.getValue().toString();
        
        TAData data = (TAData) app.getDataComponent();
        
        // WE'LL NEED THIS IN CASE WE NEED TO DISPLAY ANY ERROR MESSAGES
        
        if (newFirstName.equals(firstName) && newLastName.equals(lastName)) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(UNIQUE_NAME_TITLE), props.getProperty(UNIQUE_NAME_MESS));
        }
         if (!(newRole.equalsIgnoreCase("Lead Designer") || newRole.equalsIgnoreCase("Lead Programmer") || newRole.equalsIgnoreCase("Data Designer") || newRole.equalsIgnoreCase("Project Manager"))) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(UNIQUE_NAME_TITLE2), props.getProperty(UNIQUE_NAME_MESS2));
        }
        if (newFirstName.isEmpty() || newLastName.isEmpty()) {
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_INFO_TITLE3), props.getProperty(MISSING_INFO_MESSAGE3));
        }// DID THE USER NEGLECT TO PROVIDE A TA EMAIL?
        
        else {
            // ADD THE NEW TA TO THE DATA
            jTPS_Transaction transaction3 = new UpdateStudentProj_trans(firstName, newFirstName, lastName, newLastName, team, newTeam, role, newRole, data, workspace);
            jTPS.addTransaction(transaction3);

            projTable.refresh();
            

            // AND SEND THE CARET BACK TO THE NAME TEXT FIELD FOR EASY DATA ENTRY
            //nameTextField.requestFocus();
            // WE'VE CHANGED STUFF

        }
        }
        catch(Exception e){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(MISSING_INFO_TITLE3), props.getProperty(MISSING_INFO_MESSAGE3));
            
        }
        // workspace.reloadOfficeHoursGrid(data);

    }
    
    public void handleTaTableRefresh() {
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TableView taTable = workspace.getTATable();
        taTable.refresh();
    }

    public void handleUpdateTaGrid(String taName, String newName) {

        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        TAData data = (TAData) app.getDataComponent();
        //data.removeTA(taName);

        // AND BE SURE TO REMOVE ALL THE TA'S OFFICE HOURS
        HashMap<String, Label> labels = workspace.getOfficeHoursGridTACellLabels();

        for (Label label : labels.values()) {   //iterates thourhg the hashmap to find all occurences of orgTA in the office hour grid
            if (label.getText().equals(taName)
                    || (label.getText().contains(taName + "\n"))
                    || (label.getText().contains("\n" + taName))) {
                data.renameTaCell(label.textProperty(), taName, newName);
            }
        }
        TableView taTable = workspace.getTATable();
        Collections.sort(data.getTeachingAssistants());  //sorts the teachingAssistants List 
        taTable.refresh();

        markWorkAsEdited();

    }

    void handleGridCellMouseExited(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData) app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();

        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().remove(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
            cell.getStyleClass().add(CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        }
    }

    void handleGridCellMouseEntered(Pane pane) {
        String cellKey = pane.getId();
        TAData data = (TAData) app.getDataComponent();
        int column = Integer.parseInt(cellKey.substring(0, cellKey.indexOf("_")));
        int row = Integer.parseInt(cellKey.substring(cellKey.indexOf("_") + 1));
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();

        // THE MOUSED OVER PANE
        Pane mousedOverPane = workspace.getTACellPane(data.getCellKey(column, row));
        mousedOverPane.getStyleClass().clear();
        mousedOverPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_CELL);

        // THE MOUSED OVER COLUMN HEADER
        Pane headerPane = workspace.getOfficeHoursGridDayHeaderPanes().get(data.getCellKey(column, 0));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // THE MOUSED OVER ROW HEADERS
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(0, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        headerPane = workspace.getOfficeHoursGridTimeCellPanes().get(data.getCellKey(1, row));
        headerPane.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);

        // AND NOW UPDATE ALL THE CELLS IN THE SAME ROW TO THE LEFT
        for (int i = 2; i < column; i++) {
            cellKey = data.getCellKey(i, row);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }

        // AND THE CELLS IN THE SAME COLUMN ABOVE
        for (int i = 1; i < row; i++) {
            cellKey = data.getCellKey(column, i);
            Pane cell = workspace.getTACellPane(cellKey);
            cell.getStyleClass().add(CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN);
        }
    }
    
    private <T> TableColumn<T, ?> getTableColumnByName(TableView<T> tableView, String name) {
    for (TableColumn<T, ?> col : tableView.getColumns())
        if (col.getText().equals(name)) return col ;
    return null ;
    }
   
    /*
    public TableRow call(TableView tableView) {
        return new TableRow() {
            protected void updateItem(ScheduleData item, boolean empty) {
                super.updateItem(item, empty);
                
                        setDisable(true);
                    
                }
        };
    }

*/
    void handleStartDateChange(LocalDate newValue){
        TAData data = (TAData) app.getDataComponent();
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String monthSetter="";
        String daySetter="";
        AppYesNoDialogSingleton yesNoDialog = AppYesNoDialogSingleton.getSingleton();
        yesNoDialog.show(props.getProperty(UPDATE_DATE_TITLE), props.getProperty(UPDATE_DATE_MESSAGE));
        String selection = yesNoDialog.getSelection();
        if(selection.equals(AppYesNoDialogSingleton.YES)){
        Month month=newValue.getMonth();
        DayOfWeek day=newValue.getDayOfWeek();
        monthSetter=month.toString();
        daySetter=day.toString();
        data.setSchedStartMonthDate(monthSetter);
        data.setSchedStartDayDate(daySetter);
        if(day.equals(DayOfWeek.MONDAY)==false){
               AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
               dialog.show(props.getProperty(INVALID_START), props.getProperty(INVALID_START_MESS));
            }
        }
            else{
           
                data.setSchedStartMonthDate(monthSetter);
                data.setSchedStartDayDate(daySetter);
            
           
    }
    }
    
    void handleChangeDate(String startDate, String endDate, String day1, String day2){
        System.out.println(day1 + " " + day2);
        TAWorkspace workspace = (TAWorkspace) app.getWorkspaceComponent();
        ObservableList<ScheduleData> schedList2 = FXCollections.observableArrayList();
        ArrayList<ScheduleData> rejects= new ArrayList<ScheduleData>();
        TAData data = (TAData) app.getDataComponent();
        ObservableList<ScheduleData> schedList = data.getSchedData();
        String [] starters=startDate.split("-");
        String startYear=starters[0];
        String startMonth=starters[1];
        String startDay=starters[2];
        String [] enders=endDate.split("-");
        String endYear=enders[0];
        String endMonth=enders[1];
        String endDay=enders[2];
           PropertiesManager props = PropertiesManager.getPropertiesManager();
           if(day1.equals("Mon")==false){
               AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
               dialog.show(props.getProperty(INVALID_START), props.getProperty(INVALID_START_MESS));
           } 
           else if(day2.equals("Fri")==false){
               AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
               dialog.show(props.getProperty(INVALID_END), props.getProperty(INVALID_END_MESS));
           }
           
           else{
            AppYesNoDialogSingleton yesNoDialog = AppYesNoDialogSingleton.getSingleton();
            yesNoDialog.show(props.getProperty(UPDATE_DATE_TITLE), props.getProperty(UPDATE_DATE_MESSAGE));

        // AND NOW GET THE USER'S SELECTION
            String selection = yesNoDialog.getSelection();
            if (selection.equals(AppYesNoDialogSingleton.YES)) {
                   for (ScheduleData sched : schedList) {
                   String date=sched.getDate();
                   String [] dates;
                   if(date.contains("/")){
                       dates=date.split("/");
                   }
                   else{
                       dates=date.split("-");
                   }
                   if(startMonth.charAt(0)=='0'){
                       startMonth=startMonth.substring(1);
                   }
                   if(startDay.charAt(0)=='0'){
                       startDay=startDay.substring(1);
                   }
                   if(endMonth.charAt(0)=='0'){
                       endMonth=endMonth.substring(1);
                   }
                   if(endDay.charAt(0)=='0'){
                       endDay=endDay.substring(1);
                   }
                   String currMonth=dates[0];
                   String currDay=dates[1];
                   String currYear=dates[2];
                   
                   System.out.println(sched);
                   System.out.println(currMonth + " " + startMonth + " " + endMonth);
                   if(Integer.parseInt(currMonth)>Integer.parseInt(startMonth) && Integer.parseInt(currMonth)<Integer.parseInt(endMonth)){
                       schedList2.add(sched);
                   }
                   else if(Integer.parseInt(currMonth)==Integer.parseInt(startMonth) || Integer.parseInt(currMonth)==Integer.parseInt(endMonth)){
                       if(Integer.parseInt(currMonth)==Integer.parseInt(startMonth) && Integer.parseInt(currMonth)<Integer.parseInt(endMonth)){
                       if(Integer.parseInt(currDay)>=Integer.parseInt(startDay)){
                           schedList2.add(sched);
                           
                       }
                       }
                       
              
                       else if(Integer.parseInt(currMonth)>Integer.parseInt(startMonth) && Integer.parseInt(currMonth)==Integer.parseInt(endMonth)){
                       if(Integer.parseInt(currDay)<=Integer.parseInt(endDay)){
                           schedList2.add(sched);
                       }
                       }
                  
                       else if(Integer.parseInt(currMonth)==Integer.parseInt(startMonth) && Integer.parseInt(currMonth)==Integer.parseInt(endMonth)){
                       if(Integer.parseInt(currDay)>=Integer.parseInt(startDay) &&Integer.parseInt(currDay)<=Integer.parseInt(endDay)){
                           schedList2.add(sched);
                       }
                       }
                       }
                       
                   
                   else{
                       rejects.add(sched);
                       System.out.println("didn't make the cut");
                               
                       }
                   
                  
                   }
                     //workspace.getSchedTable().setItems(schedList2);
                     jTPS_Transaction transaction3 = new ChangeDate_Trans(schedList, schedList2, rejects, workspace, data);
                     jTPS.addTransaction(transaction3);
            
            }
               }
    }
                       
           
  
           
      
    
    void clearSchedData(){
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        workspace.getTitleSchedTextField().clear();
        workspace.getHolidayPicker().getSelectionModel().select("Type");
        workspace.getDateBox().setPromptText("Date");
        workspace.getTimeSchedTextField().clear();
        workspace.getTitleSchedTextField().clear();
        workspace.getTopicSchedTextField().clear();
        workspace.getLinkSchedTextField().clear();
        workspace.getCriteriaSchedTextField().clear();
        
    }

    void handleChangeTime(String startTime, String endTime) {
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        AppYesNoDialogSingleton yesNoDialog = AppYesNoDialogSingleton.getSingleton();
        yesNoDialog.show(props.getProperty(UPDATE_TIME_TITLE), props.getProperty(UPDATE_TIME_MESSAGE));

        // AND NOW GET THE USER'S SELECTION
        String selection = yesNoDialog.getSelection();
        if (selection.equals(AppYesNoDialogSingleton.YES)) {

            int start = convertToMilitaryTime(startTime);
            int end = convertToMilitaryTime(endTime);
            System.out.println(start);

            //TAWorkspace workspace = (TAWorkspace)app.getDataComponent();
            if (start == end || start == -1 || end == -1) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TIME_INPUT_TITLE), props.getProperty(INVALID_TIME_INPUT_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              

            } else if (start > end) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TIME_INPUT_TITLE), props.getProperty(INVALID_TIME_INPUT_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              

            } else if (end < start) {
                AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                dialog.show(props.getProperty(INVALID_TIME_INPUT_TITLE), props.getProperty(INVALID_TIME_INPUT_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              

            } else {    //At this point the time varialbes are good to go. 
                TAData data = (TAData) app.getDataComponent();

                jTPS_Transaction transaction = new updateTime_Transaction(start, end, data);
                jTPS.addTransaction(transaction);

                //workspace.resetWorkspace(); 
                //workspace.reloadWorkspace(oldData);
                markWorkAsEdited();
                //workspace.reloadOfficeHoursGrid(data);
            }
        }

    }

    public int convertToMilitaryTime(String time) {
        int milTime = 0;
        if (time == null) {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show(props.getProperty(INVALID_TA_EMAIL_TITLE), props.getProperty(INVALID_TA_EMAIL_MESSAGE));       //REMEMBER TO CHANGE TO PROPER ERROR MESSAGE                              
        } else if (time.equalsIgnoreCase("12:00pm")) {
            milTime = 12;
        } else {
            int index = time.indexOf(":");
            String subStringTime = time.substring(0, index);
            milTime = Integer.parseInt(subStringTime);
            if (time.contains("p")) {
                milTime += 12;
            }
        }
        return milTime;
    }
}
