package tam.workspace;

import java.util.Collections;
import java.util.HashMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import tam.data.ProjectData;
import tam.data.ProjectStudentData;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class RemoveProjStudentData_Trans implements jTPS_Transaction {

    private String firstName;
    private String lastName;
    private String team;
    private String role;
   
    private TAData dataManager;
    private TAWorkspace workspace;

    public RemoveProjStudentData_Trans(String firstname, String lastname, String team1, String role1, TAData taData, TAWorkspace work) {
        firstName=firstname;
        lastName=lastname;
        team=team1;
        role=role1;
        dataManager=taData;
        workspace=work;
    }

    @Override
    public void doTransaction() {  //Control Y
        ObservableList<String> list = FXCollections.observableArrayList();
       System.out.println("undo Transaction");
        ObservableList<ProjectStudentData> projData = dataManager.getProjStudentData();
        for (ProjectStudentData proj : projData) {
            if(proj.getFirstName().equals(firstName) && proj.getLastName().equals(lastName) && proj.getTeam().equals(team) && proj.getRole().equals(role)){
                projData.remove(proj);
                //workspace.getStudentProjTeamText().getSelectionModel().clearSelection();
                return;
            }
            
        }
       
        workspace.getStudentProjFirstNameText().clear();
        workspace.getStudentProjLastNameText().clear();
        workspace.getStudentProjRoleText().clear();
        workspace.getStudentProjTeamText().setPromptText("Team");
                
        
    }
       
    
    

    @Override
    public void undoTransaction() {  //Control Z 
      dataManager.addProjStudentData(firstName, lastName, team, role);
      
        workspace.getStudentProjFirstNameText().clear();
        workspace.getStudentProjLastNameText().clear();
        workspace.getStudentProjRoleText().clear();
        workspace.getStudentProjTeamText().setPromptText("Team");
        // data.removeTA(taName);

}
}
