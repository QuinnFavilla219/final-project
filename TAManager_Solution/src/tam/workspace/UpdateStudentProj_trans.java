/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;


import javafx.collections.ObservableList;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam.data.ProjectData;
import tam.data.ProjectStudentData;
import tam.data.TAData;
import tam.data.TeachingAssistant;


/**
 *
 * @author khurr
 */
public class UpdateStudentProj_trans implements jTPS_Transaction {

    private String firstName;
    private String newFirstName;
    private String lastName;
    private String newLastName;
    private String role;
    private String newRole;
    private String team;
    private String newTeam;
    private TAData data; 
    private TAWorkspace transWorkspace; 

    public UpdateStudentProj_trans(String firstName1, String newFirstName1, String lastName1, String newLastName1, String team1, String newTeam1, String role1, String newRole1, TAData data1, TAWorkspace workspace) {
        firstName=firstName1;
        newFirstName=newFirstName1;
        lastName=lastName1;
        newLastName=newLastName1;
        role=role1;
        newRole=newRole1;
        team=team1;
        newTeam=newTeam1;
        transWorkspace=workspace; 
        data=data1;
    }

    @Override
    public void doTransaction() {  //Control Y 
        ProjectStudentData x= data.getStudentProj(firstName, lastName, team, role);
        x.setFirstName(newFirstName);
        x.setLastName(newLastName);
        x.setRole(newRole);
        x.setTeam(newTeam);
        
       
        
        
        
        transWorkspace.getStudentProjFirstNameText().setText(newFirstName);
        transWorkspace.getStudentProjLastNameText().setText(newLastName);
        transWorkspace.getStudentProjTeamText().getSelectionModel().select(newTeam);
        transWorkspace.getStudentProjRoleText().setText(newRole);
        transWorkspace.getProjStudentTable().refresh();
        

    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("updateTA undoTransaction ");
        ProjectStudentData x= data.getStudentProj(newFirstName, newLastName, newTeam, newRole);
        x.setFirstName(firstName);
        x.setLastName(lastName);
        x.setRole(role);
        x.setTeam(team);
        
        
        
        transWorkspace.getStudentProjFirstNameText().setText(firstName);
        transWorkspace.getStudentProjLastNameText().setText(lastName);
        transWorkspace.getStudentProjTeamText().getSelectionModel().select(team);
        transWorkspace.getStudentProjRoleText().setText(role);
        transWorkspace.getProjStudentTable().refresh();
        //transWorkspace.taTable.refresh();

    }

}
