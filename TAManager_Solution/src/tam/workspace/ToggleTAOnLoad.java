/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import javafx.beans.property.StringProperty;
import javafx.scene.layout.Pane;
import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class ToggleTAOnLoad implements jTPS_Transaction {
private String cellKey;
private String taName;
private TAData dataspace;
    /**
     *
     * @param selectedItem
     * @param workspace
     * @param app
     * @param pane
     */
    public ToggleTAOnLoad(String cellPlace, String name, TAData dataspacee) {
    this.dataspace=dataspacee;
    this.cellKey=cellPlace;
    this.taName=name;
    }

    @Override
    public void doTransaction() {
    String [] keys2=cellKey.split("_");
    String col=keys2[0];
    String row=keys2[1];
    int colNum=Integer.parseInt(col);
    int rowNum=Integer.parseInt(row);
    StringProperty cellText=dataspace.getCellTextProperty(colNum, rowNum);
    String cellText2=cellText.getValue();
    if(!cellText2.contains(taName)){
    dataspace.toggleTAOfficeHours(cellKey, taName);
    }
    else{
     dataspace.removeTAFromCell(cellText, taName);   
    }

    }

    @Override
    public void undoTransaction() {
    String [] keys2=cellKey.split("_");
    String col=keys2[0];
    String row=keys2[1];
    int colNum=Integer.parseInt(col);
    int rowNum=Integer.parseInt(row);
    StringProperty cellProp=dataspace.getCellTextProperty(colNum, rowNum);
    String cellText=cellProp.getValue();
    if(cellText.contains(taName)){
    dataspace.removeTAFromCell(cellProp, taName);
    }
    else{
     dataspace.toggleTAOfficeHours(cellKey, taName);   
    }
    

    }

}

