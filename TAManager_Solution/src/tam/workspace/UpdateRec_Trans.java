/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.Collections;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import jtps.jTPS_Transaction;
import tam.data.RecitationData;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class UpdateRec_Trans implements jTPS_Transaction {

    private String sec;
    private String inst;
    private String tim;
    private String loc;
    private String ta1;
    private String ta2;
    private TAData dataManager;
    private String secOld;
    private String instOld;
    private String timOld;
    private String locOld;
    private String ta1Old;
    private String ta2Old;
    private TAWorkspace workspace;
    

    public UpdateRec_Trans(String section, String oldSec, String instructor, String oldInstructor, String time, String oldTime,
            String location, String oldLoc, String teach1, String old1, String teach2, String old2, TAData taData, TAWorkspace work) {
        sec=section;
        inst=instructor;
        tim=time;
        loc=location;
        ta1=teach1;
        ta2=teach2;
        dataManager=taData;
        secOld=oldSec;
        instOld=oldInstructor;
        timOld=oldTime;
        locOld=oldLoc;
        ta1Old=old1;
        ta2Old=old2;
        workspace=work;
    }

    @Override
    public void doTransaction() {  //Control Y 
        System.out.println("updateRec undoTransaction ");
        RecitationData rec= dataManager.getREC(secOld, instOld, timOld, locOld, ta1Old, ta2Old);
        rec.setSection(sec);
        rec.setDayAndTime(tim);
        System.out.println(inst);
        rec.setInstructor(inst);
        rec.setLocation(loc);
        rec.setTA1(ta1);
        rec.setTA2(ta2);
        workspace.sectionField.setText(sec);
        workspace.locationField.setText(loc);
        workspace.daytimeField.setText(tim);
        workspace.instructorField.setText(inst);
        workspace.superTACombo1.getSelectionModel().select(ta1);
        workspace.superTACombo1.getSelectionModel().select(ta2);
        workspace.getRecTable().refresh();
    }

    @Override
    public void undoTransaction() {  //Control Z 
        RecitationData rec= dataManager.getREC(sec, inst, tim, loc, ta1, ta2);
        rec.setSection(secOld);
        rec.setDayAndTime(timOld);
        System.out.println(instOld);
        rec.setInstructor(instOld);
        rec.setLocation(locOld);
        rec.setTA1(ta1Old);
        rec.setTA2(ta2Old);
        workspace.sectionField.setText(secOld);
        workspace.locationField.setText(locOld);
        workspace.daytimeField.setText(timOld);
        workspace.instructorField.setText(instOld);
        workspace.superTACombo1.getSelectionModel().select(ta1Old);
        workspace.superTACombo1.getSelectionModel().select(ta2Old);
        System.out.println("updateTA undoTransaction ");
        workspace.getRecTable().refresh();

        }
        // data.removeTA(taName);

    }



