/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;


import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import tam.TAManagerApp;
import tam.data.ProjectData;
import tam.data.TAData;
import tam.data.TeachingAssistant;


/**
 *
 * @author khurr
 */
public class UpdateProj_Trans implements jTPS_Transaction {

    private String name;
    private String newName;
    private String link;
    private String newLink;
    private String txtColor;
    private String newTxtColor;
    private String color;
    private String newColor;
    private TAData data; 
    private TAWorkspace transWorkspace; 

    public UpdateProj_Trans(String newName1, String name1, String newHex, String hexColor, String newTextColor, String txtColor1, String newLink1, String link1, TAData data1, TAWorkspace workspace) {
        newName=newName1;
        name=name1;
        newColor=newHex;
        color=hexColor;
        newTxtColor=newTextColor;
        txtColor=txtColor1;
        link=link1;
        newLink=newLink1;
        transWorkspace=workspace; 
        data=data1;
    }

    @Override
    public void doTransaction() {  //Control Y 
        ProjectData x=data.getProjData(name, color, txtColor, link);
        x.setName(newName);
        x.setColorHex(newColor);
        x.setLink(newLink);
        x.setTxtColor(newTxtColor);
        transWorkspace.getProjLinkTextField().setText(newLink);
        transWorkspace.getProjNameTextField().setText(newName);
        String color1='#'+newColor;
        Color newColor1=Color.web(color1);
        String color2='#'+newTxtColor;
        Color newTxtColor1=Color.web(color2);
        transWorkspace.getColorPicker1().setValue(newColor1);
        transWorkspace.getColorPicker2().setValue(newTxtColor1);
        transWorkspace.getProjTable().refresh();
        

    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("updateTA undoTransaction ");
        ProjectData x=data.getProjData(newName, newColor, newTxtColor, newLink);
        System.out.println(name);
        x.setName(name);
        x.setColorHex(color);
        x.setLink(link);
        x.setTxtColor(txtColor);
        transWorkspace.getProjLinkTextField().setText(link);
        transWorkspace.getProjNameTextField().setText(name);
        String color1='#'+color;
        Color color11=Color.web(color1);
        String color2='#'+txtColor;
        Color txtColor1=Color.web(color2);
        transWorkspace.getColorPicker1().setValue(color11);
        transWorkspace.getColorPicker2().setValue(txtColor1);
        transWorkspace.getProjTable().refresh();
        //transWorkspace.taTable.refresh();

    }

}