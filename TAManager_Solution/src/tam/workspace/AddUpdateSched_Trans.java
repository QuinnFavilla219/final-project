/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import jtps.jTPS_Transaction;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class AddUpdateSched_Trans implements jTPS_Transaction {

    private String type;
    private String date;
    private String title;
    private String topic;
    private String link;
    private String time;
    private String criteria;
    private TAData data;
    private String typeOld;
    private String dateOld;
    private String titleOld;
    private String topicOld;
    private String linkOld;
    private String timeOld;
    private String criteriaOld;
    private TAWorkspace workspace;
    

    public AddUpdateSched_Trans(String updateType, String type1, String updateCriteria, String criteria1, 
                       String updateDate, String date1, String updateLink, String link1, String updateTime, String time1, String updateTitle, String title1, 
                       String updateTopic, String topic1, TAData taData, TAWorkspace work) {
       type=updateType;
       typeOld=type1;
       date=updateDate;
       dateOld=date1;
       title=updateTitle;
       titleOld=title1;
       topic=updateTopic;
       topicOld=topic1;
       link=updateLink;
       linkOld=link1;
       time=updateTime;
       timeOld=time1;
       criteria=updateCriteria;
       criteriaOld=criteria1;
       data=taData;
       workspace=work;
       
    }

    @Override
    public void doTransaction() {  //Control Y 
        System.out.println("updateRec undoTransaction ");
        System.out.println(type);
        System.out.println(typeOld);
        ScheduleData sched= data.getSched(typeOld, dateOld, titleOld, topicOld, linkOld, timeOld, criteriaOld);
        sched.setTyle(type);
        sched.setTime(time);
        sched.setDate(date);
        sched.setTitle(title);
        sched.setLink(link);
        sched.setTopic(topic);
        sched.setCriteria(criteria);
        TextField titleText=workspace.getTitleSchedTextField();
        TextField topicText=workspace.getTopicSchedTextField();
        TextField criteriaText=workspace.getCriteriaSchedTextField();
        TextField linkText=workspace.getLinkSchedTextField();
        TextField timeText=workspace.getTimeSchedTextField();
        DatePicker dateBox=workspace.getDateBox();
        ComboBox typeBox=workspace.getHolidayPicker();
       
        titleText.setText(title);
        topicText.setText(topic);
        criteriaText.setText(criteria);
        linkText.setText(link);
        timeText.setText(time);
        String [] dates;
        if(date.contains("-")){
        dates=date.split("-");
        }
        else{
           dates=date.split("/");
        }
        final DatePicker datePicker= new DatePicker(LocalDate.now());
            dateBox.setOnAction(new EventHandler(){
            public void handle(Event t){
                LocalDate thisDate=dateBox.getValue();
            }
        });
         try{
             if(dates[0].length()==1){
                 dates[0]='0'+dates[0];
             }
             if(dates[1].length()==1){
                 dates[1]='0'+dates[1];
             }
        dateBox.setValue(LocalDate.of(Integer.parseInt(dates[0]), Integer.parseInt(dates[1]), Integer.parseInt(dates[2])));
        }
        catch(Exception e){
            if(dates[0].length()==1){
                 dates[0]='0'+dates[0];
             }
             if(dates[1].length()==1){
                 dates[1]='0'+dates[1];
             }
             System.out.println(dates[2] + " " + dates[0] + " " + dates[1]);
        dateBox.setValue(LocalDate.of(Integer.parseInt(dates[2]), Integer.parseInt(dates[0]), Integer.parseInt(dates[1])));    
        }
        typeBox.getSelectionModel().select(type);
        //workspace.getSchedTable().setItems(data.getSchedData());
        workspace.getSchedTable().refresh();
    }

    @Override
    public void undoTransaction() {  //Control Z 
        ScheduleData sched= data.getSched(type, date, title, topic, link, time, criteria);
        sched.setTyle(typeOld);
        sched.setTime(timeOld);
        sched.setDate(dateOld);
        sched.setTitle(titleOld);
        sched.setLink(linkOld);
        sched.setTopic(topicOld);
        sched.setCriteria(criteriaOld);
        TextField titleText=workspace.getTitleSchedTextField();
        TextField topicText=workspace.getTopicSchedTextField();
        TextField criteriaText=workspace.getCriteriaSchedTextField();
        TextField linkText=workspace.getLinkSchedTextField();
        TextField timeText=workspace.getTimeSchedTextField();
        DatePicker dateBox=workspace.getDateBox();
        ComboBox typeBox=workspace.getHolidayPicker();
       
        titleText.setText(titleOld);
        topicText.setText(topicOld);
        criteriaText.setText(criteriaOld);
        linkText.setText(linkOld);
        timeText.setText(timeOld);
        String [] dates;
        if(dateOld.contains("-")){
        dates=dateOld.split("-");
        }
        else{
           dates=dateOld.split("/");
        }
        final DatePicker datePicker= new DatePicker(LocalDate.now());
            dateBox.setOnAction(new EventHandler(){
            public void handle(Event t){
                LocalDate thisDate=dateBox.getValue();
            }
        });
         try{
             if(dates[0].length()==1){
                 dates[0]='0'+dates[0];
             }
             if(dates[1].length()==1){
                 dates[1]='0'+dates[1];
             }
         dateBox.setValue(LocalDate.of(Integer.parseInt(dates[2]), Integer.parseInt(dates[0]), Integer.parseInt(dates[1])));
        }
        catch(Exception e){
            if(dates[0].length()==1){
                 dates[0]='0'+dates[0];
             }
             if(dates[1].length()==1){
                 dates[1]='0'+dates[1];
             }
        dateBox.setValue(LocalDate.of(Integer.parseInt(dates[2]), Integer.parseInt(dates[0]), Integer.parseInt(dates[1])));    
        }
        typeBox.getSelectionModel().select(type);
        //workspace.getSchedTable().setItems(data.getSchedData());
        workspace.getSchedTable().refresh();
    }

        // data.removeTA(taName);

    }



