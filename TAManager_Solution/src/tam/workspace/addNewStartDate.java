/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import jtps.jTPS_Transaction;
import tam.data.TAData;

/**
 *
 * @author User
 */
public class addNewStartDate implements jTPS_Transaction {
    TAWorkspace work;
    LocalDate newStart;
    TAData dataManager;
    String oldEndDate;
    
    public addNewStartDate(LocalDate endDate, String oldDate, TAData data, TAWorkspace workspace){
        work=workspace;
        newStart=endDate;
        dataManager=data;
        oldEndDate=oldDate;
        
    }

    @Override
    public void doTransaction() {
        String newEndDate=newStart.toString();
        dataManager.setEndDate(newEndDate);
        work.getEnder().getEditor().setText(newEndDate);
        
    }
    

    @Override
    public void undoTransaction() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(oldEndDate, format);
        String oldDate=localDate.toString();
        
        dataManager.setEndDate(oldDate);
        work.getEnder().getEditor().setText(oldDate);
        
    }
    

}
