/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.Collections;
import java.util.HashMap;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import jtps.jTPS_Transaction;
import tam.data.ProjectData;
import tam.data.ProjectStudentData;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class AddProjStudent_Trans implements jTPS_Transaction {

    private String firstname;
    private String lastname;
    private String team;
    private String role;
    private TAData dataManager;
    private TAWorkspace work;

    public AddProjStudent_Trans(String firstName, String lastName, String team1, String role1, TAData taData, TAWorkspace workspace) {
        
        team=team1;
        role=role1;
        firstname=firstName;
        lastname=lastName;
        dataManager=taData;
        work=workspace;
    }

    @Override
    public void doTransaction() {  //Control Y 
        dataManager.addProjStudentData(firstname, lastname, team, role);
        //Collections.sort(data.getTeachingAssistants());
        System.out.println("doTransaction");
        work.getStudentProjFirstNameText().clear();
        work.getStudentProjLastNameText().clear();
        work.getStudentProjRoleText().clear();
        work.getStudentProjTeamText().setPromptText("Team");
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo Transaction");
        System.out.println("undo Transaction");
        ObservableList<ProjectStudentData> projData = dataManager.getProjStudentData();
        for (ProjectStudentData proj : projData) {
            if(proj.getFirstName().equals(firstname) && proj.getLastName().equals(lastname) && proj.getTeam().equals(team) && proj.getRole().equals(role)){
                projData.remove(proj);
                return;
            }
            
        }
        /*
         for(ProjectStudentData proj: projData){
            work.getStudentProjTeamText().getItems().add(proj.getTeam());
        }
        
    */
        work.getStudentProjFirstNameText().clear();
        work.getStudentProjLastNameText().clear();
        work.getStudentProjRoleText().clear();
        work.getStudentProjTeamText().setPromptText("Team");
    }
        // data.removeTA(taName);

    }
