/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tam.workspace;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import jtps.jTPS_Transaction;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;

/**
 *
 * @author khurr
 */
public class ChangeDate_Trans implements jTPS_Transaction {

    private ObservableList<ScheduleData> currSchedData;
    private ObservableList<ScheduleData> newSchedData;
    private ArrayList<ScheduleData> rejects;
    private ObservableList<ScheduleData> previousList=FXCollections.observableArrayList();
    TAWorkspace workspace;
    TAData dataManager;

    public ChangeDate_Trans(ObservableList origList, ObservableList newList, ArrayList<ScheduleData> x, TAWorkspace work, TAData data) {
        currSchedData=origList;
        newSchedData=newList;
        workspace=work;
        dataManager=data;
        rejects=x;
    }

    @Override
    public void doTransaction() {  //Control Y
        ArrayList<ScheduleData> temp= new ArrayList<ScheduleData>();
        /*
        for(ScheduleData x: currSchedData){
            for(ScheduleData y: rejects){
                if(x.getDate().equals(y.getDate()) && x.getLink().equals(y.getLink()) && x.getTime().equals(y.getTime()) &&
                        x.getTitle().equals(y.getTitle()) && x.getTopic().equals(y.getTopic()) && x.getType().equals(y.getType())
                        && x.getCriteria().equals(y.getCriteria())){
                    currSchedData.remove(y);
                    
                }
            }
        }
*/
        for(ScheduleData y: rejects){
            dataManager.removeSched(y.getType(), y.getDate(), y.getTitle(), y.getTopic(), y.getLink(), y.getTime(), y.getCriteria());
        }
        workspace.getSchedTable().refresh();
       
    }

    @Override
    public void undoTransaction() {  //Control Z 
        System.out.println("undo Transaction");
        /*
        currSchedData.setAll(previousList);
        workspace.getSchedTable().setItems(currSchedData);
        dataManager.getSchedData().setAll(currSchedData);
*/
        for(ScheduleData x: rejects){
            dataManager.addSchedData(x.getType(), x.getDate(), x.getTitle(), x.getTopic(), x.getLink(), x.getTime(), x.getCriteria());
        }
        workspace.getSchedTable().refresh();
        // data.removeTA(taName);

    }

}