package tam.file;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import djf.components.AppDataComponent;
import djf.components.AppFileComponent;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import tam.TAManagerApp;
import tam.data.CoursePage;
import tam.data.ProjectData;
import tam.data.ProjectStudentData;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.workspace.TAWorkspace;
import test_bed.testSave;

/**
 * This class serves as the file component for the TA
 * manager app. It provides all saving and loading 
 * services for the application.
 * 
 * @author Richard McKenna
 */
public class TAFiles implements AppFileComponent {
    // THIS IS THE APP ITSELF
    TAManagerApp app;
    
    ArrayList<TimeSlot> taGridHours;
    
    
    // THESE ARE USED FOR IDENTIFYING JSON TYPES
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_GRAD_TAS = "grad_tas";
    static final String JSON_EMAIL = "email";
    static final String JSON_SUBJECT = "subject";
    static final String JSON_YEAR = "year";
    static final String JSON_SEMESTER = "semester";
    static final String JSON_NUMBER = "number";
    static final String JSON_TITLETEXTFIELD = "title";
    static final String JSON_INSTRUCTORNAMEFIELD = "name";
    static final String JSON_INSTRUCTORHOMEFIELD = "home";
    static final String JSON_EXPORTDIR = "export dir";
    static final String JSON_COURSEINFO = "Course Info";
    static final String JSON_TEMPLATEDIR = "template dir";
    static final String JSON_HOME = "home";
    static final String JSON_SYLLABUS = "syllabus";
    static final String JSON_SCHEDULE = "schedule";
    static final String JSON_HWS = "HWS";
    static final String JSON_PROJECTS = "projects";
    static final String JSON_SITETEMPLATE = "Site Template";
    static final String JSON_PAGESTYLE = "Page Style";
    static final String JSON_STYLESHEET= "Stylesheet";
    static final String JSON_BANNERIMAGE= "banner image";
    static final String JSON_RIGHTFOOTER= "right footer image";
    static final String JSON_LEFTFOOTER= "left footer image";
    static final String JSON_CHECKBOX= "checkbox";
    static final String JSON_OLDCHECKBOX= "oldcheckbox";
    static final String JSON_SECTION= "section";
    static final String JSON_INSTRUCTOR= "instructor";
    static final String JSON_DAYTIME= "day_time";
    static final String JSON_LOCATION= "location";
    static final String JSON_TA= "ta_1";
    static final String JSON_TA2= "ta_2";
    static final String JSON_RECITATIONS= "Recitations";
    static final String JSON_TYPE= "type";
    static final String JSON_DATE= "date";
    static final String JSON_TITLE= "title";
    static final String JSON_TOPIC= "topic";
    static final String JSON_SCHEDULEITEMS= "Schedule Items";
    static final String JSON_TEAMS= "Teams";
    static final String JSON_TEAMNAME= "name";
    static final String JSON_COLOR= "color (hex#)";
    static final String JSON_TEXTCOLOR= "text color(hex#)";
    static final String JSON_TEAMLINK= "link";
    static final String JSON_STUDENTS= "Students";
    static final String JSON_FIRSTNAME= "first name";
    static final String JSON_LASTNAME= "last name";
    static final String JSON_TEAM= "team";
    static final String JSON_ROLE= "role";
    static final String JSON_STARTINGMONDAYMONTH= "startingMondayMonth";
    static final String JSON_STARTINGMONDAYDAY= "startingMondayDay";
    static final String JSON_ENDINGFRIDAYMONTH= "endingFridayMonth";
    static final String JSON_ENDINGFRIDAYDAY= "endingFridayDay";
    static final String JSON_SCHEDLINK= "link:";
    static final String JSON_SCHEDTIME= "time";
    static final String JSON_SCHEDCRITERIA= "criteria";
    static final String JSON_SCHEDMONTH= "month";
    static final String JSON_SCHEDDAY= "day";
    static final String JSON_HOLIDAYS= "holidays";
    static final String JSON_LECTURES= "lectures";
    static final String JSON_SCHEDRECITATIONS= "recitations";
    static final String JSON_SCHEDHWS= "hws";
    static final String JSON_NAMES= "students";
    static final String JSON_ROLES= "roles";
    static final String JSON_WORK= "work";
    static final String JSON_RECS= "recitations";
    static final String JSON_LINK2= "link";
    static final String JSON_REFERENCES= "references";
    static final String JSON_SEMESTERTITLE= "semester";
    static final String JSON_PROJECTSTITLE= "projects";
    static final String JSON_NEWCOURSEINFO= "courseInfo";
    static final String JSON_NEWEXPORTDIR= "export_dir";
    static final String JSON_NEWTEMPLATEDIR= "template_dir";
    static final String JSON_NEWSITETEMPLATE= "siteTemplate";
    static final String JSON_NEWPAGESTYLE= "pageStyle";
    static final String JSON_NEWBANNERIMAGE= "banner_image";
    static final String JSON_NEWLEFTFOOTER= "left_footer";
    static final String JSON_NEWRIGHTFOOTER= "right_footer";
    static final String JSON_NEWSTYLESHEET= "stylesheet";
    static final String JSON_STARTYEAR= "startyear";
    static final String JSON_ENDYEAR= "endyear";
    
    
    
    
    
    public TAFiles(TAManagerApp initApp) {
        app = initApp;
        
        
    }
    
    public void setTAGridHours(ArrayList<TimeSlot> arr){
        this.taGridHours=arr;
    }
testSave x;
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
	// CLEAR THE OLD DATA OUT
	TAData dataManager = (TAData)data;
        //testSave x= new testSave(app);
       // x= new testSave();
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
        TAWorkspace workspace=(TAWorkspace)app.getWorkspaceComponent();
	// LOAD THE START AND END HOURS
	String startHour = json.getString(JSON_START_HOUR);
        String endHour = json.getString(JSON_END_HOUR);
        dataManager.initHours(startHour, endHour);
        

        // MAKR TESTSAVE OBJECT
        
        
        // NOW RELOAD THE WORKSPACE WITH THE LOADED DATA
       
        app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());
        //app.getWorkspaceComponent().reloadWorkspace2(app.getDataComponent());
        // NOW LOAD ALL THE UNDERGRAD TAs
        
        JsonArray jsonTAArray = json.getJsonArray(JSON_UNDERGRAD_TAS);
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            String name = jsonTA.getString(JSON_NAME);
            String email = jsonTA.getString(JSON_EMAIL);
            boolean checkbox=jsonTA.getBoolean(JSON_CHECKBOX);
            //boolean oldcheckbox=jsonTA.getBoolean(JSON_OLDCHECKBOX);
            dataManager.addTA(name, email, checkbox);
        }
        

        // AND THEN ALL THE OFFICE HOURS
        JsonArray jsonOfficeHoursArray = json.getJsonArray(JSON_OFFICE_HOURS);
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            String day = jsonOfficeHours.getString(JSON_DAY);
            String time = jsonOfficeHours.getString(JSON_TIME);
            String name = jsonOfficeHours.getString(JSON_NAME);
            dataManager.addOfficeHoursReservation(day, time, name);
        }
        
       
        
            JsonArray courseInfoArray = json.getJsonArray(JSON_COURSEINFO);
		JsonObject subject=courseInfoArray.getJsonObject(0);
                String subjectString=subject.getString(JSON_SUBJECT);
                String semesterString=subject.getString(JSON_SEMESTER);
                String numberString=subject.getString(JSON_NUMBER);
                String yearString=subject.getString(JSON_YEAR);
                String titleString=subject.getString(JSON_TITLETEXTFIELD);
                String nameString=subject.getString(JSON_NAME);
                String homeString=subject.getString(JSON_HOME);
                String exportDirString=subject.getString(JSON_EXPORTDIR);
                dataManager.setSubject(subjectString);
                dataManager.setSem(semesterString);
                dataManager.setNum(numberString);
                dataManager.setYear(yearString);
                dataManager.setTitleTextField(titleString);
                dataManager.setNameCourseTextField(nameString);
                dataManager.setHomeTextField(homeString);
                dataManager.setExportDir(exportDirString);
        
                
                JsonArray siteTemplateArray = json.getJsonArray(JSON_SITETEMPLATE);
		JsonObject templateDir=siteTemplateArray.getJsonObject(0);
                String templateString=templateDir.getString(JSON_TEMPLATEDIR);
                boolean home=templateDir.getBoolean(JSON_HOME);
                boolean syllabus=templateDir.getBoolean(JSON_SYLLABUS);
                boolean schedule=templateDir.getBoolean(JSON_SCHEDULE);
                boolean hws=templateDir.getBoolean(JSON_HWS);
                boolean projects=templateDir.getBoolean(JSON_PROJECTS);
                dataManager.setTemplateDir(templateString);
                dataManager.setHome(home);
                dataManager.setSyllabus(syllabus);
                dataManager.setSchedule(schedule);
                dataManager.setHWS(hws);
                dataManager.setProjects(projects);
                
                JsonArray pageStyleArray = json.getJsonArray(JSON_PAGESTYLE);
		JsonObject bannerSchoolImage=pageStyleArray.getJsonObject(0);
                String bannerString=bannerSchoolImage.getString(JSON_BANNERIMAGE);
                String leftFooter=bannerSchoolImage.getString(JSON_LEFTFOOTER);
                String rightFooter=bannerSchoolImage.getString(JSON_RIGHTFOOTER);
                String styleSheet=bannerSchoolImage.getString(JSON_STYLESHEET);
                dataManager.setBannerImage(bannerString);
                dataManager.setLeftFooterImage(leftFooter);
                dataManager.setRightFooterImage(rightFooter);
                dataManager.setStyleSheet(styleSheet);
                
            JsonArray jsonRecArray = json.getJsonArray(JSON_RECITATIONS);
            System.out.println(jsonRecArray.size());
            for (int i = 0; i < jsonRecArray.size(); i++) {
            JsonObject jsonrecHours = jsonRecArray.getJsonObject(i);
            System.out.println(jsonrecHours);
            String section = jsonrecHours.getString(JSON_SECTION);
            String instructor = jsonrecHours.getString(JSON_INSTRUCTOR);
            String daytime= jsonrecHours.getString(JSON_DAYTIME);
            System.out.println(daytime);
            String location = jsonrecHours.getString(JSON_LOCATION);
            System.out.println(daytime);
            String TA1 = jsonrecHours.getString(JSON_TA);
            String TA2 = jsonrecHours.getString(JSON_TA2);
            System.out.println(TA2);
            dataManager.addRecitation(section, instructor, daytime, location, TA1, TA2);
        
            }
            
            
        String startDay= json.getString(JSON_STARTINGMONDAYDAY);
        String startMonth=json.getString(JSON_STARTINGMONDAYMONTH);
        String endDay= json.getString(JSON_ENDINGFRIDAYDAY);
        String endMonth= json.getString(JSON_ENDINGFRIDAYMONTH);
        String endYear= json.getString(JSON_ENDYEAR);
        String startYear= json.getString(JSON_STARTYEAR);
        dataManager.setSchedEndDayDate(endDay);
        dataManager.setSchedEndMonthDate(endMonth);
        dataManager.setSchedStartDayDate(startDay);
        dataManager.setSchedStartMonthDate(startMonth);
        dataManager.setStartYear(startYear);
        dataManager.setEndYear(endYear);

            
            JsonArray jsonSchedArray = json.getJsonArray(JSON_SCHEDULEITEMS);
            for (int i = 0; i < jsonSchedArray.size(); i++) {
            JsonObject jsonsched = jsonSchedArray.getJsonObject(i);
            String type = jsonsched.getString(JSON_TYPE);
            String date = jsonsched.getString(JSON_DATE);
            String title= jsonsched.getString(JSON_TITLE);
            String topic = jsonsched.getString(JSON_TOPIC);
            String link= jsonsched.getString(JSON_SCHEDLINK);
            String time=jsonsched.getString(JSON_SCHEDTIME);
            String criteria=jsonsched.getString(JSON_SCHEDCRITERIA);
            dataManager.addSchedData(type, date, title, topic, link, time, criteria);
        
            }
            
            JsonArray jsonProjArray = json.getJsonArray(JSON_TEAMS);
            for (int i = 0; i < jsonProjArray.size(); i++) {
            JsonObject jsonProj = jsonProjArray.getJsonObject(i);
            String name = jsonProj.getString(JSON_TEAMNAME);
            String colorhex = jsonProj.getString(JSON_COLOR);
            String colortxt= jsonProj.getString(JSON_TEXTCOLOR);
            String link = jsonProj.getString(JSON_TEAMLINK);
            dataManager.addProjData(name, colorhex, colortxt, link);
        
            }
            
            JsonArray jsonProjStudentArray = json.getJsonArray(JSON_PROJECTS);
            for (int i = 0; i < jsonProjStudentArray.size(); i++) {
            JsonObject jsonProjStudent = jsonProjStudentArray.getJsonObject(i);
            String firstname = jsonProjStudent.getString(JSON_FIRSTNAME);
            String lastname = jsonProjStudent.getString(JSON_LASTNAME);
            String team= jsonProjStudent.getString(JSON_TEAM);
            String role = jsonProjStudent.getString(JSON_ROLE);
            dataManager.addProjStudentData(firstname, lastname, team, role);
        
            }
         workspace.reloadCourseSite(dataManager);
         //app.getWorkspaceComponent().reloadWorkspace(app.getDataComponent());
         
        
            
    
    }
      
    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }

    
    public void saveData2(AppDataComponent data, String filePath1, String filePath2,
            String filePath3, String filePath4, String filePath5) throws IOException {
	// GET THE DATA
	TAData dataManager = (TAData)data;
        JsonArray timeSlotsArray;
        System.out.println("hi");
	// NOW BUILD THE TA JSON OBJCTS TO SAVE
        
	
        
	// NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
        try{
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	timeSlotsArray = timeSlotArrayBuilder.build();
        }
        catch(Exception e){
            JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = this.taGridHours;
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
        
	timeSlotsArray = timeSlotArrayBuilder.build();
        }
            
        
        
        JsonArrayBuilder CourseInfoArrayBuilder = Json.createArrayBuilder(); 
	    JsonObject courseInfoJson = Json.createObjectBuilder()
		.add(JSON_SUBJECT, dataManager.getSubj())
                .add(JSON_SEMESTER, dataManager.getSem())
                .add(JSON_NUMBER, dataManager.getNum())
                .add(JSON_YEAR, dataManager.getYear())
                .add(JSON_TITLETEXTFIELD, dataManager.getTitleTextField())
                .add(JSON_INSTRUCTORNAMEFIELD, dataManager.getNameCourseTextField())
                .add(JSON_INSTRUCTORHOMEFIELD, dataManager.getHomeTextField())
                .add(JSON_EXPORTDIR, dataManager.getExportDir()).build();
	    CourseInfoArrayBuilder.add(courseInfoJson);
            JsonArray courseInfoArr= CourseInfoArrayBuilder.build();
            
        JsonArrayBuilder SiteTemplateArrayBuilder = Json.createArrayBuilder(); 
	    JsonObject siteTempJson = Json.createObjectBuilder()
		.add(JSON_TEMPLATEDIR, dataManager.getTemplateDir())
                .add(JSON_HOME, dataManager.getHome())
                .add(JSON_SYLLABUS, dataManager.getSyllabus())
                .add(JSON_SCHEDULE, dataManager.getSchedule())
                .add(JSON_HWS, dataManager.getHWS())
                .add(JSON_PROJECTS, dataManager.getProjects()).build();
	    SiteTemplateArrayBuilder.add(siteTempJson);
            JsonArray siteTemplateArr= SiteTemplateArrayBuilder.build();
            
            JsonArrayBuilder PageStyleArrayBuilder = Json.createArrayBuilder(); 
	    JsonObject pageStyleJson = Json.createObjectBuilder()
                .add(JSON_BANNERIMAGE, dataManager.getBannerImage())
                .add(JSON_LEFTFOOTER, dataManager.getLeftFooterImage())
                .add(JSON_RIGHTFOOTER, dataManager.getRightFooterImage())
                .add(JSON_STYLESHEET, dataManager.getStyleSheet()).build();
	    PageStyleArrayBuilder.add(pageStyleJson);
            JsonArray pageStyleArr= PageStyleArrayBuilder.build();
            
            JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
            JsonArrayBuilder taGradArrayBuilder = Json.createArrayBuilder();
            ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
            for (TeachingAssistant ta : tas) {
                if(ta.getUndergrad().getValue()==false){
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail()).build();
	    taArrayBuilder.add(taJson);
            }
                if(ta.getUndergrad().getValue()==true){
                  JsonObject taGradJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail()).build();
	    taGradArrayBuilder.add(taGradJson);  
                }
            }
            JsonArray undergradTAsArray = taArrayBuilder.build();
            JsonArray gradTAsArray = taGradArrayBuilder.build();
            
            JsonArrayBuilder recArrBuilder= Json.createArrayBuilder();
            ObservableList<RecitationData> recArr = dataManager.getRecDataTable();
            for (RecitationData RD : recArr) {
                String section=RD.getSection()+" "+"("+RD.getInstructor()+")";
	    JsonObject rdJson = Json.createObjectBuilder()
		    .add(JSON_SECTION, section)
                    .add(JSON_DAYTIME, RD.getDayAndTime())
                    .add(JSON_LOCATION, RD.getLocation())
                    .add(JSON_TA, RD.getTA1())
                    .add(JSON_TA2, RD.getTA2()).build();
	    recArrBuilder.add(rdJson);
            }
            
            JsonArray recArray = recArrBuilder.build();
            
            
            JsonArrayBuilder schedArrBuilder= Json.createArrayBuilder();
            JsonArrayBuilder schedArrBuilder2= Json.createArrayBuilder();
            JsonArrayBuilder schedArrBuilder3= Json.createArrayBuilder();
            JsonArrayBuilder schedArrBuilder4= Json.createArrayBuilder();
             JsonArrayBuilder schedArrBuilder5= Json.createArrayBuilder();
            ObservableList<ScheduleData> schedArr = dataManager.getSchedData();
            for (ScheduleData SD : schedArr) {
                if(SD.getType()=="Holiday"){
                    String date=SD.getDate();
                    String [] dates=date.split("/");
                    String year=dates[0];
                    String month=dates[1];
                    String day=dates[2];
	    JsonObject sdJson = Json.createObjectBuilder()
                    .add(JSON_SCHEDMONTH, month)
                    .add(JSON_SCHEDDAY, day)
                    .add(JSON_TITLE, SD.getTitle())
                    .add(JSON_LINK2, SD.getLink()).build();
	    schedArrBuilder.add(sdJson);
                }
                
                if(SD.getType()=="Lecture"){
                 String date=SD.getDate();
                 String [] dates=date.split("/");
                 String year=dates[0];
                 String month=dates[1];
                 String day=dates[2];
              JsonObject sdJson2 = Json.createObjectBuilder()
		    .add(JSON_SCHEDMONTH, month)
                    .add(JSON_SCHEDDAY, day)
                    .add(JSON_TITLE, SD.getTitle())
                    .add(JSON_TOPIC, SD.getTopic())
                    .add(JSON_LINK2, SD.getLink()).build();
	    schedArrBuilder2.add(sdJson2);
                }
                
                if(SD.getType()=="Reference"){
                 String date=SD.getDate();
                 String [] dates=date.split("/");
                 String year=dates[0];
                 String month=dates[1];
                 String day=dates[2];
              JsonObject sdJson5 = Json.createObjectBuilder()
		    .add(JSON_SCHEDMONTH, month)
                    .add(JSON_SCHEDDAY, day)
                    .add(JSON_TITLE, SD.getTitle())
                    .add(JSON_TOPIC, SD.getTopic())
                    .add(JSON_LINK2, SD.getLink()).build();
	    schedArrBuilder5.add(sdJson5);
                }
                
                 
                if(SD.getType()=="Recitation"){
                String date=SD.getDate();
                String [] dates=date.split("/");
                String year=dates[0];
                String month=dates[1];
                String day=dates[2];
              JsonObject sdJson4 = Json.createObjectBuilder()
		    .add(JSON_SCHEDMONTH, month)
                    .add(JSON_SCHEDDAY, day)
                    .add(JSON_TITLE, SD.getTitle())
                    .add(JSON_TOPIC, SD.getTopic()).build();
	    schedArrBuilder3.add(sdJson4);
                }
                
                if(SD.getType()=="HWs"){
                String date=SD.getDate();
                String [] dates=date.split("/");
                String year=dates[0];
                String month=dates[1];
                String day=dates[2];
              JsonObject sdJson5 = Json.createObjectBuilder()
		    .add(JSON_SCHEDMONTH, month)
                    .add(JSON_SCHEDDAY, day)
                    .add(JSON_TITLE, SD.getTitle())
                    .add(JSON_TOPIC, SD.getTopic())
                    .add(JSON_LINK2, SD.getLink())
                    .add(JSON_SCHEDTIME, SD.getTime())
                    .add(JSON_SCHEDCRITERIA, SD.getCriteria()).build();
	    schedArrBuilder4.add(sdJson5);
                }
                
                 
            }
        
            JsonArray schedArray = schedArrBuilder.build();
            JsonArray schedArray2 = schedArrBuilder2.build();
            JsonArray schedArray3 = schedArrBuilder3.build();
            JsonArray schedArray4 = schedArrBuilder4.build();
            JsonArray schedArray5= schedArrBuilder5.build();
            
            JsonArrayBuilder projArrBuilder= Json.createArrayBuilder();
            JsonArrayBuilder projStudentDataArrBuilder= Json.createArrayBuilder();
            ObservableList<ProjectStudentData> projStudentArr = dataManager.getProjStudentData();
            ObservableList<ProjectData> projArr = dataManager.getProjData();
            JsonArrayBuilder namesArr= Json.createArrayBuilder();
            JsonArrayBuilder rolesArr= Json.createArrayBuilder();
            for (ProjectData PD : projArr) {
             for (ProjectStudentData PSD : projStudentArr) {
                 if(PSD.getTeam()==PD.getName()){
                   String name=PSD.getFirstName()+ " "+ PSD.getLastName();
                   namesArr.add(name);
                   rolesArr.add(PSD.getRole());
            }  
             }    
	    JsonObject pdJson = Json.createObjectBuilder()
		    .add(JSON_TEAMNAME, PD.getName())
                    .add(JSON_NAMES, namesArr)
                    .add(JSON_ROLES, rolesArr)
                    .add(JSON_TEAMLINK, PD.getLink())
                    .add(JSON_COLOR, PD.getColorHex())
                    .add(JSON_TEXTCOLOR, PD.getTxtColor()).build();
	    projArrBuilder.add(pdJson);
            }
        
            JsonArray projArray = projArrBuilder.build();
            
            
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject courseInfoJSO = Json.createObjectBuilder()
                .add(JSON_COURSEINFO, courseInfoArr)
                .add(JSON_SITETEMPLATE, siteTemplateArr)
                .add(JSON_PAGESTYLE, pageStyleArr).build();
        
        JsonObject taDataJSO= Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_GRAD_TAS, gradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray).build();
        
        JsonObject recitationsJSO= Json.createObjectBuilder()
                .add(JSON_RECS, recArray).build();
        
        JsonObject scheduleJSO=Json.createObjectBuilder()
                .add(JSON_STARTINGMONDAYMONTH, dataManager.getSchedStartMonthDate())
                .add(JSON_STARTINGMONDAYDAY, dataManager.getSchedStartDayDate())
                .add(JSON_ENDINGFRIDAYMONTH, dataManager.getSchedEndMonthDate())
                .add(JSON_ENDINGFRIDAYDAY, dataManager.getSchedEndMonthDay())
                .add(JSON_HOLIDAYS, schedArray)
                .add(JSON_LECTURES, schedArray2)
                .add(JSON_REFERENCES, schedArray5)
                .add(JSON_SCHEDRECITATIONS, schedArray3)
                .add(JSON_SCHEDHWS, schedArray4).build();
        
        
        JsonObject projectJSO=Json.createObjectBuilder()
                .add(JSON_WORK, projArray).build();
                
   
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        
        
	StringWriter sw = new StringWriter();
	JsonWriter jsonCourseInfoWriter = writerFactory.createWriter(sw);
	jsonCourseInfoWriter.writeObject(courseInfoJSO);
	jsonCourseInfoWriter.close();
        
        StringWriter td = new StringWriter();
	JsonWriter jsonTADataWriter = writerFactory.createWriter(td);
	jsonTADataWriter.writeObject(taDataJSO);
	jsonTADataWriter.close();
        
        StringWriter rd = new StringWriter();
	JsonWriter jsonrecitationWriter = writerFactory.createWriter(rd);
	jsonrecitationWriter.writeObject(recitationsJSO);
	jsonrecitationWriter.close();
        
        StringWriter sd = new StringWriter();
	JsonWriter jsonscheduleWriter = writerFactory.createWriter(sd);
	jsonscheduleWriter.writeObject(scheduleJSO);
	jsonscheduleWriter.close();
        
        StringWriter pd = new StringWriter();
	JsonWriter jsonprojectWriter = writerFactory.createWriter(pd);
	jsonprojectWriter.writeObject(projectJSO);
	jsonprojectWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath1);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(courseInfoJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath1);
	pw.write(prettyPrinted);
	pw.close();
        
        OutputStream tdO = new FileOutputStream(filePath2);
	JsonWriter jsonFileWriter2 = Json.createWriter(tdO);
	jsonFileWriter2.writeObject(taDataJSO);
	String prettyPrinted2 = td.toString();
	PrintWriter pw2 = new PrintWriter(filePath2);
	pw2.write(prettyPrinted2);
	pw2.close();
        
        OutputStream rdO = new FileOutputStream(filePath3);
	JsonWriter jsonFileWriter3 = Json.createWriter(rdO);
	jsonFileWriter3.writeObject(recitationsJSO);
	String prettyPrinted3 = rd.toString();
	PrintWriter pw3 = new PrintWriter(filePath3);
	pw3.write(prettyPrinted3);
	pw3.close();
        
        OutputStream sdO = new FileOutputStream(filePath4);
	JsonWriter jsonFileWriter4 = Json.createWriter(sdO);
	jsonFileWriter4.writeObject(scheduleJSO);
	String prettyPrinted4 = sd.toString();
	PrintWriter pw4 = new PrintWriter(filePath4);
	pw4.write(prettyPrinted4);
	pw4.close();
        
        OutputStream pdO = new FileOutputStream(filePath5);
	JsonWriter jsonFileWriter5 = Json.createWriter(pdO);
	jsonFileWriter5.writeObject(projectJSO);
	String prettyPrinted5 = pd.toString();
	PrintWriter pw5 = new PrintWriter(filePath5);
	pw5.write(prettyPrinted5);
	pw5.close();
    }
    
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
	// GET THE DATA
	TAData dataManager = (TAData)data;
        TAWorkspace workspace = (TAWorkspace)app.getWorkspaceComponent();
        JsonArray timeSlotsArray;

	// NOW BUILD THE TA JSON OBJCTS TO SAVE
        
	
        
	// NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
        try{
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	timeSlotsArray = timeSlotArrayBuilder.build();
        }
        catch(Exception e){
            JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = this.taGridHours;
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
        
	timeSlotsArray = timeSlotArrayBuilder.build();
        }
            
        
        
        JsonArrayBuilder CourseInfoArrayBuilder = Json.createArrayBuilder(); 
	    JsonObject courseInfoJson = Json.createObjectBuilder()
		.add(JSON_SUBJECT, dataManager.getSubj())
                .add(JSON_SEMESTER, dataManager.getSem())
                .add(JSON_NUMBER, dataManager.getNum())
                .add(JSON_YEAR, dataManager.getYear())
                .add(JSON_TITLETEXTFIELD, dataManager.getTitleTextField())
                .add(JSON_INSTRUCTORNAMEFIELD, dataManager.getNameCourseTextField())
                .add(JSON_INSTRUCTORHOMEFIELD, dataManager.getHomeTextField())
                .add(JSON_EXPORTDIR, dataManager.getExportDir()).build();
	    CourseInfoArrayBuilder.add(courseInfoJson);
            JsonArray courseInfoArr= CourseInfoArrayBuilder.build();
            
        ObservableList<CoursePage> coursePages=workspace.al;
        JsonArrayBuilder SiteTemplateArrayBuilder = Json.createArrayBuilder(); 
	    JsonObject siteTempJson = Json.createObjectBuilder()
		.add(JSON_TEMPLATEDIR, dataManager.getTemplateDir())
                .add(JSON_HOME, coursePages.get(0).getCheckBox().getValue())
                .add(JSON_SYLLABUS, coursePages.get(1).getCheckBox().getValue())
                .add(JSON_SCHEDULE, coursePages.get(2).getCheckBox().getValue())
                .add(JSON_HWS, coursePages.get(3).getCheckBox().getValue())
                .add(JSON_PROJECTS, coursePages.get(4).getCheckBox().getValue()).build();
	    SiteTemplateArrayBuilder.add(siteTempJson);
            JsonArray siteTemplateArr= SiteTemplateArrayBuilder.build();
            
            JsonArrayBuilder PageStyleArrayBuilder = Json.createArrayBuilder(); 
	    JsonObject pageStyleJson = Json.createObjectBuilder()
                .add(JSON_BANNERIMAGE, dataManager.getBannerImage())
                .add(JSON_LEFTFOOTER, dataManager.getLeftFooterImage())
                .add(JSON_RIGHTFOOTER, dataManager.getRightFooterImage())
                .add(JSON_STYLESHEET, dataManager.getStyleSheet()).build();
	    PageStyleArrayBuilder.add(pageStyleJson);
            JsonArray pageStyleArr= PageStyleArrayBuilder.build();
            
            JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
            ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
            for (TeachingAssistant ta : tas) {	    
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail())
                    .add(JSON_CHECKBOX, ta.getUndergrad().getValue()).build();
	    taArrayBuilder.add(taJson);
            }
            JsonArray undergradTAsArray = taArrayBuilder.build();
            
            JsonArrayBuilder recArrBuilder= Json.createArrayBuilder();
            ObservableList<RecitationData> recArr = dataManager.getRecDataTable();
            for (RecitationData RD : recArr) {
	    JsonObject rdJson = Json.createObjectBuilder()
		    .add(JSON_SECTION, RD.getSection())
                    .add(JSON_INSTRUCTOR, RD.getInstructor())
                    .add(JSON_DAYTIME, RD.getDayAndTime())
                    .add(JSON_LOCATION, RD.getLocation())
                    .add(JSON_TA, RD.getTA1())
                    .add(JSON_TA2, RD.getTA2()).build();
	    recArrBuilder.add(rdJson);
            }
        
            JsonArray recArray = recArrBuilder.build();
            
            JsonArrayBuilder schedArrBuilder= Json.createArrayBuilder();
            ObservableList<ScheduleData> schedArr = dataManager.getSchedData();
            for (ScheduleData SD : schedArr) {
	    JsonObject sdJson = Json.createObjectBuilder()
		    .add(JSON_TYPE, SD.getType())
                    .add(JSON_DATE, SD.getDate())
                    .add(JSON_TITLE, SD.getTitle())
                    .add(JSON_TOPIC, SD.getTopic())
                    .add(JSON_SCHEDTIME, SD.getTime())
                    .add(JSON_SCHEDCRITERIA, SD.getCriteria())
                    .add(JSON_SCHEDLINK, SD.getLink()).build();
	    schedArrBuilder.add(sdJson);
            }
        
            JsonArray schedArray = schedArrBuilder.build();
            
            JsonArrayBuilder projArrBuilder= Json.createArrayBuilder();
            ObservableList<ProjectData> projArr = dataManager.getProjData();
            for (ProjectData PD : projArr) {
	    JsonObject pdJson = Json.createObjectBuilder()
		    .add(JSON_TEAMNAME, PD.getName())
                    .add(JSON_COLOR, PD.getColorHex())
                    .add(JSON_TEXTCOLOR, PD.getTxtColor())
                    .add(JSON_TEAMLINK, PD.getLink()).build();
	    projArrBuilder.add(pdJson);
            }
        
            JsonArray projArray = projArrBuilder.build();
            
            JsonArrayBuilder projStudentDataArrBuilder= Json.createArrayBuilder();
            ObservableList<ProjectStudentData> projStudentArr = dataManager.getProjStudentData();
            for (ProjectStudentData PSD : projStudentArr) {
	    JsonObject psdJson = Json.createObjectBuilder()
		    .add(JSON_FIRSTNAME, PSD.getFirstName())
                    .add(JSON_LASTNAME, PSD.getLastName())
                    .add(JSON_TEAM, PSD.getTeam())
                    .add(JSON_ROLE, PSD.getRole()).build();
	    projStudentDataArrBuilder.add(psdJson);
            }
        
            JsonArray projStudentDataArray = projStudentDataArrBuilder.build();
	
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_COURSEINFO, courseInfoArr)
                .add(JSON_SITETEMPLATE, siteTemplateArr)
                .add(JSON_PAGESTYLE, pageStyleArr)
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray)
                .add(JSON_RECITATIONS, recArray)
                .add(JSON_STARTINGMONDAYMONTH, dataManager.getSchedStartMonthDate())
                .add(JSON_STARTINGMONDAYDAY, dataManager.getSchedStartDayDate())
                .add(JSON_ENDINGFRIDAYMONTH, dataManager.getSchedEndMonthDate())
                .add(JSON_ENDINGFRIDAYDAY, dataManager.getSchedEndMonthDay())
                .add(JSON_ENDYEAR, dataManager.getEndYear())
                .add(JSON_STARTYEAR, dataManager.getStartYear())
                .add(JSON_SCHEDULEITEMS, schedArray)
                .add(JSON_TEAMS, projArray)
                .add(JSON_PROJECTS, projStudentDataArray)
		.build();
        
        
        

	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	StringWriter sw = new StringWriter();
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
    
    // IMPORTING/EXPORTING DATA IS USED WHEN WE READ/WRITE DATA IN AN
    // ADDITIONAL FORMAT USEFUL FOR ANOTHER PURPOSE, LIKE ANOTHER APPLICATION

    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        // GET THE DATA
        String filePath1=filePath+"\\js\\CourseInfoData.json";
        String filePath2=filePath+"\\js\\OfficeHoursData.json";
        String filePath3=filePath+"\\js\\RecitationData.json";
        String filePath4=filePath+"\\js\\ScheduleData.json";
        String filePath5=filePath+"\\js\\ProjectData.json";
        
        TAData dataManager = (TAData)data;
        JsonArray timeSlotsArray;
	// NOW BUILD THE TA JSON OBJCTS TO SAVE
        
	
        
	// NOW BUILD THE TIME SLOT JSON OBJCTS TO SAVE
        try{
	JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = TimeSlot.buildOfficeHoursList(dataManager);
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
	timeSlotsArray = timeSlotArrayBuilder.build();
        }
        catch(Exception e){
            JsonArrayBuilder timeSlotArrayBuilder = Json.createArrayBuilder();
	ArrayList<TimeSlot> officeHours = this.taGridHours;
	for (TimeSlot ts : officeHours) {	    
	    JsonObject tsJson = Json.createObjectBuilder()
		    .add(JSON_DAY, ts.getDay())
		    .add(JSON_TIME, ts.getTime())
		    .add(JSON_NAME, ts.getName()).build();
	    timeSlotArrayBuilder.add(tsJson);
	}
        
	timeSlotsArray = timeSlotArrayBuilder.build();
        }
            
        
        
        JsonArrayBuilder CourseInfoArrayBuilder = Json.createArrayBuilder(); 
	    JsonObject courseInfoJson = Json.createObjectBuilder()
		.add(JSON_SUBJECT, dataManager.getSubj())
                .add(JSON_SEMESTER, dataManager.getSem())
                .add(JSON_NUMBER, dataManager.getNum())
                .add(JSON_YEAR, dataManager.getYear())
                .add(JSON_TITLETEXTFIELD, dataManager.getTitleTextField())
                .add(JSON_INSTRUCTORNAMEFIELD, dataManager.getNameCourseTextField())
                .add(JSON_INSTRUCTORHOMEFIELD, dataManager.getHomeTextField())
                .add(JSON_NEWEXPORTDIR, dataManager.getExportDir()).build();
	    CourseInfoArrayBuilder.add(courseInfoJson);
            JsonArray courseInfoArr= CourseInfoArrayBuilder.build();
            
        JsonArrayBuilder SiteTemplateArrayBuilder = Json.createArrayBuilder(); 
	    JsonObject siteTempJson = Json.createObjectBuilder()
		.add(JSON_NEWTEMPLATEDIR, dataManager.getTemplateDir())
                .add(JSON_HOME, dataManager.getHome())
                .add(JSON_SYLLABUS, dataManager.getSyllabus())
                .add(JSON_SCHEDULE, dataManager.getSchedule())
                .add(JSON_HWS, dataManager.getHWS())
                .add(JSON_PROJECTS, dataManager.getProjects()).build();
	    SiteTemplateArrayBuilder.add(siteTempJson);
            JsonArray siteTemplateArr= SiteTemplateArrayBuilder.build();
            
            JsonArrayBuilder PageStyleArrayBuilder = Json.createArrayBuilder(); 
	    JsonObject pageStyleJson = Json.createObjectBuilder()
                .add(JSON_NEWBANNERIMAGE, dataManager.getBannerExport())
                .add(JSON_NEWLEFTFOOTER, dataManager.getLeftExport())
                .add(JSON_NEWRIGHTFOOTER, dataManager.getRightExport())
                .add(JSON_NEWSTYLESHEET, dataManager.getStyleSheet()).build();
	    PageStyleArrayBuilder.add(pageStyleJson);
            JsonArray pageStyleArr= PageStyleArrayBuilder.build();
            
            JsonArrayBuilder taArrayBuilder = Json.createArrayBuilder();
            JsonArrayBuilder taGradArrayBuilder = Json.createArrayBuilder();
            ObservableList<TeachingAssistant> tas = dataManager.getTeachingAssistants();
            for (TeachingAssistant ta : tas) {
                if(ta.getUndergrad().getValue()==true){
	    JsonObject taJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail()).build();
	    taArrayBuilder.add(taJson);
            }
                if(ta.getUndergrad().getValue()==false){
                  JsonObject taGradJson = Json.createObjectBuilder()
		    .add(JSON_NAME, ta.getName())
		    .add(JSON_EMAIL, ta.getEmail()).build();
	    taGradArrayBuilder.add(taGradJson);  
                }
            }
            JsonArray undergradTAsArray = taArrayBuilder.build();
            JsonArray gradTAsArray = taGradArrayBuilder.build();
            
            JsonArrayBuilder recArrBuilder= Json.createArrayBuilder();
            ObservableList<RecitationData> recArr = dataManager.getRecDataTable();
            for (RecitationData RD : recArr) {
                String section=RD.getSection()+" "+"("+RD.getInstructor()+")";
	    JsonObject rdJson = Json.createObjectBuilder()
		    .add(JSON_SECTION, section)
                    .add(JSON_DAYTIME, RD.getDayAndTime())
                    .add(JSON_LOCATION, RD.getLocation())
                    .add(JSON_TA, RD.getTA1())
                    .add(JSON_TA2, RD.getTA2()).build();
	    recArrBuilder.add(rdJson);
            }
            
            JsonArray recArray = recArrBuilder.build();
            
            
            JsonArrayBuilder schedArrBuilder= Json.createArrayBuilder();
            JsonArrayBuilder schedArrBuilder2= Json.createArrayBuilder();
            JsonArrayBuilder schedArrBuilder3= Json.createArrayBuilder();
            JsonArrayBuilder schedArrBuilder4= Json.createArrayBuilder();
             JsonArrayBuilder schedArrBuilder5= Json.createArrayBuilder();
            ObservableList<ScheduleData> schedArr = dataManager.getSchedData();
            for (ScheduleData SD : schedArr) {
                if(SD.getType().equals("Holiday")){
                    String date=SD.getDate();
                    String [] dates=date.split("/");
                    String month=dates[0];
                    String day=dates[1];
                    String year=dates[2];
                    if(month.charAt(0)==('0')){
                    month=month.substring(1);
                    }
             if(day.charAt(0)==('0')){
                 day=day.substring(1);
             }
	    JsonObject sdJson = Json.createObjectBuilder()
                    .add(JSON_SCHEDMONTH, month)
                    .add(JSON_SCHEDDAY, day)
                    .add(JSON_TITLE, SD.getTitle())
                    .add(JSON_LINK2, SD.getLink()).build();
	    schedArrBuilder.add(sdJson);
                }
                
                if(SD.getType().equals("Lecture")){
                 String date=SD.getDate();
                    String [] dates=date.split("/");
                    String month=dates[0];
                    String day=dates[1];
                    String year=dates[2];
                   if(month.charAt(0)==('0')){
                    month=month.substring(1);
                    }
             if(day.charAt(0)==('0')){
                 day=day.substring(1);
             }
              JsonObject sdJson2 = Json.createObjectBuilder()
		    .add(JSON_SCHEDMONTH, month)
                    .add(JSON_SCHEDDAY, day)
                    .add(JSON_TITLE, SD.getTitle())
                    .add(JSON_TOPIC, SD.getTopic())
                    .add(JSON_LINK2, SD.getLink()).build();
	    schedArrBuilder2.add(sdJson2);
                }
                
                if(SD.getType().equals("Reference")){
                    String date=SD.getDate();
                    String [] dates=date.split("/");
                    String month=dates[0];
                    String day=dates[1];
                    String year=dates[2];
                    if(month.charAt(0)==('0')){
                    month=month.substring(1);
                    }
             if(day.charAt(0)==('0')){
                 day=day.substring(1);
             }
              JsonObject sdJson5 = Json.createObjectBuilder()
		    .add(JSON_SCHEDMONTH, month)
                    .add(JSON_SCHEDDAY, day)
                    .add(JSON_TITLE, SD.getTitle())
                    .add(JSON_TOPIC, SD.getTopic())
                    .add(JSON_LINK2, SD.getLink()).build();
	    schedArrBuilder5.add(sdJson5);
                }
                
                 
                if(SD.getType().equals("Recitation")){
                    String date=SD.getDate();
                    String [] dates=date.split("/");
                    String month=dates[0];
                    String day=dates[1];
                    String year=dates[2];
                    if(month.charAt(0)==('0')){
                    month=month.substring(1);
                    }
             if(day.charAt(0)==('0')){
                 day=day.substring(1);
             }
              JsonObject sdJson4 = Json.createObjectBuilder()
		    .add(JSON_SCHEDMONTH, month)
                    .add(JSON_SCHEDDAY, day)
                    .add(JSON_TITLE, SD.getTitle())
                    .add(JSON_TOPIC, SD.getTopic()).build();
	    schedArrBuilder3.add(sdJson4);
                }
                
                if(SD.getType().equals("HWs")){
                String date=SD.getDate();
                    String [] dates=date.split("/");
                    String month=dates[0];
                    String day=dates[1];
                    String year=dates[2];
                    if(month.charAt(0)==('0')){
                    month=month.substring(1);
                    }
             if(day.charAt(0)==('0')){
                 day=day.substring(1);
             }
              JsonObject sdJson5 = Json.createObjectBuilder()
		    .add(JSON_SCHEDMONTH, month)
                    .add(JSON_SCHEDDAY, day)
                    .add(JSON_TITLE, SD.getTitle())
                    .add(JSON_TOPIC, SD.getTopic())
                    .add(JSON_LINK2, SD.getLink())
                    .add(JSON_SCHEDTIME, SD.getTime())
                    .add(JSON_SCHEDCRITERIA, SD.getCriteria()).build();
	    schedArrBuilder4.add(sdJson5);
                }
                
                 
            }
        
            JsonArray schedArray = schedArrBuilder.build();
            JsonArray schedArray2 = schedArrBuilder2.build();
            JsonArray schedArray3 = schedArrBuilder3.build();
            JsonArray schedArray4 = schedArrBuilder4.build();
            JsonArray schedArray5= schedArrBuilder5.build();
            
            JsonArrayBuilder projArrBuilder= Json.createArrayBuilder();
            JsonArrayBuilder projStudentDataArrBuilder= Json.createArrayBuilder();
            ObservableList<ProjectStudentData> projStudentArr = dataManager.getProjStudentData();
            ObservableList<ProjectData> projArr = dataManager.getProjData();
            JsonArrayBuilder namesArr= Json.createArrayBuilder();
            JsonArrayBuilder rolesArr= Json.createArrayBuilder();
            for (ProjectData PD : projArr) {
             for (ProjectStudentData PSD : projStudentArr) {
                 if(PSD.getTeam().equals(PD.getName())){
                   String name=PSD.getFirstName()+ " "+ PSD.getLastName();
                   namesArr.add(name);
                   rolesArr.add(PSD.getRole());
            }  
             }    
	    JsonObject pdJson = Json.createObjectBuilder()
		    .add(JSON_TEAMNAME, PD.getName())
                    .add(JSON_NAMES, namesArr)
                    .add(JSON_ROLES, rolesArr)
                    .add(JSON_TEAMLINK, PD.getLink())
                    .add(JSON_COLOR, PD.getColorHex())
                    .add(JSON_TEXTCOLOR, PD.getTxtColor()).build();
	    projArrBuilder.add(pdJson);
            
            }
            JsonArrayBuilder pd2Arr= Json.createArrayBuilder();
            
            JsonObject pd2Json= Json.createObjectBuilder()
                    .add(JSON_SEMESTERTITLE, "Spring 2017")
                    .add(JSON_PROJECTSTITLE, projArrBuilder).build();
            pd2Arr.add(pd2Json);
            
            
            
	// THEN PUT IT ALL TOGETHER IN A JsonObject
	JsonObject courseInfoJSO = Json.createObjectBuilder()
                .add(JSON_NEWCOURSEINFO, courseInfoArr)
                .add(JSON_NEWSITETEMPLATE, siteTemplateArr)
                .add(JSON_NEWPAGESTYLE, pageStyleArr).build();
        
        JsonObject taDataJSO= Json.createObjectBuilder()
		.add(JSON_START_HOUR, "" + dataManager.getStartHour())
		.add(JSON_END_HOUR, "" + dataManager.getEndHour())
                .add(JSON_UNDERGRAD_TAS, undergradTAsArray)
                .add(JSON_GRAD_TAS, gradTAsArray)
                .add(JSON_OFFICE_HOURS, timeSlotsArray).build();
        
        JsonObject recitationsJSO= Json.createObjectBuilder()
                .add(JSON_RECS, recArray).build();
        
        
        JsonObject scheduleJSO=Json.createObjectBuilder()
                .add(JSON_STARTINGMONDAYMONTH, dataManager.getSchedStartMonthDate())
                .add(JSON_STARTINGMONDAYDAY, dataManager.getSchedStartDayDate())
                .add(JSON_ENDINGFRIDAYMONTH, dataManager.getSchedEndMonthDate())
                .add(JSON_ENDINGFRIDAYDAY, dataManager.getSchedEndMonthDay())
                .add(JSON_HOLIDAYS, schedArray)
                .add(JSON_LECTURES, schedArray2)
                .add(JSON_REFERENCES, schedArray5)
                .add(JSON_SCHEDRECITATIONS, schedArray3)
                .add(JSON_SCHEDHWS, schedArray4).build();
                
        
        
        JsonObject projectJSO=Json.createObjectBuilder()
                .add(JSON_WORK, pd2Arr).build();
                
   
	
	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        
        
	StringWriter sw = new StringWriter();
	JsonWriter jsonCourseInfoWriter = writerFactory.createWriter(sw);
	jsonCourseInfoWriter.writeObject(courseInfoJSO);
	jsonCourseInfoWriter.close();
        
        StringWriter td = new StringWriter();
	JsonWriter jsonTADataWriter = writerFactory.createWriter(td);
	jsonTADataWriter.writeObject(taDataJSO);
	jsonTADataWriter.close();
        
        StringWriter rd = new StringWriter();
	JsonWriter jsonrecitationWriter = writerFactory.createWriter(rd);
	jsonrecitationWriter.writeObject(recitationsJSO);
	jsonrecitationWriter.close();
        
        StringWriter sd = new StringWriter();
	JsonWriter jsonscheduleWriter = writerFactory.createWriter(sd);
	jsonscheduleWriter.writeObject(scheduleJSO);
	jsonscheduleWriter.close();
        
        StringWriter pd = new StringWriter();
	JsonWriter jsonprojectWriter = writerFactory.createWriter(pd);
	jsonprojectWriter.writeObject(projectJSO);
	jsonprojectWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath1);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(courseInfoJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath1);
	pw.write(prettyPrinted);
	pw.close();
        
        OutputStream tdO = new FileOutputStream(filePath2);
	JsonWriter jsonFileWriter2 = Json.createWriter(tdO);
	jsonFileWriter2.writeObject(taDataJSO);
	String prettyPrinted2 = td.toString();
	PrintWriter pw2 = new PrintWriter(filePath2);
	pw2.write(prettyPrinted2);
	pw2.close();
        
        OutputStream rdO = new FileOutputStream(filePath3);
	JsonWriter jsonFileWriter3 = Json.createWriter(rdO);
	jsonFileWriter3.writeObject(recitationsJSO);
	String prettyPrinted3 = rd.toString();
	PrintWriter pw3 = new PrintWriter(filePath3);
	pw3.write(prettyPrinted3);
	pw3.close();
        
        OutputStream sdO = new FileOutputStream(filePath4);
	JsonWriter jsonFileWriter4 = Json.createWriter(sdO);
	jsonFileWriter4.writeObject(scheduleJSO);
	String prettyPrinted4 = sd.toString();
	PrintWriter pw4 = new PrintWriter(filePath4);
	pw4.write(prettyPrinted4);
	pw4.close();
        
        OutputStream pdO = new FileOutputStream(filePath5);
	JsonWriter jsonFileWriter5 = Json.createWriter(pdO);
	jsonFileWriter5.writeObject(projectJSO);
	String prettyPrinted5 = pd.toString();
	PrintWriter pw5 = new PrintWriter(filePath5);
	pw5.write(prettyPrinted5);
	pw5.close();
    
    }

}