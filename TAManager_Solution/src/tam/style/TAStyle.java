package tam.style;

import djf.AppTemplate;
import djf.components.AppStyleComponent;
import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.text.Text;
import tam.data.CoursePage;
import tam.data.RecitationData;
import tam.data.TeachingAssistant;
import tam.workspace.TAWorkspace;

/**
 * This class manages all CSS style for this application.
 * 
 * @author Richard McKenna
 * @version 1.0
 */
public class TAStyle extends AppStyleComponent {
    // FIRST WE SHOULD DECLARE ALL OF THE STYLE TYPES WE PLAN TO USE
    
    // WE'LL USE THIS FOR ORGANIZING LEFT AND RIGHT CONTROLS
    public static String CLASS_PLAIN_PANE = "plain_pane";
    
    // THESE ARE THE HEADERS FOR EACH SIDE
    public static String CLASS_HEADER_PANE = "header_pane";
    public static String CLASS_HEADER_LABEL = "header_label";

    // ON THE LEFT WE HAVE THE TA ENTRY
    public static String CLASS_TA_TABLE = "ta_table";
    public static String CLASS_TA_TABLE_COLUMN_HEADER = "ta_table_column_header";
    public static String CLASS_ADD_TA_PANE = "add_ta_pane";
    public static String CLASS_ADD_TA_TEXT_FIELD = "add_ta_text_field";
    public static String CLASS_ADD_TA_BUTTON = "add_ta_button";
    public static String CLASS_UPDATE_TA_BUTTON="update_ta_button";
    public static String CLASS_CLEAR_BUTTON="clear_button"; 
    public static String CLASS_CLEAR_BUTTON_1="clear_button_1"; 
    public static String CLASS_REMOVE_TA_BUTTON="remove_ta_button";
    public static String CLASS_TAB_PANE="tab-pane";
    public static String CLASS_COURSE_INFO_LABEL="course_info_label";
    public static String CLASS_COLOR_PICKER="color_picker";

    // ON THE RIGHT WE HAVE THE OFFICE HOURS GRID
    public static String CLASS_OFFICE_HOURS_GRID = "office_hours_grid";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_PANE = "office_hours_grid_time_column_header_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_LABEL = "office_hours_grid_time_column_header_label";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE = "office_hours_grid_day_column_header_pane";
    public static String CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_LABEL = "office_hours_grid_day_column_header_label";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE = "office_hours_grid_time_cell_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TIME_CELL_LABEL = "office_hours_grid_time_cell_label";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE = "office_hours_grid_ta_cell_pane";
    public static String CLASS_OFFICE_HOURS_GRID_TA_CELL_LABEL = "office_hours_grid_ta_cell_label";
    public static String CLASS_OFFICE_HOURS_GRID_NEW_START_TIME="office_hours_grid_new_start_time"; 
    public static String CLASS_OFFICE_HOURS_GRID_NEW_END_TIME="office_hours_grid_new_end_time";   
    public static String CLASS_OFFICE_HOURS_GRID_UPDATE_TIME_BUTTON="office_hours_grid_update_time_button";
    public static String CLASS_TEMPLATE_LABEL="template_label";
    public static String CLASS_TEMPLATE_COLUMN_HEADER="template_column_headers";
    public static String CLASS_TEMPLATE_TABLE="template_table";
    public static String CLASS_PAGE_STYLE_LABEL="page_style_label";
    public static String CLASS_COURSEDETAILS_COMBOBOXES="coursedetails_comboboxes";
    public static String CLASS_COURSEDETAILS_BUTTONS="coursedetails_buttons";
    public static String CLASS_REC_LABEL="rec_label";
    public static String CLASS_ADDEDITREC_LABEL="addeditrec_label";
    public static String CLASS_ALL_REC_LABELS="all_rec_labels";
    public static String CLASS_ALL_REC_BOXES="all_rec_boxes";
    public static String CLASS_REC_TABLE="rec_table";
    public static String CLASS_REC_COLUMN_HEADER="rec_column_headers";
    public static String CLASS_SCHEDULE_LABEL="schedule_label";
    public static String CLASS_SCHEDULEITEM_LABEL="scheduleitem_label";
    public static String CLASS_ADDUPDATE_LABEL="addupdate_label";
    public static String CLASS_DATEPICKER_BOXES="datepicker_boxes";
    public static String CLASS_PROJ_LABEL="proj_label";
    public static String CLASS_SCROLL_PANE="scroll_pane";
    public static String CLASS_BUTTONS="buttons";
    public static String CLASS_EXPORTADD_BUTTON="exportadd_button";

    // FOR HIGHLIGHTING CELLS, COLUMNS, AND ROWS
    public static String CLASS_HIGHLIGHTED_GRID_CELL = "highlighted_grid_cell";
    public static String CLASS_HIGHLIGHTED_GRID_ROW_OR_COLUMN = "highlighted_grid_row_or_column";
    
    // THIS PROVIDES ACCESS TO OTHER COMPONENTS
    private AppTemplate app;
    
    /**
     * This constructor initializes all style for the application.
     * 
     * @param initApp The application to be stylized.
     */
    public TAStyle(AppTemplate initApp) {
        // KEEP THIS FOR LATER
        app = initApp;

        // LET'S USE THE DEFAULT STYLESHEET SETUP
        super.initStylesheet(app);

        // INIT THE STYLE FOR THE FILE TOOLBAR
        app.getGUI().initFileToolbarStyle();

        // AND NOW OUR WORKSPACE STYLE
        initTAWorkspaceStyle();
    }

    /**
     * This function specifies all the style classes for
     * all user interface controls in the workspace.
     */
    private void initTAWorkspaceStyle() {
        // LEFT SIDE - THE HEADER
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.getTAsHeaderBox().getStyleClass().add(CLASS_HEADER_PANE);
        workspaceComponent.getTAsHeaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);

        // LEFT SIDE - THE TABLE
        TableView<TeachingAssistant> taTable = workspaceComponent.getTATable();
        taTable.getStyleClass().add(CLASS_TA_TABLE);
        for (TableColumn tableColumn : taTable.getColumns()) {
            tableColumn.getStyleClass().add(CLASS_TA_TABLE_COLUMN_HEADER);
        }
        
        TableView<CoursePage> template= workspaceComponent.getTemplate();
        template.getStyleClass().add(CLASS_TEMPLATE_TABLE);
        for (TableColumn tableColumn: template.getColumns()){
            tableColumn.getStyleClass().add(CLASS_TEMPLATE_COLUMN_HEADER);
        }
        
        TableView<RecitationData> recTable= workspaceComponent.getRecTable();
        recTable.getStyleClass().add(CLASS_TEMPLATE_TABLE);
        for (TableColumn tableColumn: recTable.getColumns()){
            tableColumn.getStyleClass().add(CLASS_TEMPLATE_COLUMN_HEADER);
        }
        
        TableView<CoursePage> schedTable= workspaceComponent.getSchedTable();
        schedTable.getStyleClass().add(CLASS_TEMPLATE_TABLE);
        for (TableColumn tableColumn: schedTable.getColumns()){
            tableColumn.getStyleClass().add(CLASS_TEMPLATE_COLUMN_HEADER);
        }
        
        TableView<CoursePage> projTable= workspaceComponent.getProjTable();
        projTable.getStyleClass().add(CLASS_TEMPLATE_TABLE);
        for (TableColumn tableColumn: projTable.getColumns()){
            tableColumn.getStyleClass().add(CLASS_TEMPLATE_COLUMN_HEADER);
        }
        
        TableView<CoursePage> projStudentTable= workspaceComponent.getProjStudentTable();
        projStudentTable.getStyleClass().add(CLASS_TEMPLATE_TABLE);
        for (TableColumn tableColumn: projStudentTable.getColumns()){
            tableColumn.getStyleClass().add(CLASS_TEMPLATE_COLUMN_HEADER);
        }
        
        ArrayList<ComboBox> boxes= workspaceComponent.getList();
        for(ComboBox x: boxes){
            x.getStyleClass().add(CLASS_COURSEDETAILS_COMBOBOXES);
        }
        
        
        ArrayList<Label> recLabels= workspaceComponent.getRecLabels();
        for(Label x: recLabels){
            x.getStyleClass().add(CLASS_ALL_REC_LABELS);
        }
       
        
        ArrayList<Button> courseDetailsButtons= workspaceComponent.getChangeButtonList();
        for (Button x: courseDetailsButtons){
            x.getStyleClass().add(CLASS_COURSEDETAILS_BUTTONS);
        }
       

        // LEFT SIDE - THE TA DATA ENTRY
        workspaceComponent.getAddBox().getStyleClass().add(CLASS_ADD_TA_PANE);
        workspaceComponent.getAddEdit().getStyleClass().add(CLASS_ADDEDITREC_LABEL);
        workspaceComponent.getNameTextField().getStyleClass().add(CLASS_ADD_TA_TEXT_FIELD);
        workspaceComponent.getEmailTextField().getStyleClass().add(CLASS_ADD_TA_TEXT_FIELD);
        workspaceComponent.getAddButton().getStyleClass().add(CLASS_ADD_TA_BUTTON);
        workspaceComponent.getUpdateTaButton().getStyleClass().add(CLASS_UPDATE_TA_BUTTON); 
        workspaceComponent.getClearButton().getStyleClass().add(CLASS_CLEAR_BUTTON); 
        workspaceComponent.getClearButton1().getStyleClass().add(CLASS_CLEAR_BUTTON_1);
        workspaceComponent.getTaRemoveButton().getStyleClass().add(CLASS_REMOVE_TA_BUTTON);
        workspaceComponent.getTab().getStyleClass().add(CLASS_TAB_PANE);
        workspaceComponent.getCourseInfoLabel().getStyleClass().add(CLASS_COURSE_INFO_LABEL);
        workspaceComponent.getTemplateLabel().getStyleClass().add(CLASS_TEMPLATE_LABEL);
        workspaceComponent.getPageStyle().getStyleClass().add(CLASS_PAGE_STYLE_LABEL);
        workspaceComponent.getRecLabel().getStyleClass().add(CLASS_REC_LABEL);
        workspaceComponent.getScheduleLabel().getStyleClass().add(CLASS_SCHEDULE_LABEL);
        workspaceComponent.getSchedItemLabel().getStyleClass().add(CLASS_SCHEDULEITEM_LABEL);
        workspaceComponent.getAddUpdateLabel().getStyleClass().add(CLASS_ADDUPDATE_LABEL);
        workspaceComponent.getStarter().getStyleClass().add(CLASS_DATEPICKER_BOXES);
        workspaceComponent.getEnder().getStyleClass().add(CLASS_DATEPICKER_BOXES);
        workspaceComponent.getDateBox().getStyleClass().add(CLASS_DATEPICKER_BOXES);
        workspaceComponent.getProjLabel().getStyleClass().add(CLASS_PROJ_LABEL);
        workspaceComponent.getTeamLabel().getStyleClass().add(CLASS_ADDUPDATE_LABEL);
        workspaceComponent.getAddEditProjData().getStyleClass().add(CLASS_ADDUPDATE_LABEL);
        workspaceComponent.getAddEditStudentsLabel().getStyleClass().add(CLASS_ADDUPDATE_LABEL);
        workspaceComponent.getStudentsLabel().getStyleClass().add(CLASS_ADDUPDATE_LABEL);
        workspaceComponent.getScrollPane().getStyleClass().add(CLASS_SCROLL_PANE);
        workspaceComponent.getColorPicker1().getStyleClass().add(CLASS_COLOR_PICKER);
        workspaceComponent.getColorPicker2().getStyleClass().add(CLASS_COLOR_PICKER);
        workspaceComponent.getSubj().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getNumber().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getSemester().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getYear().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getiName().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getiHome().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getTitle().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getTempMessage().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getTempAdd().getStyleClass().add(CLASS_EXPORTADD_BUTTON);
        workspaceComponent.getSitePages().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getBannerImage().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getLeftFooter().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getRightFooter().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getStyleMess().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getStyleLabel().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getDirAdd().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getExportLabelCourseDetails().getStyleClass().add(CLASS_BUTTONS);
        workspaceComponent.getCalendarLabel().getStyleClass().add(CLASS_EXPORTADD_BUTTON);        
        
        
    

        // RIGHT SIDE - THE HEADER
        workspaceComponent.getOfficeHoursSubheaderBox().getStyleClass().add(CLASS_HEADER_PANE);
        workspaceComponent.getOfficeHoursSubheaderLabel().getStyleClass().add(CLASS_HEADER_LABEL);
    }
    
    /**
     * This method initializes the style for all UI components in
     * the office hours grid. Note that this should be called every
     * time a new TA Office Hours Grid is created or loaded.
     */
    public void initOfficeHoursGridStyle() {
        // RIGHT SIDE - THE OFFICE HOURS GRID TIME HEADERS
        TAWorkspace workspaceComponent = (TAWorkspace)app.getWorkspaceComponent();
        workspaceComponent.getOfficeHoursGridPane().getStyleClass().add(CLASS_OFFICE_HOURS_GRID);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeHeaderPanes(), CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeHeaderLabels(), CLASS_OFFICE_HOURS_GRID_TIME_COLUMN_HEADER_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridDayHeaderPanes(), CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridDayHeaderLabels(), CLASS_OFFICE_HOURS_GRID_DAY_COLUMN_HEADER_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeCellPanes(), CLASS_OFFICE_HOURS_GRID_TIME_CELL_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTimeCellLabels(), CLASS_OFFICE_HOURS_GRID_TIME_CELL_LABEL);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTACellPanes(), CLASS_OFFICE_HOURS_GRID_TA_CELL_PANE);
        setStyleClassOnAll(workspaceComponent.getOfficeHoursGridTACellLabels(), CLASS_OFFICE_HOURS_GRID_TA_CELL_LABEL);
        workspaceComponent.getNewEndBox().getStyleClass().add( CLASS_OFFICE_HOURS_GRID_NEW_END_TIME); 
        workspaceComponent.getNewStartBox().getStyleClass().add(CLASS_OFFICE_HOURS_GRID_NEW_START_TIME);
        workspaceComponent.getChangeTimeButton().getStyleClass().add(CLASS_OFFICE_HOURS_GRID_UPDATE_TIME_BUTTON); 
    }
    
    /**
     * This helper method initializes the style of all the nodes in the nodes
     * map to a common style, styleClass.
     */
    private void setStyleClassOnAll(HashMap nodes, String styleClass) {
        for (Object nodeObject : nodes.values()) {
            Node n = (Node)nodeObject;
            n.getStyleClass().add(styleClass);
        }
    }
}