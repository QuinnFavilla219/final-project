/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import static djf.settings.AppStartupConstants.APP_PROPERTIES_FILE_NAME;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tam.TAManagerApp;
import tam.data.ProjectData;
import tam.data.ProjectStudentData;
import tam.data.RecitationData;
import tam.data.ScheduleData;
import tam.data.TAData;
import tam.data.TeachingAssistant;
import tam.file.TAFiles;
import tam.file.TimeSlot;
import test_bed.testSave;

/**
 *
 * @author User
 */
public class TestFiles {
    TAManagerApp app;
    
    ArrayList<TimeSlot> taGridHours; 
     
    static final String JSON_START_HOUR = "startHour";
    static final String JSON_END_HOUR = "endHour";
    static final String JSON_OFFICE_HOURS = "officeHours";
    static final String JSON_DAY = "day";
    static final String JSON_TIME = "time";
    static final String JSON_NAME = "name";
    static final String JSON_UNDERGRAD_TAS = "undergrad_tas";
    static final String JSON_GRAD_TAS = "grad_tas";
    static final String JSON_EMAIL = "email";
    static final String JSON_SUBJECT = "subject";
    static final String JSON_YEAR = "year";
    static final String JSON_SEMESTER = "semester";
    static final String JSON_NUMBER = "number";
    static final String JSON_TITLETEXTFIELD = "title";
    static final String JSON_INSTRUCTORNAMEFIELD = "name";
    static final String JSON_INSTRUCTORHOMEFIELD = "home";
    static final String JSON_EXPORTDIR = "export dir";
    static final String JSON_COURSEINFO = "Course Info";
    static final String JSON_TEMPLATEDIR = "template dir";
    static final String JSON_HOME = "home";
    static final String JSON_SYLLABUS = "syllabus";
    static final String JSON_SCHEDULE = "schedule";
    static final String JSON_HWS = "HWS";
    static final String JSON_PROJECTS = "projects";
    static final String JSON_SITETEMPLATE = "Site Template";
    static final String JSON_PAGESTYLE = "Page Style";
    static final String JSON_STYLESHEET= "Stylesheet";
    static final String JSON_BANNERIMAGE= "banner image";
    static final String JSON_RIGHTFOOTER= "right footer image";
    static final String JSON_LEFTFOOTER= "left footer image";
    static final String JSON_CHECKBOX= "checkbox";
    static final String JSON_OLDCHECKBOX= "oldcheckbox";
    static final String JSON_SECTION= "section";
    static final String JSON_INSTRUCTOR= "instructor";
    static final String JSON_DAYTIME= "day_time";
    static final String JSON_LOCATION= "location";
    static final String JSON_TA= "ta_1";
    static final String JSON_TA2= "ta_2";
    static final String JSON_RECITATIONS= "Recitations";
    static final String JSON_TYPE= "type";
    static final String JSON_DATE= "date";
    static final String JSON_TITLE= "title";
    static final String JSON_TOPIC= "topic";
    static final String JSON_SCHEDULEITEMS= "Schedule Items";
    static final String JSON_TEAMS= "Teams";
    static final String JSON_TEAMNAME= "name";
    static final String JSON_COLOR= "color (hex#)";
    static final String JSON_TEXTCOLOR= "text color(hex#)";
    static final String JSON_TEAMLINK= "link";
    static final String JSON_STUDENTS= "Students";
    static final String JSON_FIRSTNAME= "first name";
    static final String JSON_LASTNAME= "last name";
    static final String JSON_TEAM= "team";
    static final String JSON_ROLE= "role";
    static final String JSON_STARTINGMONDAYMONTH= "startingMondayMonth";
    static final String JSON_STARTINGMONDAYDAY= "startingMondayDay";
    static final String JSON_ENDINGFRIDAYMONTH= "endingFridayMonth";
    static final String JSON_ENDINGFRIDAYDAY= "endingFridayDay";
    static final String JSON_SCHEDLINK= "link:";
    static final String JSON_SCHEDTIME= "time";
    static final String JSON_SCHEDCRITERIA= "criteria";
    static final String JSON_SCHEDMONTH= "month";
    static final String JSON_SCHEDDAY= "day";
    static final String JSON_HOLIDAYS= "holidays";
    static final String JSON_LECTURES= "lectures";
    static final String JSON_SCHEDRECITATIONS= "recitations";
    static final String JSON_SCHEDHWS= "hws";
    static final String JSON_NAMES= "students";
    static final String JSON_ROLES= "roles";
    static final String JSON_WORK= "work";
    static final String JSON_RECS= "recitations";
    static final String JSON_LINK2= "link";
    static final String JSON_REFERENCES= "references";
    static final String JSON_SEMESTERTITLE= "semester";
    static final String JSON_PROJECTSTITLE= "projects";
    static final String JSON_NEWCOURSEINFO= "courseInfo";
    static final String JSON_NEWEXPORTDIR= "export_dir";
    static final String JSON_NEWTEMPLATEDIR= "template_dir";
    static final String JSON_NEWSITETEMPLATE= "siteTemplate";
    static final String JSON_NEWPAGESTYLE= "pageStyle";
    static final String JSON_NEWBANNERIMAGE= "banner_image";
    static final String JSON_NEWLEFTFOOTER= "left_footer";
    static final String JSON_NEWRIGHTFOOTER= "right_footer";
    static final String JSON_NEWSTYLESHEET= "stylesheet";
    
    public TestFiles(){
        
    }
   
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
     @Test
     public void testLoadJson() throws IOException{
        TAManagerApp app= new TAManagerApp();
        TestFiles z= new TestFiles();
        app.loadProperties(APP_PROPERTIES_FILE_NAME);
        TAData data= new TAData(app);
        TAFiles files= new TAFiles(app);
        JsonObject jsonObj=z.loadJSONFile("C:\\Users\\User\\Desktop\\HW4 CSE219\\TAManager_Solution\\work\\SiteSaveTest2.json");
        testSave testingData= new testSave();
        
        String startHour=jsonObj.getString(JSON_START_HOUR);
        assertEquals(testingData.getStartHour(), startHour);
        
        String endHour=jsonObj.getString(JSON_END_HOUR);
        assertEquals(testingData.getEndHour(), endHour);
    
        JsonArray courseInfoArray = jsonObj.getJsonArray(JSON_COURSEINFO);
        JsonObject subject=courseInfoArray.getJsonObject(0);
        String subjectText=subject.getString(JSON_SUBJECT);
        assertEquals(testingData.getSubject(), subjectText);
        
        String semester=subject.getString(JSON_SEMESTER);
        assertEquals(testingData.getSemester(), semester);
        
        String number= subject.getString(JSON_NUMBER);
        assertEquals(testingData.getNumber(), number);
        
        String year= subject.getString(JSON_YEAR);
        assertEquals(testingData.getYear(), year);
        
        String title= subject.getString(JSON_TITLE);
        assertEquals(testingData.getTitle(), title);
        
        String name= subject.getString(JSON_INSTRUCTORNAMEFIELD);
        assertEquals(testingData.getName(), name);
        
        String home= subject.getString(JSON_INSTRUCTORHOMEFIELD);
        assertEquals(testingData.getHome(), home);
        
        String exportDir= subject.getString(JSON_EXPORTDIR);
        assertEquals(testingData.getExportDir(), exportDir);
        
        JsonArray siteTemplateArray = jsonObj.getJsonArray(JSON_SITETEMPLATE);
        JsonObject templateDir=siteTemplateArray.getJsonObject(0);
        String templateString=templateDir.getString(JSON_TEMPLATEDIR);
        assertEquals(testingData.getTemplateDir(), templateString);
        
        boolean homeBool=templateDir.getBoolean(JSON_HOME);
        assertEquals(testingData.getHomeBool(), homeBool);
        
        boolean syllabus=templateDir.getBoolean(JSON_SYLLABUS);
        assertEquals(testingData.getSyllabusBool(), syllabus);
        
        boolean schedule=templateDir.getBoolean(JSON_SCHEDULE);
        assertEquals(testingData.getSchedBool(), schedule);
        
        boolean hws=templateDir.getBoolean(JSON_HWS);
        assertEquals(testingData.getHWSBool(), hws);
        
        boolean projects=templateDir.getBoolean(JSON_PROJECTS);
        assertEquals(testingData.getProjectsBool(), projects);
        
        JsonArray pageStyleArray = jsonObj.getJsonArray(JSON_PAGESTYLE);
        
        JsonObject bannerSchoolImage=pageStyleArray.getJsonObject(0);
        String bannerString=bannerSchoolImage.getString(JSON_BANNERIMAGE);
        assertEquals(testingData.getBannerImage(), bannerString);
        
        String leftFooter=bannerSchoolImage.getString(JSON_LEFTFOOTER);
        assertEquals(testingData.getLeftFooter(), leftFooter);
        
        String rightFooter=bannerSchoolImage.getString(JSON_RIGHTFOOTER);
        assertEquals(testingData.getRightFooter(), rightFooter);
                
        String styleSheet=bannerSchoolImage.getString(JSON_STYLESHEET);
        assertEquals(testingData.getStyleSheet(), styleSheet);
        
            
       JsonArray jsonTAArray = jsonObj.getJsonArray(JSON_UNDERGRAD_TAS);
       TeachingAssistant [] arr= testingData.getTeachingAssistants();
        for (int i = 0; i < jsonTAArray.size(); i++) {
            JsonObject jsonTA = jsonTAArray.getJsonObject(i);
            TeachingAssistant ta=arr[i];
            String taName = jsonTA.getString(JSON_NAME);
            assertEquals(ta.getName(), taName);
            String taEmail = jsonTA.getString(JSON_EMAIL);
            assertEquals(ta.getEmail(), taEmail);
            boolean checkbox=jsonTA.getBoolean(JSON_CHECKBOX);
            assertEquals(ta.getUndergrad().getValue(), checkbox);
            
        }
        
        JsonArray jsonOfficeHoursArray = jsonObj.getJsonArray(JSON_OFFICE_HOURS);
        TimeSlot [] timeSlots= testingData.getTimeSlots();
        for (int i = 0; i < jsonOfficeHoursArray.size(); i++) {
            JsonObject jsonOfficeHours = jsonOfficeHoursArray.getJsonObject(i);
            TimeSlot timeslot=timeSlots[i];
            String day = jsonOfficeHours.getString(JSON_DAY);
            assertEquals(timeslot.getDay(), day);
            String time = jsonOfficeHours.getString(JSON_TIME);
            assertEquals(timeslot.getTime(), time);
            String timeslotname = jsonOfficeHours.getString(JSON_NAME);
            assertEquals(timeslot.getName(), timeslotname);
        }
        
        JsonArray jsonRecArray = jsonObj.getJsonArray(JSON_RECITATIONS);
        RecitationData [] recData= testingData.getRecData();
            for (int i = 0; i < jsonRecArray.size(); i++) {
            JsonObject jsonrecHours = jsonRecArray.getJsonObject(i);
            RecitationData rec=recData[i];
            String section = jsonrecHours.getString(JSON_SECTION);
            assertEquals(rec.getSection(), section);
            String instructor = jsonrecHours.getString(JSON_INSTRUCTOR);
            assertEquals(rec.getInstructor(), instructor);
            String daytime= jsonrecHours.getString(JSON_DAYTIME);
            assertEquals(rec.getDayAndTime(), daytime);
            String location = jsonrecHours.getString(JSON_LOCATION);
            assertEquals(rec.getLocation(), location);
            String TA1 = jsonrecHours.getString(JSON_TA);
            assertEquals(rec.getTA1(), TA1);
            String TA2 = jsonrecHours.getString(JSON_TA2);
            assertEquals(rec.getTA2(), TA2);
            
            }
            
            JsonArray jsonSchedArray = jsonObj.getJsonArray(JSON_SCHEDULEITEMS);
            ScheduleData [] schedData= testingData.getSchedData();
            for (int i = 0; i < jsonSchedArray.size(); i++) {
            JsonObject jsonsched = jsonSchedArray.getJsonObject(i);
            ScheduleData sched=schedData[i];
            String type = jsonsched.getString(JSON_TYPE);
            assertEquals(sched.getType(), type);
            String date = jsonsched.getString(JSON_DATE);
            assertEquals(sched.getDate(), date);
            String schedTitle= jsonsched.getString(JSON_TITLE);
            assertEquals(sched.getTitle(), schedTitle);
            String topic = jsonsched.getString(JSON_TOPIC);
            assertEquals(sched.getTopic(), topic);
        
            }
            
            JsonArray jsonProjArray = jsonObj.getJsonArray(JSON_TEAMS);
            ProjectData [] projStudData= testingData.getProjectData();
            for (int i = 0; i < jsonProjArray.size(); i++) {
            JsonObject jsonProj = jsonProjArray.getJsonObject(i);
            ProjectData proj=projStudData[i];
            String projName = jsonProj.getString(JSON_TEAMNAME);
            assertEquals(proj.getName(), projName);
            String colorhex = jsonProj.getString(JSON_COLOR);
            assertEquals(proj.getColorHex(), colorhex);
            String colortxt= jsonProj.getString(JSON_TEXTCOLOR);
            assertEquals(proj.getTxtColor(), colortxt);
            String link = jsonProj.getString(JSON_TEAMLINK);
            assertEquals(proj.getLink(), link);
        
            }
            
            JsonArray jsonProjStudentArray = jsonObj.getJsonArray(JSON_PROJECTS);
            ProjectStudentData [] projData= testingData.getStudentProjData();
            for (int i = 0; i < jsonProjStudentArray.size(); i++) {
            JsonObject jsonProjStudent = jsonProjStudentArray.getJsonObject(i);
            ProjectStudentData proj=projData[i];
            String firstname = jsonProjStudent.getString(JSON_FIRSTNAME);
            assertEquals(proj.getFirstName(), firstname);
            String lastname = jsonProjStudent.getString(JSON_LASTNAME);
            assertEquals(proj.getLastName(), lastname);
            String team= jsonProjStudent.getString(JSON_TEAM);
            assertEquals(proj.getTeam(), team);
            String role = jsonProjStudent.getString(JSON_ROLE);
            assertEquals(proj.getRole(), role);
        
            }
         
               
        
        }
    
    public static void main(String [] args) throws IOException{
        TestFiles z= new TestFiles();
        z.testLoadJson();
        
        
      
      
}
}
