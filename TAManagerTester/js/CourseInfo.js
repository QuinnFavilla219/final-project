// DATA TO LOAD
var subject;
var semester;
var number;
var year;
var thisTitle;
var name;
var courseTable;
var exportDir;
var home;



function buildCourseInfoTab() {
    var dataFile = "./js/CourseInfoData.json";
    loadData(dataFile, loadCourseInfoData);
}

function loadData(jsonFile, callback) {
    $.getJSON(jsonFile, function(json) {
        callback(json);
    });
}

function loadCourseInfoData(json) {
    initInfo(json);
    
}

function initInfo(data){
	subject=data.courseInfo[0].subject;
	semester=data.courseInfo[0].semester;
	number=data.courseInfo[0].number;
	year=data.courseInfo[0].year;
	title=data.courseInfo[0].title;
	name=data.courseInfo[0].name;
	home=data.courseInfo[0].home;
	console.log(name);

	var banner = $("#banner");
	banner.append(subject+" "+number+"-"+semester+" "+year+ "-"+"<br />"+title);

}
