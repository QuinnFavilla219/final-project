// DATA TO LOAD
var subject;
var semester;
var number;
var year;
var thisTitle;
var nam;
var tit;
var courseTable;
var exportDir;
var home;
var homeUse;
var syllabusUse;
var scheduleUse;
var hwsUse;
var projectsUse;
var bannerImage;
var leftFooterImage;
var rightFooterImage;
var css;

function buildCourseInfoTab() {
    var dataFile = "./js/CourseInfoData.json";
    loadData2(dataFile);
}

function loadData2(jsonFile) {
    $.getJSON(jsonFile, function(json) {
        initInfo(json);
    });
}

function loadCourseInfoData(json) {
    initInfo(json);
    initNavBar(json);
    
}

function initInfo(data){
	subject=data.courseInfo[0].subject;
	semester=data.courseInfo[0].semester;
	number=data.courseInfo[0].number;
	year=data.courseInfo[0].year;
	tit=data.courseInfo[0].title;
	nam=data.courseInfo[0].name;
	home=data.courseInfo[0].home;

	var banner = $("#banner");
	banner.append(subject+" "+number+"-"+semester+" "+year+ "-"+"<br />"+tit);

	var instructor=$("#instructor_link");
	instructor.append("<a href="+home+">"+nam+"</a>");

	homeUse=data.siteTemplate[0].home;
	syllabusUse=data.siteTemplate[0].syllabus;
	scheduleUse=data.siteTemplate[0].schedule;
	hwsUse=data.siteTemplate[0].HWS;
	projectsUse=data.siteTemplate[0].projects;

	bannerImage=data.pageStyle[0].banner_image;
	console.log(bannerImage);
	var tempString="\"http://www.stonybrook.edu\"";

	var navBar=$("#navbar");
	var textToAppend="<a href="+tempString+"><img alt"+"="+"\"Stony Brook University\""+" "+"class="+"\"sbu_navbar\""+" "+"src="+bannerImage+"></a>";
	if(homeUse==true){
		textToAppend+="<a class="+"\"open_nav\""+" "+"href="+"\"index.html\""+" "+"id="+"\"home_link\""+">Home</a>";
	}
	if(syllabusUse==true){
		textToAppend+="<a class="+"\"nav\""+" "+"href="+"\"syllabus.html\""+" "+"id="+"\"syllabus_link\""+">Syllabus</a>";
	}
	if(scheduleUse==true){
		textToAppend+="<a class="+"\"nav\""+" "+"href="+"\"schedule.html\""+" "+"id="+"\"schedule_link\""+">Schedule</a>";
	}
	if(hwsUse==true){
		textToAppend+="<a class="+"\"nav\""+" "+"href="+"\"hws.html\""+" "+"id="+"\"hws_link\""+">HWs</a>";
	}
	if(projectsUse==true){
		textToAppend+="<a class="+"\"nav\""+" "+"href="+"\"projects.html\""+" "+"id="+"\"projects_link\""+">Projects</a>";
	}
	navBar.append(textToAppend);

	leftFooterImage=data.pageStyle[0].left_footer;
	rightFooterImage=data.pageStyle[0].right_footer;
	css=data.pageStyle[0].stylesheet;

	var imagesDiv=$("#images");
	var imagesToAppend="<a href="+"\"http://www.stonybrook.edu\""+"><img alt="+"\"SBU\""+" "+"class="+"\"sunysb\""+" "+"src="+leftFooterImage+" "+"style="+"\"float:left\""+"></a>"+"\n";
	imagesToAppend+="<a href="+"\"http://www.stonybrook.edu\""+"><img alt="+"\"SBU\""+" "+"class="+"\"sunysb\""+" "+"src="+rightFooterImage+" "+"style="+"\"float:right\""+"></a>";
	imagesDiv.append(imagesToAppend);

	var styleDiv=$("#style");
	var styleToAppend="<link"+" "+"href="+"\"./css/course_homepage_layout.css\""+" "+"rel"+"="+"\"stylesheet\""+" "+"type"+"="+"\"text/css\""+">"+"\n";
	if(css=="sea_wolf.css"){
		styleToAppend+="<link href="+"\"./css/sea_wolf.css\""+" "+"rel="+"\"stylesheet\""+" "+"type="+"\"text/css\""+">";
	}
	styleDiv.append(styleToAppend);
	console.log(css);
	console.log(styleToAppend)
}





